# -*- coding: utf-8 -*-

"""
/***************************************************************************
 Embankment and ditch
                                 A QGIS plugin
 Retrieve embankment and ditch from geomorphon and compute overlapping with hedges
 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                              -------------------
        begin                : 2022-01-22
        copyright            : (C) 2022 by Gabriel Marquès
        email                : gabriel.marques@toulouse-inp.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

__author__ = 'Gabriel Marquès'
__date__ = '2022-01-22'
__copyright__ = '(C) 2022 by Gabriel Marquès'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterFeatureSink,
                       QgsProcessingParameterNumber,
                       QgsProcessingParameterField,
                       QgsProcessingParameterRasterLayer)
from qgis.PyQt.QtGui import QIcon
from hedge_tools import resources # Only need in hedge_tools.py normaly but just to keep track of import

from hedge_tools.utils.classes import class_hedge as h

class EmbankmentDitchAlgorithm(QgsProcessingAlgorithm):
    """
    Informs if hedges has an embankment or a ditch using geomorphon.

    Parameters
    ---
    INPUT_POLY (QgisObject : QgsVectorLayer) : Polygon layer path. Contains hedges
    INPUT_ARC (QgisObject : QgsVectorLayer) : Linestring layer path. Contains arc (polyline) hedges
    INPUT_NODE (QgisObject : QgsVectorLayer) : Node layer path. Contains nodes to delimit the hedges
    INPUT_SEARCH (Integer) : Search distance in meters
    INPUT_THRESH (Integer) : Percentage of overlap to consider that the hedge have
                             an embankment or a ditch.
    Return
    ---
    OUTPUT_POLY (QgisObject : QgsVectorLayer) : Polygon : Polygon layer with
                              2 boolean fields : embankment and ditch
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.
    INPUT_POLY = "INPUT_POLY"
    INPUT_ARC = "INPUT_ARC"
    INPUT_NODE = "INPUT_NODE"
    INPUT_DEM = "INPUT_DEM"
    INPUT_BAND = "INPUT_BAND"
    INPUT_SEARCH = "INPUT_SEARCH"
    INPUT_THRESH = "INPUT_THRESH"
    OUTPUT_POLY = "OUTPUT_POLY"

    def initAlgorithm(self, config):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        # We add the input vector polygons features source.
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT_POLY,
                self.tr("Polygons vector layer"),
                [QgsProcessing.TypeVectorPolygon]
            )
        )

        # We add the input vector lines features source.
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT_ARC,
                self.tr("Arcs vector layer"),
                [QgsProcessing.TypeVectorLine]
            )
        )

        # We add the input vector nodes features source.
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT_NODE,
                self.tr("Nodes vector layer"),
                [QgsProcessing.TypeVectorPoint]
            )
        )

        # We add the input DEM
        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.INPUT_DEM,
                self.tr("Digital elevation model"),
                [QgsProcessing.TypeRaster]
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                name=self.INPUT_BAND,
                description=self.tr("DEM band number"),
                type=QgsProcessingParameterNumber.Integer,
                defaultValue=1,
                optional=False,
                minValue=1
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                name=self.INPUT_SEARCH,
                description=self.tr("Search distance for detecting embankment and ditch"),
                type=QgsProcessingParameterNumber.Integer,
                defaultValue=3,
                optional=False,
                minValue=3
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                name=self.INPUT_THRESH,
                description=self.tr("Percentage of overlap with hedges to consider an embankment or a ditch"),
                type=QgsProcessingParameterNumber.Integer,
                defaultValue=20,
                optional=False,
                minValue=0,
                maxValue=100
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """

        poly_layer = self.parameterAsVectorLayer(parameters, self.INPUT_POLY, context)
        arc_layer = self.parameterAsVectorLayer(parameters, self.INPUT_ARC, context)
        node_layer = self.parameterAsVectorLayer(parameters, self.INPUT_NODE, context)
        dem = self.parameterAsRasterLayer(parameters, self.INPUT_DEM, context)

        feedback.pushInfo("Starting processing")

        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Init hedge class and retrieve interface type
        hedges = h.Hedges(poly_layer, arc_layer, node_layer)
        hedges.embankment_ditch(context, feedback,
                                dem, parameters["INPUT_BAND"],
                                parameters["INPUT_SEARCH"],
                                parameters["INPUT_THRESH"])

        return {"OUTPUT_POLY": parameters[self.INPUT_POLY]}

    def icon(self):
        """
        Should return a QIcon which is used for your provider inside
        the Processing toolbox.
        """
        return QIcon(":/plugins/hedge_tools/images/hedge_tools.png")
    
    def shortHelpString(self):
        """
        Returns a localised short help string for the algorithm.
        """
        return self.tr("Compute geomorphon with a small search distance \
                        to manifest embankment and ditch in the DEM. \
                        Then check if the overlapping percentage with hedges \
                        is higher than the specified threshold.")

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "embankmentditch"

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr("Embankment and ditch")

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr("Geographic context")

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'geographiccontext'

    def tr(self, string):
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return EmbankmentDitchAlgorithm()