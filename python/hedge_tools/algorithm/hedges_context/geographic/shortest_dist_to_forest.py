# -*- coding: utf-8 -*-

"""
/***************************************************************************
 Shortest distance to forest
                                 A QGIS plugin
 Compute distance on the fly between hedge nodes and forest
 and store result in arc layer data provider.x

 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                              -------------------
        begin                : 2022-01-22
        copyright            : (C) 2022 by Gabriel Marquès
        email                : gabriel.marques@toulouse-inp.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

__author__ = 'Gabriel Marquès'
__date__ = '2022-01-22'
__copyright__ = '(C) 2022 by Gabriel Marquès'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterFeatureSink,
                       QgsProcessingParameterNumber,
                       QgsProcessingParameterField)
from qgis.PyQt.QtGui import QIcon
from hedge_tools import resources # Only need in hedge_tools.py normaly but just to keep track of import

from hedge_tools.utils.classes import class_hedge as h

class DistanceToForestAlgorithm(QgsProcessingAlgorithm):
    """
    Compute and store in a field the distance as the crow flies
    between nodes and closest forest aswell as forest id

    Parameters
    ---
    INPUT_POLY (QgisObject : QgsVectorLayer) : Polygon layer path. Contains hedges
    INPUT_ARC (QgisObject : QgsVectorLayer) : Linestring layer path. Contains arc (polyline) hedges
    INPUT_NODE (QgisObject : QgsVectorLayer) : Node layer path. Contains nodes to delimit the hedges
    INPUT_FOREST (QgisObject ; QgsVectorLayer) : Forest layer path.
    INPUT_FOREST_ID : (Integer) : Id field of the forest
    INPUT_CONNECT : (Integer) : Distance of the buffer to artificially connect forest
                                to hedge extremities.

    Return
    ---
    OUTPUT_POLY (QgisObject : QgsVectorLayer) : Polygon : Polygon layer with new field
                                                corresponding to shortest distance
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.
    INPUT_POLY = "INPUT_POLY"
    INPUT_ARC = "INPUT_ARC"
    INPUT_NODE = "INPUT_NODE"
    INPUT_FOREST = "INPUT_FOREST"
    INPUT_FOREST_ID = "INPUT_FOREST_ID"
    INPUT_CONNECT = "INPUT_CONNECT"
    OUTPUT_NODE = "OUTPUT_NODE"

    def initAlgorithm(self, config):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        # We add the input vector polygons features source.
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT_POLY,
                self.tr("Polygons vector layer"),
                [QgsProcessing.TypeVectorPolygon]
            )
        )

        # We add the input vector lines features source.
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT_ARC,
                self.tr("Arcs vector layer"),
                [QgsProcessing.TypeVectorLine]
            )
        )

        # We add the input vector nodes features source.
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT_NODE,
                self.tr("Nodes vector layer"),
                [QgsProcessing.TypeVectorPoint]
            )
        )

        # Forest layer
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT_FOREST,
                self.tr("Forest vector layer"),
                [QgsProcessing.TypeVectorPolygon]
            )
        )

        # We add the forest id field.
        self.addParameter(
            QgsProcessingParameterField(
                self.INPUT_FOREST_ID,
                self.tr("Forest id field"),
                parentLayerParameterName="INPUT_FOREST"
            )
        )

        # Snap distance to network
        self.addParameter(
            QgsProcessingParameterNumber(
                name=self.INPUT_CONNECT,
                description=self.tr("Allowed distance for connecting forest to network"),
                type=QgsProcessingParameterNumber.Integer,
                defaultValue=20,
                optional=False,
                minValue=0
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """

        poly_layer = self.parameterAsVectorLayer(parameters, self.INPUT_POLY, context)
        arc_layer = self.parameterAsVectorLayer(parameters, self.INPUT_ARC, context)
        node_layer = self.parameterAsVectorLayer(parameters, self.INPUT_NODE, context)
        forest_layer = self.parameterAsVectorLayer(parameters, self.INPUT_FOREST, context)

        feedback.pushInfo("Starting processing")

        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Init hedge class and retrieve shortest distance
        hedges = h.Hedges(poly_layer, arc_layer, node_layer)
        hedges.shortest_distance(context, feedback, forest_layer,
                                 parameters[self.INPUT_FOREST_ID],
                                 [self.INPUT_CONNECT])

        return {"OUTPUT_NODE": parameters[self.INPUT_NODE]}

    def icon(self):
        """
        Should return a QIcon which is used for your provider inside
        the Processing toolbox.
        """
        return QIcon(":/plugins/hedge_tools/images/hedge_tools.png")
    
    def shortHelpString(self):
        """
        Returns a localised short help string for the algorithm.
        """
        return self.tr("Connexion option will inform users if a forest is connected \
                        (either directly or indirectly) or not to a forest from his hedge network. \
                        Distance option will return the distance as the crow flies \
                        to the nearest forest from hedge. \
                        Forest can be a polygon layer or a point layer depicting the forest. \
                        Distance parameter is usefull for the connexion option only. \
                        It allows to connect forest center point to network.")

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "distancetoforest"

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr("Shortest distance to forest")

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr("Geographic context")

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'geographiccontext'

    def tr(self, string):
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return DistanceToForestAlgorithm()
