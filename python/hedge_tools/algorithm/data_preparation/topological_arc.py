# -*- coding: utf-8 -*-

"""
/***************************************************************************
 MedianAxisVoronoi
                                 A QGIS plugin
 Create median axis of polygone features with voronoï method
 Generated by Plugin Builder: http://g-sherman.github.io/Qgis-Plugin-Builder/
                              -------------------
        begin                : 2022-01-22
        copyright            : (C) 2022 by Gabriel Marquès
        email                : gabriel.marques@toulouse-inp.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

__author__ = 'Gabriel Marquès'
__date__ = '2022-01-22'
__copyright__ = '(C) 2022 by Gabriel Marquès'

# This will get replaced with a git SHA1 when you do a git archive

__revision__ = '$Format:%H$'

from qgis.PyQt.QtCore import (QCoreApplication,
                              QVariant)
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterFeatureSink,
                       QgsProcessingParameterNumber,
                       QgsVectorLayer)
from qgis.PyQt.QtGui import QIcon
from hedge_tools import resources # Only need in hedge_tools.py normaly but just to keep track of import 

from hedge_tools.utils.vector import geometry as g
from hedge_tools.utils.vector import attribute_table as at
from hedge_tools.utils.vector import utils
from hedge_tools.utils.classes import HedgeGraph as hG
import uuid
# For debugging
# import cProfile, pstats, io
# from pstats import SortKey


class TopologicalArcAlgorithm(QgsProcessingAlgorithm):
    """
    Implementation of median axis computation form voronoi extraction.
    Pre process is inspired from open jump skeletonizer algorithm.

    Parameters
    ---
    INPUT (QgisObject : QgsVectorLayer) : Polygon : Layer input from users.
    MIN_WIDTH (float) : Minimal width of a polygon in the input layer
                        Value used for pre process (simplification/densification)
    DANGLE_LGTH (int) : Below this length the path is considered a dangle and deleted

    Return
    ---
    OUTPUT (QgisObject : QgsVectorLayer) : Linestring : Skeleton of the input layer features.
    ERROR (QgisObject : QgsVectorLayer) : Linestring : Potential error in the median axis.
    Usually disconnected graph caused by a too high min width resulting in a vertex density too low.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.
    INPUT = "INPUT"
    MIN_WIDTH = "MIN_WIDTH"
    DANGLE_LGTH = "DANGLE_LGTH"
    OUTPUT = "OUTPUT"
    ERROR = "ERROR"

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        # We add the input vector features source. It can have any kind of
        # geometry.
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT,
                self.tr('Polygon layer'),
                [QgsProcessing.TypeVectorPolygon]
            )
        )

        # Distance value parameter
        self.addParameter(
            QgsProcessingParameterNumber(
                name=self.MIN_WIDTH,
                description=self.tr("Minimum width (-1 for auto minimal width)"),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=-1,
                optional=False,
                minValue=-1,
                maxValue=1000
            )
        )

        # Distance value parameter
        self.addParameter(
            QgsProcessingParameterNumber(
                name=self.DANGLE_LGTH,
                description=self.tr("Minimum dangle length"),
                type=QgsProcessingParameterNumber.Integer,
                defaultValue=50,
                optional=False,
                minValue=0,
                maxValue=1000
            )
        )

        # We add a feature sink in which to store our processed features (this
        # usually takes the form of a newly created vector layer when the
        # algorithm is run in QGIS).
        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.OUTPUT,
                self.tr("Output layer")
            )
        )

        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.ERROR,
                self.tr("Error layer")
            )
        )
    
    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """
        polygon_layer = self.parameterAsVectorLayer(parameters, self.INPUT,
                                                    context)
        min_width = self.parameterAsDouble(parameters, self.MIN_WIDTH, context)
        dangle_lgth = self.parameterAsInt(parameters, self.DANGLE_LGTH, context)

        idx = polygon_layer.fields().indexFromName("pid")
        if idx == -1:
            feedback.pushInfo("pid field will be created in {}. \
                Please do not remove it if you want to use hedge tools".format(polygon_layer.name()))
            at.create_fields(polygon_layer, [("pid", QVariant.Int)])
            idx = polygon_layer.fields().indexFromName("pid")
            attr_map = {f.id(): {idx: f.id()} for f in polygon_layer.getFeatures()}
            polygon_layer.dataProvider().changeAttributeValues(attr_map)

        count = polygon_layer.featureCount()

        workspace, folder_object = utils.create_temp_workspace()

        feedback.setProgress(0)

        features = []
        errors = []
        id = 0
        for current, poly in enumerate(polygon_layer.getFeatures()):
            polygon_geometry = poly.geometry()
            polygon_geometry = g.prepare_geometry(polygon_geometry, min_width)
            skeleton = g.compute_skeleton(polygon_geometry)
            graph = hG.HedgeGraph(skeleton)
            graph.non_recursive_connected_component()
            graph.remove_fork(dangle_lgth)
            # We have to recreate graph object because "hole" in vid and eid create
            # ambiguity in topology in leaf correction
            # Edge or vertex creation return an id of max(id) + 1 but topology id
            # on vertex and edge store the first vacant place in the list of id
            temp_geom = graph.edges_to_lines()  # Default output_type is geometry
            graph = hG.HedgeGraph(temp_geom)
            graph.correct_leaf(poly.geometry())  # Pass original geometry
            graph.non_recursive_connected_component()
            results = graph.edges_to_polylines(output_type=True)
            if isinstance(results, tuple):  # There is error and results is a tuple
                # features += results[0]
                errors += results[1]
                feats = results[0]
            else:
                # features += results
                feats = results
            # Add fid, poly id (pid), and edge id (eid) and set value
            feats = [at.add_fields(feature, ["fid", "eid", "pid"],
                                   [QVariant.Int, QVariant.Int, QVariant.Int]) for feature
                     in feats]
            features += [at.set_fields_value(feature, ["fid", "eid", "pid"],
                                             [id + i + 1, id + i + 1, poly["pid"]])
                         for i, feature in enumerate(feats)]
            # Increment id value with len of feats for current poly
            id += len(feats)

            # del(temp)
            # Set progress
            feedback.setProgress(int((current / count) * 100))
            # Check for cancellation
            if feedback.isCanceled():
                return {}

        # Load into layers
        name = str(uuid.uuid4())[0:5]
        graph_layer = utils.create_layer(polygon_layer, geom_type="LineString",
                                      data_provider="ogr",
                                      path=workspace + "/" + name + ".gpkg")
        name = str(uuid.uuid4())[0:5]
        error_layer = utils.create_layer(polygon_layer, geom_type="LineString",
                                      data_provider="ogr",
                                      path=workspace + "/" + name + ".gpkg")
        # Create fields already present in feature. Fid is created by default
        at.create_fields(graph_layer, [("eid", QVariant.Int), ("pid", QVariant.Int)])
        graph_layer.dataProvider().addFeatures(features)
        error_layer.dataProvider().addFeatures(errors)

        # Compute Id_line --> eid ?
        # at.create_fields(graph_layer, [("eid", QVariant.Int)])
        # idx = graph_layer.fields().indexFromName("eid")
        # attr_map = {f.id(): {idx: f.id()} for f in graph_layer.getFeatures()}
        # graph_layer.dataProvider().changeAttributeValues(attr_map)

        (sink_output, output_id) = self.parameterAsSink(parameters,
                                                        self.OUTPUT,
                                                        context,
                                                        graph_layer.fields(),
                                                        graph_layer.wkbType(),
                                                        graph_layer.sourceCrs())
        for feature in graph_layer.getFeatures():
            sink_output.addFeature(feature, QgsFeatureSink.FastInsert)

        (sink_error, error_id) = self.parameterAsSink(parameters,
                                                      self.ERROR,
                                                      context,
                                                      error_layer.fields(),
                                                      error_layer.wkbType(),
                                                      error_layer.sourceCrs())
        for feature in error_layer.getFeatures():
            sink_error.addFeature(feature, QgsFeatureSink.FastInsert)

        try:
            utils.delete_temp_workspace(folder_object)
        except:
            pass
        
        # pr.disable()
        # s = io.StringIO()
        # sortby = SortKey.CUMULATIVE
        # ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
        # ps.print_stats()
        # path_profiling = os.path.join("/home/terudel/hedge_tools/Qgis-plugin/eagle_hedge/hedge_tools/back_up_brouillon/utils", "line_profiling.dmp")
        # ps.dump_stats(path_profiling)

        return {self.OUTPUT: output_id,
                self.ERROR: error_id}

    def icon(self):
        """
        Should return a QIcon which is used for your provider inside
        the Processing toolbox.
        """
        return QIcon(":/plugins/hedge_tools/images/hedge_tools.png")
    
    def shortHelpString(self):
        """
        Returns a localised short help string for the algorithm.
        """
        return self.tr("Compute the median axis for each hedge in a polygon layer. \
                        Regroup the line into polyline to form the topological \
                        representation of the hedge network. \n\
                        Auto min width is determined if min width parameter is set to -1 \
                        but it can signigicatively slow the process. \
                        Thus in case of auto width the minimal width allowed is 2. \
                        If you want a lower value you are free to do so.")

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'topologicalarc'

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr("1 - Create topological arc")

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr("1 - Data preparation")

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'datapreparation'

    def tr(self, string):
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return TopologicalArcAlgorithm()
