# -*- coding: utf-8 -*-

"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsProcessingException,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterFeatureSink)
from qgis import processing


def tree_classification(max_z, min_z, avg_z, max_nnd, min_nnd, avg_nnd):
    """

    Parameters
    ----------
    max_z :
        param min_z:
    avg_z :
        param max_nnd:
    min_nnd :
        param avg_nnd:
    min_z :
        
    max_nnd :
        
    avg_nnd :
        

    Returns
    -------

    """
    if max_z <= 9.52:
        return 'taillis'
    if max_z > 9.52:
        if avg_z <= 7.66:
            if max_nnd <= 4.63:
                if max_z <= 13.64:
                    if max_z <= 12.05:
                        return 'taillis_futaie'
                    if max_z > 12.05:
                        if max_nnd <= 3.15:
                            return 'futaie'
                        if max_nnd > 3.15:
                            return 'taillis'
                if max_z > 13.64:
                    return 'taillis_futaie'
            if max_nnd > 4.63:
                if max_z <= 21.76:
                    return 'taillis'
                if max_z > 21.76:
                    if min_z <= -0.61:
                        return 'taillis_futaie'
                    if min_z > -0.61:
                        if avg_nnd <= 0.90:
                            return 'taillis'
                        if avg_nnd > 0.90:
                            return 'futaie'
        if avg_z > 7.66:
            if max_nnd <= 3.48:
                return 'taillis'
            if max_nnd > 3.48:
                return 'futaie'


class ExampleProcessingAlgorithm(QgsProcessingAlgorithm):
    """This is an example algorithm that takes a vector layer and
    creates a new identical one.
    
    It is meant to be used as an example of how to create your own
    algorithms and explain methods and variables used to do it. An
    algorithm like this will be available in all elements, and there
    is not need for additional work.
    
    All Processing algorithms should extend the QgsProcessingAlgorithm
    class.

    Parameters
    ----------

    Returns
    -------

    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    INPUT = 'INPUT'
    OUTPUT = 'OUTPUT'

    def tr(self, string):
        """Returns a translatable string with the self.tr() function.

        Parameters
        ----------
        string :
            

        Returns
        -------

        """
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        """ """
        return ExampleProcessingAlgorithm()

    def name(self):
        """Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.

        Parameters
        ----------

        Returns
        -------

        """
        return 'hedgeclassification'

    def displayName(self):
        """Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.

        Parameters
        ----------

        Returns
        -------

        """
        return self.tr('Hedge Classification')

    def group(self):
        """Returns the name of the group this algorithm belongs to. This string
        should be localised.

        Parameters
        ----------

        Returns
        -------

        """
        return self.tr('Hedges Lidar')

    def groupId(self):
        """Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.

        Parameters
        ----------

        Returns
        -------

        """
        return 'hedgeslidar'

    def shortHelpString(self):
        """

        Parameters
        ----------

        Returns
        -------
        
            should provide a basic description about what the algorithm does and the

        """
        return self.tr("Classification of an hedge layer thanks to a decision tree")

    def initAlgorithm(self, config=None):
        """Here we define the inputs and output of the algorithm, along
        with some other properties.

        Parameters
        ----------
        config :
            Default value = None)

        Returns
        -------

        """

        # We add the input vector features source. It can have any kind of
        # geometry.
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT,
                self.tr('Input layer'),
                [QgsProcessing.TypeVectorPolygon]
            )
        )

        # We add a feature sink in which to store our processed features (this
        # usually takes the form of a newly created vector layer when the
        # algorithm is run in QGIS).
        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.OUTPUT,
                self.tr('Output layer')
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        """Here is where the processing itself takes place.
        
        Args:

        Parameters
        ----------
        feedback :
            
        context :
            
        parameters :
            

        Returns
        -------
        type
            

        """

        # Retrieve the feature source and sink. The 'dest_id' variable is used
        # to uniquely identify the feature sink, and must be included in the
        # dictionary returned by the processAlgorithm function.
        source = self.parameterAsVectorLayer(
            parameters,
            self.INPUT,
            context
        )
        if source is None:
            raise QgsProcessingException(self.invalidSourceError(parameters, self.INPUT))

        (sink, dest_id) = self.parameterAsSink(
            parameters,
            self.OUTPUT,
            context,
            source.fields(),
            source.wkbType(),
            source.sourceCrs()
        )

        # Send some information to the user
        feedback.pushInfo('CRS is {}'.format(source.sourceCrs().authid()))
        if sink is None:
            raise QgsProcessingException(self.invalidSinkError(parameters, self.OUTPUT))

        # Compute the number of steps to display within the progress bar and
        # get features from source
        total = 100.0 / source.featureCount() if source.featureCount() else 0
        features = source.getFeatures()

        
        for current, feature in enumerate(features):
            if feedback.isCanceled():
                break
            value = tree_classification(
                max_z=feature["max_z"], min_z=feature["min_z"], avg_z=feature["min_z"],
                max_nnd=feature["max_nnd"], min_nnd=feature["min_nnd"], avg_nnd=feature["min_nnd"]
            )
            print(value)
            map[feature.id()] = {source.fields().indexFromName("Classe"): value}

            sink.addFeature(feature, QgsFeatureSink.FastInsert)
            feedback.setProgress(int(current * total))

        source.dataProvider().changeAttributeValues(map)
        # Return the results of the algorithm. In this case our only result is
        # the feature sink which contains the processed features, but some
        # algorithms may return multiple feature sinks, calculated numeric
        # statistics, etc. These should all be included in the returned
        # dictionary, with keys matching the feature corresponding parameter
        # or output names.
        return {self.OUTPUT: dest_id}
