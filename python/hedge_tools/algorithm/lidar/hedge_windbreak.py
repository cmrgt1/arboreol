# -*- coding: utf-8 -*-

"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

from qgis.PyQt.QtCore import QCoreApplication
from qgis.PyQt.QtGui import QIcon
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsProcessingException,
                       QgsVertexIterator,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterField,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterFeatureSink,
                       QgsProcessingParameterFile,
                       QgsProcessingParameterString,
                       QgsProcessingParameterBoolean,
                       QgsProcessingParameterNumber,
                       QgsWkbTypes,
                       QgsVectorLayer,
                       QgsProcessingParameterRasterDestination,
                       QgsProject
                       )
from qgis import processing
import os
import hedge_tools.utils.lidar.windbreak_function as wf
import hedge_tools.utils.vector.utils as vutils
import cProfile, pstats, io 
from pstats import SortKey


class WindbreakAlgorithm(QgsProcessingAlgorithm):
    """
    This is an example algorithm that takes a vector layer and
    creates a new identical one.

    It is meant to be used as an example of how to create your own
    algorithms and explain methods and variables used to do it. An
    algorithm like this will be available in all elements, and there
    is not need for additional work.

    All Processing algorithms should extend the QgsProcessingAlgorithm
    class.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    INPUT = 'INPUT'
    WIND_ORIENTATION = 'WIND_ORIENTATION'
    MAX_EXTENT = 'MAX_EXTENT'
    OUTPUT_FOLDER = 'OUTPUT_FOLDER'
    HEIGHT_FIELD_NAME = 'HEIGHT_FIELD_NAME'
    POROSITY_FIELD_NAME = 'POROSITY_FIELD_NAME'
    WIDTH_FIELD_NAME = 'WIDTH_FIELD_NAME'
    HEDGE_ID_FIELD_NAME = 'HEDGE_ID_FIELD_NAME'
    RESOLUTION = 'RESOLUTION'
    INTERMEDIATE_RASTER = 'INTERMEDIATE_RASTER'
    SHELTERS_AREA = "SHELTERS_AREA"
    PNT_IN_SHELTERS_AREA = "PNT_IN_SHELTERS_AREA" 
    F_XH = "F_XH" 


    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return WindbreakAlgorithm()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'windbreakeffect'

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr('Windbreak effect')

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr('Hedges Lidar')

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'hedgeslidar'

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm. This string
        should provide a basic description about what the algorithm does and the
        parameters and outputs associated with it..
        """
        return self.tr("""
        Allows to calculate friction velocity reduction factor at any distance in the surrounding area. 
        Parameters
        ---
        INPUT (Polyline) : Layer to copy the attributes, geometry type and crs. 
        WIND_ORIENTATION (int) : Angle in degrees indicating the origin of the wind.
        MAX_EXTENT (int) : Factor of the height of the hedge characterizing the maximum distance to the hedge.
        HEIGHT_FILD_NAME (str) : Name of the height field containing in the `data_hedge` layer. Defaults to "height".
        POROSITY_FIELD_NAME (str) : Name of the optical porosity field containing in the `data_hedge` layer. Defaults to "op".
        WIDTH_FIELD_NAME (str) : Name of the width field containing in the `data_hedge` layer. Defaults to "width".
        HEDGE_ID_FIELD_NAME (str) : Name of the id of the hedge field containing in the `data_hedge` layer. Defaults to "id".
        RESOLUTION (int) : Dimension (width, height) of the resulting raster image.
        INTERMEDIATE_RASTER (bool) : Option to store the intermediate rasters calculated by the function. If None, only the final
        OUTPUT_FOLDER (str) : Path of file in which the output layers will be deposited if INTERMEDIATE_RASTER is True.
        RASTER 'raster_fxh' will be stored.
        Return
        ---
        fxh (raster) : Friction velocity reduction factor raster.
        """ )
    
    def icon(self):
        """
        Should return a QIcon which is used for your provider inside
        the Processing toolbox.
        """
        return QIcon(":/plugins/hedge_tools/images/hedge_tools.png")

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        # We add the input vector features source. It can have any kind of

        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT,
                self.tr('Input layer (hedge)'),
                [QgsProcessing.TypeVectorLine]))
        
        self.addParameter(
            QgsProcessingParameterNumber(
                name=self.WIND_ORIENTATION,
                description=self.tr('Wind orientation'),
                type=QgsProcessingParameterNumber.Integer,
                optional=False,
                minValue=0,
                maxValue=360 ))  
          
        self.addParameter(
            QgsProcessingParameterNumber(
                name=self.MAX_EXTENT,
                description=self.tr('Maximum extent of the shelter zone'),
                type=QgsProcessingParameterNumber.Integer,
                optional=False,
                defaultValue=25
            ))

        self.addParameter(
            QgsProcessingParameterField(
                name=self.HEIGHT_FIELD_NAME,
                description=self.tr('Height field name in input layer'),
                optional=False,
                defaultValue='height',
                type = QgsProcessingParameterField.Numeric,
                parentLayerParameterName= self.INPUT
            ))  
           
        self.addParameter(
            QgsProcessingParameterField(
                name=self.POROSITY_FIELD_NAME,
                description=self.tr('Porosity field name in input layer'),
                type = QgsProcessingParameterField.Numeric,
                optional=False,
                defaultValue='op',
                parentLayerParameterName= self.INPUT
            )) 
        
        self.addParameter(
            QgsProcessingParameterField(
                name=self.WIDTH_FIELD_NAME,
                description=self.tr('Width field name in input layer'),
                type = QgsProcessingParameterField.Numeric,
                optional=False,
                defaultValue='width',
                parentLayerParameterName= self.INPUT
            )) 
            
        self.addParameter(
            QgsProcessingParameterField(
                name=self.HEDGE_ID_FIELD_NAME,
                description=self.tr('Id field name in input layer'),
                optional=False,
                defaultValue='id',
                type = QgsProcessingParameterField.Numeric,
                parentLayerParameterName= self.INPUT
            )) 
          
        self.addParameter(
            QgsProcessingParameterNumber(
                name=self.RESOLUTION,
                description=self.tr('Output spatial resolution (m)'),
                optional=False,
                type = QgsProcessingParameterNumber.Double,
                defaultValue=5
            ))
        
        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.SHELTERS_AREA,
                self.tr("Shelter area layer")
            ))

        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.PNT_IN_SHELTERS_AREA,
                self.tr("Points inside shelter")
            ))
        
        self.addParameter(
            QgsProcessingParameterRasterDestination(
                self.F_XH,
                self.tr('Raster of friction velocity reduction factor (fxh) path'),
            ))
        
        self.addParameter(
        QgsProcessingParameterBoolean(
            name=self.INTERMEDIATE_RASTER,
            description=self.tr('Write intermediate raster'),
            defaultValue=False))  
        
        self.addParameter(
            QgsProcessingParameterFile(
                name=self.OUTPUT_FOLDER,
                description=self.tr('Intermediates results directory path'),
                optional=False,
                behavior = QgsProcessingParameterFile.Folder))

        
    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """

        pr = cProfile.Profile()
        pr.enable()

        # Retrieve the feature source and sink. The 'dest_id' variable is used
        # to uniquely identify the feature sink, and must be included in the
        # dictionary returned by the processAlgorithm function.
        data_hedge = self.parameterAsVectorLayer(parameters,
                                                 self.INPUT,
                                                 context)
        
        wind_orientation = self.parameterAsInt(parameters,
                                               self.WIND_ORIENTATION,
                                               context)
        
        max_extent = self.parameterAsInt(parameters,
                                         self.MAX_EXTENT,
                                         context)
                                     
        height_field_name = self.parameterAsFields(parameters,
                                                   self.HEIGHT_FIELD_NAME,
                                                   context)[0]
        
        porosity_field_name = self.parameterAsFields(parameters,
                                                     self.POROSITY_FIELD_NAME,
                                                     context)[0]
        
        width_field_name = self.parameterAsFields(parameters,
                                                  self.WIDTH_FIELD_NAME,
                                                  context)[0]

        hedge_id_field_name = self.parameterAsFields( parameters,
                                                     self.HEDGE_ID_FIELD_NAME,
                                                     context)[0]
        
        resolution = self.parameterAsDouble(parameters,
                                            self.RESOLUTION,
                                            context)

        intermediate_raster = self.parameterAsBool(parameters,
                                                   self.INTERMEDIATE_RASTER,
                                                   context)
        
        output_folder = self.parameterAsFile(parameters,
                                            self.OUTPUT_FOLDER,
                                            context)
        
        workspace, folder_object = vutils.create_temp_workspace()
        
        if intermediate_raster is False:
            output_folder = workspace

        fxh, shelters_area, pnt_intersect = wf.compute_windbreak_effect(data_hedge, 
                                                                       wind_orientation, 
                                                                       max_extent,
                                                                       resolution,
                                                                       output_folder,
                                                                       feedback,
                                                                       height_field_name,
                                                                       porosity_field_name,  
                                                                       width_field_name,
                                                                       hedge_id_field_name, 
                                                                       intermediate_raster,
                                                                       parameters["F_XH"])
        
        (sink_pnt, id_pnt) = self.parameterAsSink(parameters,
                                                  self.PNT_IN_SHELTERS_AREA,
                                                  context,
                                                  pnt_intersect.fields(),
                                                  pnt_intersect.wkbType(),
                                                  pnt_intersect.sourceCrs())
        for feature in pnt_intersect.getFeatures():
            sink_pnt.addFeature(feature, QgsFeatureSink.FastInsert)

        (sink_shelter, id_shelter) = self.parameterAsSink(parameters,
                                                        self.SHELTERS_AREA,
                                                        context,
                                                        shelters_area.fields(),
                                                        shelters_area.wkbType(),
                                                        shelters_area.sourceCrs())
        for feature in shelters_area.getFeatures():
            sink_shelter.addFeature(feature, QgsFeatureSink.FastInsert)

        vutils.delete_temp_workspace(folder_object)
        QgsProject.instance().addMapLayer(fxh)

        pr.disable()
        s = io.StringIO()
        sortby = SortKey.CUMULATIVE
        ps= pstats.Stats(pr, stream=s).sort_stats(sortby)
        #output path
        path_profiling = os.path.join (output_folder + "line_profiling.dmp")
        ps.dump_stats(path_profiling)
        return {"SHELTERS_AREA": id_shelter,
                "PNT_IN_SHELTERS_AREA": id_pnt}
