"""
Utility functions to handle vector data.
"""
from qgis.core import (QgsVectorLayer,
                       QgsVectorFileWriter, 
                       QgsWkbTypes,
                       QgsProject,
                       QgsField)
import tempfile
import os
from hedge_tools.utils.vector import qgis_wrapper as qw


def create_temp_workspace(prefix="ht_"):
    """
    Create a workspace for intermediary files in /tmp folder.

    In case a temp folder of the same name already exists it erase it and
    create a new one.

    Parameters
    ----------
    prefix : str : name of the workspace

    Returns
    -------
    workspace : str :
        Path of the workspace.
    folder_object : TemporaryDirectory object :
        Used to clean up after processing.
    """
    # Init temp folder
    folder_object = tempfile.TemporaryDirectory(prefix=prefix,
                                                ignore_cleanup_errors=True)
    workspace = folder_object.name

    return workspace, folder_object


def delete_temp_workspace(folder_object):
    """
    Delete the workspace for intermediary files in /tmp folder.

    Parameters
    ----------
    folder_object : TemporaryDirectory object
    """
    folder_object.cleanup()


def get_gpkg_path(layer):
    """
    From an open gpkg, get the path and the index of the layer currently used.

    This allows to fetch the correct layer to open in ogr.
    
    Parameters
    ----------
    layer : QgsVectorLayer :
        Any geometry.
    
    Returns
    -------
    path : str
    layer_idx : int
    """
    layer_path = layer.dataProvider().dataSourceUri()
    head_or_tail = layer_path.split("|")
    path = head_or_tail[0]
    name = head_or_tail[1].replace("layername=", "")
    
    # Fetch idx
    base_layer = QgsVectorLayer(path, "temp", "ogr")
    sub_layers = base_layer.dataProvider().subLayers()
    layer_idx = None
    for sub_layer in sub_layers:
        idx, sub_name, _, _, _, _ = sub_layer.split("!!::!!")
        if name == sub_name:
            layer_idx = int(idx)
            
    return path, layer_idx


def check_unique_constraint(layer, idx=0, update=True):
    """
    On a vector layer check if the given field index has unique values.

    If not, update it if specified.
    
    Parameters
    ----------
    layer : QgsVectorLayer
    idx : int :
        Field index. Field must be of type int.
    update : boolean : Default True :
        Compute unique value for the given field if unique constraint is not
        respected.
    
    Returns
    -------
    success : boolean :
        True in case of unique constraint respected
        True if successfully computed
        Else False
    """
    values = [feat[idx] for feat in layer.getFeatures()]
    unique = len(set(values)) - len(values)
    if unique < 0:
        if update:
            attr_map = {f.id(): {idx: f.id()} for f in layer.getFeatures()}
            success = layer.dataProvider().changeAttributeValues(attr_map)
        else:
            success = False
    elif unique == 0:
        success = True
    else:  # Should be impossible hence apocalyptic event must have happened
        success = "Dark overlord has ascended ! \
                   Praise him for he brings eternal suffering !"
        
    return success


def update_unique_constraint(feedback, layer, idx=0, update=True):
    """
    On a vector layer check if the given field index has unique values.

    If not update it if specified
    If update failed it will use field_calculator to compute a
    new layer with correct field value and check again.
    If this failed too, push an error to user.
    Algorithm should stop with an empty return after 
    this function call in case of error

    Parameters
    ----------
    feedback : QgsFeedback
    layer : QgsVectorLayer
    idx : int :
        Field index. Field must be of type int
    update : boolean :
        Compute unique value for the given field if unique constraint is not
        respected. (Default value = True)

    Returns
    -------
    success : boolean :
        True in case of unique constraint respected
        True if successfully computed
        Else False
    output : boolean/QgsVectorLayer :
        True if field calculator is not used, else return new layer.
    """
    success = check_unique_constraint(layer, idx, update)
    if success is False:  # Create a new layer
        output = qw.field_calculator(layer, "fid", 1, "$id")
        success = check_unique_constraint(output, idx, update)
        if success is False:
            feedback.pushWarning("Could not compute unique fid value \
                                  after converting to single part. \
                                  Please convert to single part \
                                  and compute fid manually.")
    else:
        output = True

    return success, output


def write_layer(inlayer, geom_type, copy_feat=False, request=None,
                copy_field=False, data_provider="ogr", path="",
                driver_name="GPKG"):
    """
    Compute empty vector layer with same crs as input Layer.

    By default, it copies the input geometry, but we can also change it.
    It can also copy the data provider (attribute table) and the features.
    If more than circa 500 features are to be copied you have to write it on
    hard drive

    Parameters
    ----------
    inlayer : (QgsVectorMap) :
        Layer to copy the attributes,geometry type and crs.
    geom_type : (QgsWkbTypes) :
        QgsWkbTypes... See https://qgis.org/pyqgis/3.0/core/Wkb/QgsWkbTypes.html
    copy_feat : (bool) default = False :
        If True copy the features in the new layer.
    request : QgsFeatureRequest :
        If copy_feat is True then you can specify a request to select part of
        the feature to copy.
    copy_field : bool :
        If True copy the fields in the new layer. (Default value = False)
    data_provider : str:
        DataProvider supported by Qgis. (Default value = "ogr")
    path : str:
        (Default value = "")
    driver_name : str :
        ESRI Shapefile is also commonly used. (Default value = "GPKG")

    Returns
    -------
    out_layer (QgsVectorMap) :
        New empty layer.

    TODO : MAKE IT WORK
    """
    save_options = QgsVectorFileWriter.SaveVectorOptions()
    save_options.driverName = driver_name
    save_options.fileEncoding = "UTF-8"

    transform_context = QgsProject.instance().transformContext()
    if copy_field:
        fields = inlayer.fields()

        writer = QgsVectorFileWriter.create(
            path,
            fields,
            geometryType=geom_type,
            srs=inlayer.crs(),
            transformContext=transform_context,
            options=save_options
        )

    else:
        writer = QgsVectorFileWriter.create(
            path,
            geometryType=geom_type,
            srs=inlayer.crs(),
            transformContext=transform_context,
            options=save_options
        )

    if copy_feat:
        if request is None:
            iterator = inlayer.getFeatures()
        else:
            iterator = inlayer.getFeatures(request)

        writer.addFeatures(iterator)
        # delete the writer to flush features to disk
        del(writer)

    new_layer = QgsVectorLayer(path, "newLayer", data_provider)

    return new_layer


def create_layer(inlayer, geom_type="", copy_feat=False, request=None,
                 copy_field=False, multi=False, data_provider="memory", path=""):
    """
    Compute empty vector layer with same crs as input Layer.
    By default, it copies the input geometry, but we can also change it.
    It can also copy the attribute table and the features.
    If more than circa 500 features are to be copied you have to write it on
    hard drive. For that use write_layer

    Don't work if layer does not have a fid field in index 0.
     --> Sort of work but move the field data by an index right without moving field name

    Parameters
    ----------
    inlayer : (QgsVectorMap) :
        Layer to copy the attributes,geometry type and crs.
    geom_type : str:
        Point, LineString or Polygon. In case you want a different geometry in
        output. (Default value = "")
    copy_feat : bool :
        If True copy the features in the new layer. (Default value = False)
    request : QgsFeatureRequest :
        If copy_feat is True then you can specify a request to select part of
         the feature to copy.
    copy_field : bool:
        If True copy the fields in the new layer. (Default value = False)
    multi  : bool:
        If True create a multipart geometry layer. (Default value = False)
    data_provider : str:
        DataProvider supported by Qgis. If not memory then a path is needed.
        (Default value = "memory")
    path : (str) :
        Should be path/to/file/filename.extension
        If no extension a geopackage is created
        Layer name is the file name --> don't handle multi layer geopackage

    Returns
    -------
    out_layer (QgsVectorMap) :
        New empty layer.

    TODO : Transform to a class
    """
    if len(geom_type) > 1:
        if multi:
            layer_geom = "Multi" + geom_type + "?crs="
        else:
            layer_geom = geom_type + "?crs="
    else:
        if multi:
            if inlayer.geometryType() == QgsWkbTypes.PointGeometry:
                layer_geom = "MultiPoint?crs="
            elif inlayer.geometryType() == QgsWkbTypes.LineGeometry:
                layer_geom = "MultiLineString?crs="
            elif inlayer.geometryType() == QgsWkbTypes.PolygonGeometry:
                layer_geom = "MultiPolygon?crs="
        else:
            if inlayer.geometryType() == QgsWkbTypes.PointGeometry:
                layer_geom = "Point?crs="
            elif inlayer.geometryType() == QgsWkbTypes.LineGeometry:
                layer_geom = "LineString?crs="
            elif inlayer.geometryType() == QgsWkbTypes.PolygonGeometry:
                layer_geom = "Polygon?crs="

    layer_crs = inlayer.crs()

    new_layer = QgsVectorLayer(layer_geom + layer_crs.authid(), "newLayer",
                               "memory")

    if data_provider != "memory":
        # Context (crs and other project data)
        transform_context = QgsProject.instance().transformContext()

        # Handle extension and layer creation
        head_or_tail = path.split(".")
        name = os.path.basename(head_or_tail[0])
        if len(head_or_tail) > 1:
            extension = head_or_tail[1]
        else:
            extension = "gpkg"
        
        save_options = QgsVectorFileWriter.SaveVectorOptions()
        save_options.actionOnExistingFile = QgsVectorFileWriter.CreateOrOverwriteFile
        save_options.fileEncoding = "UTF-8"
        save_options.driverName = QgsVectorFileWriter.driverForExtension(extension)

        # Write layer
        QgsVectorFileWriter.writeAsVectorFormatV3(new_layer,
                                                  path,
                                                  transform_context,
                                                  save_options)
        new_layer = QgsVectorLayer(path, name, data_provider)

    attribute_table = new_layer.dataProvider()

    if copy_field:
        if data_provider == "memory":
            for field in inlayer.fields():
                attribute_table.addAttributes([QgsField(field.name(),
                                                        field.type(),
                                                        field.typeName())])
                new_layer.updateFields()
        else:
            for field in inlayer.fields():
                if field.name() != "fid":
                    attribute_table.addAttributes([QgsField(field.name(),
                                                            field.type(),
                                                            field.typeName())])
                    new_layer.updateFields()
                    
    if copy_feat:
        if request is None:
            iterator = inlayer.getFeatures()
        else:
            iterator = inlayer.getFeatures(request)

        feats = [feat for feat in iterator]
        attribute_table.addFeatures(feats)

    return new_layer


def nested_dict_values(dictionary):
    """
    Helper function (generator) to extract all values in a nested dict :
    dict(dict())

    Parameters
    ----------
    dictionary (defaultdict) :
        Nested dictionary from collections library.

    Returns
    -------
    value :
        Value inside the nested dict.
    """
    for value in dictionary.values():
        if isinstance(value, dict):
            yield from nested_dict_values(value)
        else:
            yield value
