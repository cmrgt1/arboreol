"""
Utility functions to handle vector data with geopandas library.
"""
# from typing import List
# import rasterio
# import geopandas as gpd
# from shapely import box


def save_extent_of_raster_as_vector_file(raster_path: str, vector_path: str,
                                         driver: str = "GPKG") -> None:
    """
    Export the extent of a raster in a vector file.
    
    The extent is a rectangular bounding box.

    Parameters
    ----------
    raster_path :
        Raster's path.
    vector_path :
        Vector's path.
    driver :
        (Default value = "GPKG").

    Returns
    -------
    None.

    """
    with rasterio.open(raster_path, "r") as dataset:
        bounds = dataset.bounds
        crs_raster = dataset.crs
        raster_epsg = crs_raster.to_epsg()

    df = gpd.GeoDataFrame({"id": 1, "geometry": [box(*bounds)]})
    print(df.crs)
    df = df.set_crs(epsg=raster_epsg)
    print(df.crs)
    df.to_file(vector_path, driver=driver)
    print(bounds)


def get_tiles_intersecting_with_shapefile(shapefile_path: str,
                                          tiles_path: str) -> List[str]:
    """
    Get the ID of tiles intersecting with a shapefile.
    
    The shapefiles must be in the same coordinates system.

    Parameters
    ----------
    shapefile_path :
        Path of the input shapefile.
    tiles_path :
        Path of the tiles shapefile.

    Returns
    -------
    List of all the ID od the tiles intersecting the input shapefile.

    """
    shapes = gpd.read_file(shapefile_path)
    tiles = gpd.read_file(tiles_path)
    all_shapes = shapes.dissolve()
    selected_tiles = gpd.sjoin(tiles, all_shapes)
    selected_tiles_id = list(selected_tiles.TILES.unique())
    return selected_tiles_id


def multipolygon_to_simple(dataframe: gpd.GeoDataFrame,
                           index_parts: bool = False) -> gpd.GeoDataFrame:
    """
    Convert multi-polygons shapes to single polygons.

    Parameters
    ----------
    dataframe :
        Multi polygons shapes dataframe.
    index_parts :
        If True, the resulting index will be a multi-index (original index with
        an additional level indicating the multiple geometries: a new zero-based
        index for each single part geometry per multi-part geometry)
        (Default value = False)

    Returns
    -------
    Single polygons shapes.

    """
    return dataframe.explode(index_parts=index_parts)
