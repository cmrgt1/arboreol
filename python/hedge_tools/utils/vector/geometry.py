"""
# TODO Add docstring.
"""
import math
import os
from itertools import combinations

import processing
from qgis.PyQt.QtCore import (QVariant)
from qgis.core import (QgsVectorLayer,
                       QgsField,
                       QgsWkbTypes,
                       QgsGeometry,
                       QgsGeometryUtils,
                       QgsPoint,
                       QgsPointXY,
                       QgsLineString,
                       QgsFeature,
                       QgsSpatialIndex,
                       QgsFeatureRequest,
                       QgsVectorFileWriter,
                       NULL)
from qgis.core.additions.edit import edit
import os

from hedge_tools.utils.vector import attribute_table as at
from hedge_tools.utils.vector import qgis_wrapper as qw
from hedge_tools.utils.vector import utils


def check_valid_geom(inlayer, crs):
    """
    Check if the geometry is valid.

    If not, try to repair it with 0 distance buffer and fixgeometry algorithm
    or delete it if it is a null geometry.

    Parameters
    ----------
    inlayer : (QgisObject : QgsVectorLayer) :
        Layer path or QGISLayerObject
    crs :
        Coordinate reference system.

    Returns
    -------
    out_layer (QgisObject : QgsVectorLayer) :
        Layer path or QGISLayerObject.
    out_error (QgisObject : QgsVectorLayer) :
        Layer path or QGISLayerObject of incorrect geom if they can't be repaired.
    """

    # Check validity
    alg_name = "qgis:checkvalidity"
    params = {"INPUT_LAYER": inlayer,
              "METHOD": 2,  # 1:QGIS, 2:GEOS
              "IGNORE_RING_SELF_INTERSECTION": True,
              "VALID_OUTPUT": "TEMPORARY_OUTPUT",
              "INVALID_OUTPUT": "TEMPORARY_OUTPUT"}
    result = processing.run(alg_name, params)
    invalid_count = result["INVALID_COUNT"]
    invalid_output = result["INVALID_OUTPUT"]
    valid_output = result["VALID_OUTPUT"]

    if invalid_count:
        # Remove null geom
        alg_name = "native:removenullgeometries"
        params = {"INPUT": invalid_output,
                  "REMOVE_EMPTY": True,
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        no_null_layer = processing.run(alg_name, params)["OUTPUT"]

        # Fix geometries
        alg_name = "native:fixgeometries"
        params = {"INPUT": no_null_layer,
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        fix_layer = processing.run(alg_name, params)["OUTPUT"]
        del(no_null_layer)

        # Check validity again
        alg_name = "qgis:checkvalidity"
        params = {"INPUT_LAYER": fix_layer,
                  "METHOD": 2,  # 1:QGIS, 2:GEOS
                  "IGNORE_RING_SELF_INTERSECTION": True,
                  "VALID_OUTPUT": "TEMPORARY_OUTPUT",
                  "INVALID_OUTPUT": "TEMPORARY_OUTPUT"}
        result = processing.run(alg_name, params)
        invalid_count2 = result["INVALID_COUNT"]

        alg_name = "native:mergevectorlayers"
        params = {"LAYERS": [valid_output, fix_layer],
                  "CRS": crs,
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        merge_layer = processing.run(alg_name, params)["OUTPUT"]

        return merge_layer, invalid_count2

    else:
        return inlayer, invalid_count


def update_arc_and_identifiers(node_layer, line_layer):
    """
    Create new lines given nodes and update pk and fk of arc layer.
    This algorithm is the previous topological_arc tools.

    Parameters
    ----------
    node_layer : QgsVectorLayer :
        Point.
    line_layer : QgsVectorLayer :
        LineString.

    Returns
    -------
    output : QgsVectorLayer :
        LineString.

    TODO : Could be optimised to do it with graph theory but no time
    """
    workspace, _ = utils.create_temp_workspace()

    line_dissolve = qw.dissolve(line_layer)
    # fid make grass bug
    # Get fields of input layer
    input_field_list = line_dissolve.fields().names()
    # Remove fid if present
    if "fid" in input_field_list:
        with edit(line_dissolve):
            idx = line_dissolve.fields().indexFromName("fid")
            line_dissolve.deleteAttribute(idx)
        input_field_list.remove("fid")

    # Build arcs
    output_temp = os.path.join(workspace + "temp_net.gpkg")

    alg_name = "grass7:v.net"
    params = {
        "input": line_dissolve,
        "points": node_layer,
        "operation": 1,  # connect
        "-s": True,
        "output": output_temp
    }
    output = processing.run(alg_name, params)["output"]
    del(line_dissolve)

    # Load grass output
    layer_temp = QgsVectorLayer(output, "temp", "ogr")

    alg_name = "native:deletecolumn"
    params = {
        "INPUT": layer_temp,
        "COLUMN": "cat",
        "OUTPUT": "TEMPORARY_OUTPUT"
    }
    arcs_layer = processing.run(alg_name, params)["OUTPUT"]

    # Deletion of temporary output from v.net
    layer_temp = None
    QgsVectorFileWriter.deleteShapeFile(output_temp)

    # We compute fid again since we regrouped the lines earlier
    alg_name = "qgis:fieldcalculator"
    params = {
        "INPUT": arcs_layer,
        "FIELD_NAME": "eid",
        "FIELD_TYPE": 1,  # 0: float, 1: int
        "FIELD_LENGTH": 7,
        "FIELD_PRECISION": 0,
        "FORMULA": "$id",
        "OUTPUT": "TEMPORARY_OUTPUT"
    }
    arcs_layer = processing.run(alg_name, params)["OUTPUT"]

    # Get id_node to arcs
    for fld in range(1, 3):
        alg_name = "qgis:fieldcalculator"
        params = {
            "INPUT": arcs_layer,
            "FIELD_NAME": "vid_%d" %fld,
            "FIELD_TYPE": 1,  # 0: float, 1: int
            "FIELD_LENGTH": 7,
            "FIELD_PRECISION": 0,
            "FORMULA": "NULL",
            "OUTPUT": "TEMPORARY_OUTPUT"
        }
        arcs_layer = processing.run(alg_name, params)["OUTPUT"]

    # Build spatial index
    index = QgsSpatialIndex(arcs_layer.getFeatures())

    for node in node_layer.getFeatures():
        # Construct a geometry engine to speed up spatial relationship
        engine = QgsGeometry.createGeometryEngine(node.geometry().constGet())
        engine.prepareGeometry()

        # Get potential neighbour
        candidate_ids = index.intersects(node.geometry().boundingBox())
        request = QgsFeatureRequest().setFilterFids(candidate_ids)

        for arc in arcs_layer.getFeatures(request):
            # Get real neighbour
            if engine.intersects(arc.geometry().constGet()):
                # Fill the fk_Id_node
                for fld in range(1, 3):
                    if arc["vid_%d" %fld] == NULL:
                        with edit(arcs_layer):
                            arc["vid_%d" %fld] = node["vid"]
                            arcs_layer.updateFeature(arc)
                        break
    
    return arcs_layer


def get_isolated_line(poly_layer, arc_layer, method=0):
    """
    Test an arc against all the arcs in the parent polygon.
    If arc to be tested intersects no other arc it'll be considered as an error.
    If arc is connected but short it'll be considered as an error.

    Parameters
    ----------
    poly_layer : QgsVectorLayer : Polygon :
        Median axis parent polygon layer.
    arc_layer : QgsVectorLayer : LineString :
        Current median axis layer.
    method: int:
        0 for voronoi method and 1 for thin method.(Default value = 0)

    Returns
    -------
    del_list : ite[id] :
        List of features id to be deleted.
    feat_list : ite[QgsFeature] :
        List of features which returned an error.
    """
    del_list = []
    feat_list = []

    for poly in poly_layer.getFeatures():
        # Thin method produce arc that is not geos contained inside a polygon.
        # Clip, intersection, snapping does not resolve issues
        # so i use a small buffer to detect error.
        if method:
            ccounts, cfeatures = get_clementini(arc_layer,
                                                poly.geometry().buffer(0.1, 5),
                                                predicate="contains")
        else:
            ccounts, cfeatures = get_clementini(arc_layer,
                                                poly,
                                                predicate="contains")
        if ccounts > 1:
            id_list = []
            len_list = []
            ids = [arc.id() for arc in cfeatures]
            for arc in cfeatures:
                # Remove curr id from ids
                ids_copy = ids.copy()
                ids_copy.remove(arc.id())
                # Test intersection with other ids
                request = QgsFeatureRequest().setFilterFids(ids_copy)
                icounts, ifeatures = get_clementini(arc_layer, arc, request)
                # No intersection and multiple arc in the poly
                if icounts == 0 and len(ids) > 1:
                    id_list.append(arc.id())
                    len_list.append(arc.geometry().length())
                # Connected but small we delete it (probably a small network
                # unconnected from main arc) = type 2 error
                elif icounts > 1 and arc.geometry().length() < 5:
                    # if small segment but directly connected to a big one then we keep it
                    features_len = [arc.geometry().length() for arc in ifeatures]
                    if all(length < 5 for length in features_len):
                        del_list.append(arc.id())
                        feat_list.append(arc)

            # If multiple line not intersecting with each over then delete the shortest one = type 1 error
            if len(id_list) > 1:
                idx = len_list.index(max(len_list))
                id_list.pop(idx)
                del_list.extend(id_list)
                feat_list.extend([arc for arc in cfeatures if arc.id() in id_list])
            # To prevent deleting main median axis if there is type 2 error in the same polygon we add a length threshold
            elif len(id_list) == 1 and len_list[0] < 25:
                del_list.append(id_list[0])
                feat_list.append(arc_layer.getFeature(id_list[0]))

    return del_list, feat_list


def get_angle(innode_point, ingeom1, ingeom2, intern_angle=True):
    """
    Compute the angle of the node formed by the two segments in parameter

    Parameters
    ----------
    innode_point (QgisObject) :
        QgsPointXY or QgsPoint.
    ingeom1 (QgisObject) : QgsGeometry  :
        LineString or QgsPoint.
    ingeom2 (QgisObject) : QgsGeometry :
        LineString or QgsPoint.
    intern_angle (boolean) :
        Return intern angle.
        If angle > 180 clockwise it returns the anticlockwise angle.
        (Default value = True)
    Returns
    -------
    angle_deg (int) :
        Angle of the input node.
    """
    # Get coordinates from QgisObject
    try:
        if innode_point.wkbType() == QgsWkbTypes.Point:
            node_coords = innode_point
    except AttributeError:
        node_coords = QgsPoint(innode_point)

    # If type exist then --> QgsGeometry
    try:
        if ingeom1.type() == QgsWkbTypes.LineGeometry \
                and ingeom2.type() == QgsWkbTypes.LineGeometry:

            # Get points coords which are not nodeCoords as a list
            other_coords1 = [QgsPoint(pnt) for pnt in ingeom1.asPolyline()
                             if pnt != innode_point][0]
            other_coords2 = [QgsPoint(pnt) for pnt in ingeom2.asPolyline()
                             if pnt != innode_point][0]

        elif ingeom1.type() == QgsWkbTypes.PointGeometry \
                and ingeom2.type() == QgsWkbTypes.PointGeometry:

            # Make QgsPoint from QgsGeometry
            other_coords1 = QgsPoint(ingeom1.asPoint())
            other_coords2 = QgsPoint(ingeom2 .asPoint())

    # Except type not exist --> QgsPoint or other class. We need to call wkbType()
    except AttributeError:
        if ingeom1.wkbType() == QgsWkbTypes.Point \
                and ingeom2.wkbType() == QgsWkbTypes.Point:

            # No need to transform object instance
            other_coords1 = ingeom1
            other_coords2 = ingeom2

    # Compute angle formed by the points
    angle_rad = QgsGeometryUtils.angleBetweenThreePoints(
        other_coords1.x(), other_coords1.y(),
        node_coords.x(), node_coords.y(),
        other_coords2.x(), other_coords2.y())

    angle_deg = math.degrees(angle_rad)
    if intern_angle and angle_deg > 180:
        angle_deg = 360 - angle_deg

    return angle_deg


def distance_from_extremities(arc, pnt):
    """
    Return distance from start and end of the line given a QgsPointXY
    on the line or polyline.

    Parameters
    ----------
    arc : QgisObject : QgsVFeature: LineString or PolyLineString
        Linear feature.
    pnt : QgisObject : QgsPointXY
        Point to compute distance from.

    Returns
    -------
    dist_start : int
       Distance between start of the line and point.
    dist_end : int
        Distance between end of the line and point.
    """
    # Get vertex id of the node
    vertex_id = arc.geometry().closestVertex(pnt)[1]

    dist_start = arc.geometry().distanceToVertex(vertex_id)
    dist_end = arc.geometry().length() - dist_start

    return dist_start, dist_end


def make_cutline(inpoly_layer, inarc_layer, innode_layer, inrequest=None):
    """
    Build cutline at bissector angle between 2 arcs (at nodes).
    The request allow to select nodes where we want to build cutline.

    Parameters
    ----------
    inpoly_layer : QgisObject : QgsVectorLayer: Polygon
        Layer containing polygon representation of hedges.
    inarc_layer : QgisObject : QgsVectorLayer : LineString
        Layer containing lines which represents hedges.
    innode_layer : QgisObject : QgsVectorLayer : Node
        Layer containing nodes which represents hedges limits
    inrequest : QgsFeatureRequest : Any
        Request allowing to select nodes to be treated.
        If none all the nodes will be selected.

    Returns
    -------
    outcutline : QgsVectorLayer : LineString
        Cutline to be validated.
    """
    # Init cutLine layer
    outcutline = utils.create_layer(inarc_layer)
    pr = outcutline.dataProvider()
    pr.addAttributes([QgsField("vid", QVariant.Int),
                      QgsField("pid", QVariant.Int),
                      QgsField("theta", QVariant.Double),
                      QgsField("length", QVariant.Double),
                      QgsField("isValid", QVariant.Bool)])
    outcutline.updateFields()
  
    # Create spatial index
    index_line = QgsSpatialIndex(inarc_layer.getFeatures())

    # Check if request
    if inrequest is None:
        nodes = innode_layer.getFeatures()
    else:
        nodes = innode_layer.getFeatures(inrequest)

    # Start computing angle for cut line
    for node in nodes:
        # Init dict that store arc id and the closest vertex to the node
        coord_vertex_dict = {}
        angle_list = []
        # Init nested dict storing line id couple and their angle.
        cutline_angle_list = []
        # Construct a geometry engine to speed up spatial relationship

        engine = QgsGeometry.createGeometryEngine(node.geometry().constGet())
        engine.prepareGeometry()

        # Get parent polygon
        _, parent_poly_list = get_clementini(inpoly_layer, node)

        # Get potential neighbour. We can replace by selection on fk_vid if necessary
        candidate_ids = index_line.intersects(node.geometry().boundingBox())
        request = QgsFeatureRequest().setFilterFids(candidate_ids)

        for arc in inarc_layer.getFeatures(request):
            # Get real neighbour
            if engine.intersects(arc.geometry().constGet()):
                # Get and store arc.id() and closest vertex to node
                if arc.geometry().closestVertex(node.geometry().asPoint())[2] >= 0:
                    # If it's the end of the line get the previous vertex
                    next_vertex = arc.geometry().closestVertex(node.geometry().asPoint())[2]
                else:
                    # If it's the start get the next vertex
                    next_vertex = arc.geometry().closestVertex(node.geometry().asPoint())[3]
                coord_vertex_dict[arc.id()] = arc.geometry().vertexAt(next_vertex)  # QgsPoint Geom

        # Get anchor point (East from node because theta computation
        # start from right of the circle center)
        anchor_pnt = QgsPoint(node.geometry().asPoint().x() + 1, node.geometry().asPoint().y())

        # Angle computation from East anchor (anticlockwise)
        for item in coord_vertex_dict:
            # Get angle between east point and segment (anticlockwise)
            angle_from_anchor = get_angle(node.geometry().asPoint(),
                                          anchor_pnt, coord_vertex_dict[item],
                                          intern_angle=False)
            angle_list.append(360 - angle_from_anchor)

        # Compute difference to get angle between adjacent segment (anticlockwise)
        angle_list.sort(reverse=True)
        # Get segment angle difference anticlockwise because cutLine
        # computation is anticlockwise (cosine(theta)*coord + length)
        for idx in range(0, len(angle_list)):
            if idx < len(angle_list) - 1:
                # Get half the angle between adjacent segment and add angle
                # with the lowest one to have the angle from East
                cutline_angle_list.append(((angle_list[idx + 1] - angle_list[idx])/2)
                                            + angle_list[idx])
            else:
                # If it's the last one we have to compare him with the first
                # and get the inverse (acute angle), halve it then add the distance from East
                cutline_angle_list.append(angle_list[idx]
                                          + ((360 - (angle_list[idx]
                                          - angle_list[0]))/2))
            # Just for visualisation purpose if the angle > 360° substract 360
            if cutline_angle_list[idx] >= 360:
                cutline_angle_list[idx] -= 360
        # Compute "bissector" angle, draw a line, get intersection with parent polygon
        # Construct polyline node --> intersection point for each "bissector" angle

        # Compute a point with the cutLine angle to draw the cut line
        x1 = node.geometry().asPoint().x()
        y1 = node.geometry().asPoint().y()

        for angle in cutline_angle_list:
            # Transform to radian to avoid special case (90°,180°,...)
            x2 = x1 + 200 * math.cos(math.radians(angle))
            y2 = y1 + 200 * math.sin(math.radians(angle))
            new_line = QgsGeometry.fromPolyline([QgsPoint(node.geometry().asPoint()),
                                                QgsPoint(x2, y2)])
            for poly in parent_poly_list:
                if new_line.intersects(poly.geometry()):
                    # Get the closest intersection with parent polygon
                    # Turn this into geometry collection allow selection of first intersection
                    # if it turns to be intersecting several times and becoming a multipolyline
                    closest_inter = new_line.intersection(poly.geometry()).asGeometryCollection()[0]
                    # Build line and store it in new Layer
                    feature = QgsFeature()
                    feature.setGeometry(QgsGeometry(closest_inter))
                    feature.setAttributes([node["vid"],
                                           poly["pid"],
                                           angle,
                                           QgsGeometry(closest_inter).length(),
                                           False])  # node["Nb_seg"]
                    pr.addFeature(feature)
                    outcutline.updateExtents()

    return outcutline


def make_cutline_from_itf(inpoly_layer, inarc_layer, itf_layer,
                          ori_bound=75):
    """
    This function create cutline from an estimated border point
    on the hedge which is closest to the junction between adjacent interface.
    Then we link this point to the median axis and the ovther side of the hedge.

    Parameters
    ----------
    inpoly_layer : QgisObject : QgsVectorLayer: Polygon
        Layer containing polygon representation of hedges.
    inarc_layer : QgisObject : QgsVectorLayer : LineString
        Layer containing lines which represents hedges.
    itf_layer : QgisObject : QgsVectorLayer : Polygon
        Interface layer.
    ori_bound : int:
        Define lower and upper orientation bound to allow splitting.

    Returns
    -------
    outcutline : QgsVectorLayer : LineString
        Cutline to be validated.
    """
    # Creating and formating output layer
    outcutline = utils.create_layer(inarc_layer)
    at.create_fields(outcutline, [("id_cut", QVariant.Int)])

    # Init variable
    id = 1
    feat_list = []

    hedges = inpoly_layer.getFeatures()
    for hedge in hedges:
        # Fetch parent arc
        expression = "pid = %d" % hedge["pid"]
        request = QgsFeatureRequest().setFilterExpression(expression)
        arc = next(inarc_layer.getFeatures(request))
        # Get adjacent interface
        _, i_list = get_clementini(itf_layer, hedge.geometry())
        boundary_list = []
        for feat in i_list:
            geom = feat.geometry()
            # Check intersection geometry type
            if geom.type() == QgsWkbTypes.LineGeometry:
                boundary_list.append(QgsGeometry().fromPolyline(geom))
            elif geom.type() == QgsWkbTypes.PolygonGeometry:
                if geom.isMultipart():
                    for poly in geom.asGeometryCollection():
                        boundary_list.extend(
                            [QgsGeometry().fromPolyline(g) for g in
                             get_boundary(poly, longest=False)])
                else:
                    boundary_list.extend([QgsGeometry().fromPolyline(g) for g in
                                          get_boundary(geom, longest=False)])
        # Get intersection between interface boundary and hedges
        line_list = []
        for line in boundary_list:
            if line.intersects(hedge.geometry()):
                i_line = line.intersection(hedge.geometry())
                if i_line.isMultipart():
                    for l in i_line.asGeometryCollection():
                        line_list.append(l)
                else:
                    line_list.append(i_line)

        # Orientation criteria to create a cutline
        azi_hedge = hedge.geometry().orientedMinimumBoundingBox()[2]
        ori_hedge = azi_hedge if azi_hedge >= 0 else 360 + azi_hedge

        for line in line_list:
            # Oritentation validation
            azi = line.orientedMinimumBoundingBox()[2]
            ori = azi if azi >= 0 else 360 - azi
            # Bounds delimitation
            lower = ori - ori_bound  # deprecated
            upper = ori + ori_bound  # deprecated

            if ori_hedge < lower or ori_hedge > upper: # deprecated
            # if 90 - ori_bound <= abs(ori_hedge - ori) <= 90 + ori_bound: #ori_bou_nd = 15 by default
                # Get a cutline that take the hedge's width
                ext_line = line.extendLine(120, 120)
                cutline = hedge.geometry().intersection(ext_line)
                if cutline.intersects(arc.geometry()):
                    if cutline.isMultipart():
                        dist_min = 9999
                        for g in cutline.asGeometryCollection():
                            if g.intersects(arc.geometry()):
                                if g.distance(line) < dist_min:
                                    geom = g
                                    dist_min = g.distance(line)
                        cutline = geom

                    # Create and add feature to feat_list
                    feat = QgsFeature()
                    feat.setGeometry(cutline)
                    feat.setAttributes([id])

                    feat_list.append(feat)
                    id += 1

    # Add feature to cutline's layer
    outcutline.dataProvider().addFeatures(feat_list)
    outcutline.updateExtents()

    return outcutline


def validate_cutline_from_itf(inarc_layer, innode_layer, incutline,
                              ignore_dist=25,
                              node_type_field="Node_type"):
    """
    Validation of cutline created from interface change :
        distance between cutlines and distance between cutline and start/end

    Parameters
    ----------
    inarc_layer : QgisObject : QgsVectorLayer : LineString
        Layer containing lines which represents hedges.
    innode_layer : QgisObject : QgsVectorLayer : Node
        Layer containing nodes which represents hedges limits.
    incutline : QgisObject : QgsVectorLayer : LineString
        Cutline layer.
    ignore_dist : int:
        If different from 0, cancel the splitting if below the value.
        (Default value = 25)
    node_type_field : str
        Name of the field that store the topologic type of the nodes.

    Returns
    -------
    outcutline : QgsVectorLayer : LineString
        Validated cutline .
    outnode_layer : QgsVectorLayer : Point
    """
    # Init variable
    node_list = []  # Node that will be added at the end
    line_remove_list = []  # Cutline that'll be removed at the end

    # Create new point in node layer
    for arc in inarc_layer.getFeatures():
        # Init variable
        feat_dict = {}  # Node with a key that represents the id_cut from his cutline
        node_remove_list = []  # Node that'll be removed from node_list after each hedge
        # Get all the cutline of this hedge
        _, i_list = get_clementini(incutline, arc.geometry())
        arc_geom = arc.geometry()
        for f in i_list:
            # Get point intersecting arc and cutline
            inter = arc.geometry().intersection(f.geometry())

            if inter.isMultipart():
                # If it's multipart there is an error, so we skip it
                # as the hedge surely have an abonormal shape
                line_remove_list.append(f.id())
            else:
                # We check for distance to end/start of arc
                ma_pnt = inter.asPoint()

                arc_geom = vertex_add(arc_geom, ma_pnt.x(), ma_pnt.y())
                arc_feat = QgsFeature()
                arc_feat.setGeometry(arc_geom)
                dist_start, dist_end = distance_from_extremities(arc_feat,
                                                                    ma_pnt)
                if dist_start < ignore_dist or dist_end < ignore_dist:
                    # If too close of an extremities, ignore it
                    del(ma_pnt)
                    line_remove_list.append(f.id())
                    continue
                # Adding the node and his associated cutline id
                feat_dict[f["id_cut"]] = ma_pnt
        if len(i_list) > 1:
            for k_1, k_2 in combinations(feat_dict, 2):
                geom_1 = feat_dict[k_1]
                geom_2 = feat_dict[k_2]

                arc_geom = vertex_add(arc_geom, geom_1.x(), geom_1.y())
                arc_geom = vertex_add(arc_geom, geom_2.x(), geom_2.y())
                arc_feat = QgsFeature()
                arc_feat.setGeometry(arc_geom)
                # Check for distance between them
                dist_start_1, _ = distance_from_extremities(arc_feat,
                                                                   geom_1)
                dist_start_2, _ = distance_from_extremities(arc_feat,
                                                                   geom_2)
                # If ignore dist, then delete hedge < ignore dist.
                if abs(dist_start_2 - dist_start_1) < ignore_dist:
                    expression = "id_cut = %d or id_cut = %d" % (k_1, k_2)
                    request = QgsFeatureRequest().setFilterExpression(
                        expression)
                    cutline = [c for c in incutline.getFeatures(request)]
                    # Quick fix to match k_1 with cutline_1 and other way around
                    if cutline[0]["id_cut"] == k_1:
                        cut_1, cut_2 = cutline[0], cutline[1]
                    else:
                        cut_1, cut_2 = cutline[1], cutline[0]
                    # If two are closes check for the shortest one and delete the other
                    if cut_1.geometry().length() <= cut_2.geometry().length():
                        line_remove_list.append(cut_2.id())
                        node_remove_list.append(k_2)
                    else:
                        line_remove_list.append(cut_1.id())
                        node_remove_list.append(k_1)

            # Delete the unwanted nodes from feat_dict and transform it to a list
            node_remove_list = list(set(node_remove_list))
            for v in node_remove_list:
                feat_dict.pop(v)
        node_list += [v for v in list(feat_dict.values())]

    # Remove all the unwanted cutlines
    line_remove_list = list(set(line_remove_list))
    incutline.dataProvider().deleteFeatures(line_remove_list)
    incutline.updateFields()

    # Adding nodes to node_layer and recomputing vid/fid
    idx_node_id = innode_layer.fields().indexFromName("vid")
    idx_node_fid = innode_layer.fields().indexFromName("fid")

    max_id = innode_layer.maximumValue(idx_node_id) + 1
    max_fid = innode_layer.maximumValue(idx_node_fid) + 1
    feat_list = []
    for f in node_list:
        # Add new node
        new_node = QgsFeature()
        new_node.setGeometry(QgsGeometry().fromPointXY(f))
        new_node.setFields(innode_layer.fields())

        new_node["fid"] = max_fid
        new_node["vid"] = max_id
        new_node["Degree"] = 2
        new_node[node_type_field] = "L"
        max_fid += 1
        max_id += 1
        feat_list.append(new_node)

    innode_layer.dataProvider().addFeatures(feat_list)
    innode_layer.updateExtents()

    return incutline, innode_layer


def validate_cutline(incutline, inarc_layer, inpoly_layer):
    """
    Function to validate the cutLine.
    First it checks if the cutLines intersects with another node cut line
    If True it snaps the current cutLine to the end of the intersected cutline
    and then, in all cases, cut it by parent polygon boundary
    Then it checks the initial condition of the current cutLine
    (length and intersection with hedges).
    Finally it makes theta variation with a threshold until:
    - his length is minimized;
    - it doesn't intersect with an hedgeLine;

    CutLine hierarchisation is based on length.
    The longest one is the first to be treated.
    
    Parameters
    ----------
    incutline: QgisObject : QgsVectorLayer : LineString
        Layer containing the cutLine.
    inarc_layer: QgisObject : QgsVectorLayer : LineString
        Layer containing lines which represents hedges.
    inpoly_layer : QgisObject : QgsVectorLayer: Polygon
        Layer containing polygon representation of hedges.
        
    Returns
    -------
    outcutline: QgisObject : QgsVectorLayer : LineString
        Input layer containing the modified and validated cutLine.
    """
    # Get index of interest fields
    idx_theta = incutline.fields().indexFromName("theta")
    idx_lgth = incutline.fields().indexFromName("length")
    idx_valid = incutline.fields().indexFromName("isValid")

    # Get an unvalidated line. Start with the longest one
    req_is_valid = QgsFeatureRequest().setFilterExpression("isValid is False")
    clause = QgsFeatureRequest.OrderByClause("length", ascending=False)
    order_by = QgsFeatureRequest.OrderBy([clause])
    req_is_valid.setOrderBy(order_by)

    for line_to_valid in incutline.getFeatures(req_is_valid):
        # Get start and end node
        start_pnt = QgsPoint(line_to_valid.geometry().asPolyline()[0])
        end_pnt = QgsPoint(line_to_valid.geometry().asPolyline()[1])

        start_geom = QgsGeometry(start_pnt)

        # In case we already built topology we can use it to speed up the algorithm
        if "pid" in incutline.fields().names():
            expression = "pid = %d"%line_to_valid["pid"]
            request = QgsFeatureRequest().setFilterExpression(expression)
            parent_poly = next(inpoly_layer.getFeatures(request))
        else:
            # Get parent polygon with start node to get the real one
            # (in case long cutline to another polygon border)
            _, parent_poly_list = get_clementini(inpoly_layer, start_geom)
            parent_poly = parent_poly_list[0]

        # Construct a new "start point" close the original to avoid
        # false intersection detection with node hedges
        # or real intersection with node hedges down the arc.
        fake_start_pnt = QgsGeometryUtils.interpolatePointOnLine(start_pnt, end_pnt,
                                                                 0.01)
        fake_line_to_valid = QgsGeometry.fromPolyline([fake_start_pnt, end_pnt])
        # Get cutlines intersection count and list with fake line to valid
        count_cutlines, cut_inter_list = get_clementini(incutline, fake_line_to_valid)
        # If there is an intersection with other nodes cutlines. Snap lineToValid
        # to end node other cut line and cut it by parentpoly boundary if it intersects
        if count_cutlines > 1:
            cut_inter_list = [f for f in cut_inter_list if f.id() != line_to_valid.id()]

            # Get closest from start node
            dist = 9999
            for feat in cut_inter_list:
                closer = QgsGeometry(QgsPoint(line_to_valid.geometry().asPolyline()[0])).distance(feat.geometry())
                if closer < dist:
                    dist = closer
                    cut_inter = feat
                    # id = feat.id()

            # Get intersected cutLine
            # cut_inter = next(incutline.getFeatures(QgsFeatureRequest().setFilterFid(id)))
            # Get cutLine end point
            new_end_pnt = QgsPoint(cut_inter.geometry().asPolyline()[1])
            # Construct new lineToValid
            new_line_to_valid = QgsGeometry.fromPolyline([start_pnt, new_end_pnt])
            # Cut by parent polygon boundary
            inter_list = new_line_to_valid.intersection(parent_poly.geometry()).asGeometryCollection()
            if len(inter_list) == 1:
                closest_inter = inter_list[0]
            elif len(inter_list) > 1:
                for inter in inter_list:
                    if inter.type() == QgsWkbTypes.LineGeometry:
                        closest_inter = inter
                        break
            new_cutline = QgsFeature()
            new_cutline.setGeometry(QgsGeometry(closest_inter))
            # Get the new theta
            anchor_pnt = QgsPoint(start_pnt.x() + 1, start_pnt.y())
            # Compute angle from east anchor
            angle_from_anchor = get_angle(start_pnt,
                                          anchor_pnt, new_end_pnt,
                                          intern_angle=False)
            # Transform to anticlockwise
            angle_from_anchor = 360 - angle_from_anchor
            # Edit lineToValid with new geometry, new length, new theta
            with edit(incutline):
                incutline.changeGeometry(line_to_valid.id(),
                                            new_cutline.geometry())
                incutline.changeAttributeValues(line_to_valid.id(),
                                                   {idx_theta: angle_from_anchor,
                                                   idx_lgth: new_cutline.geometry().length()})

            # Get the feature again to "refresh" the change
            line_to_valid = next(incutline.getFeatures(QgsFeatureRequest().setFilterFid(line_to_valid.id())))
        # Build condition validation dict
        is_valid_dict = {"isInterH": True, "length": line_to_valid.geometry().length(),
                         "theta": line_to_valid["theta"]}

        # Angle variation +/- 15° around theta
        # If there is no solution, variation becomes wider
        angle_max = line_to_valid["theta"] + 15
        angle_min = line_to_valid["theta"] - 15
        angle = angle_min
        is_solution = False
        max_ite = 3
        ite = 1
        while angle <= angle_max:
            # Compute the new line
            try:
                x1 = line_to_valid.geometry().asPolyline()[0].x()
                y1 = line_to_valid.geometry().asPolyline()[0].y()
            except TypeError:
                break
            x2 = x1 + 200 * math.cos(math.radians(angle))
            y2 = y1 + 200 * math.sin(math.radians(angle))
            new_line = QgsGeometry.fromPolyline([QgsPoint(x1, y1), QgsPoint(x2, y2)])
            closest_inter = new_line.intersection(parent_poly.geometry()).asGeometryCollection()[0]
            new_cutline = QgsGeometry(closest_inter)
            # Get start and end node
            try:
                start_pnt = QgsPoint(new_cutline.asPolyline()[0])
                end_pnt = QgsPoint(new_cutline.asPolyline()[1])
            except IndexError:
                break
            # Construct a new "start point" close to the original to avoid
            # false intersection detection with node hedges
            # or real intersection with node hedges down the arc.
            fake_start_pnt = QgsGeometryUtils.interpolatePointOnLine(start_pnt,
                                                                     end_pnt,
                                                                     0.1)
            fake_line_to_valid = QgsGeometry.fromPolyline([fake_start_pnt, end_pnt])
            # Get cutlines intersection count and list with fake line to valid
            count_hedge_lines, _ = get_clementini(inarc_layer, fake_line_to_valid)
            # If the two conditions are validated then register it
            if count_hedge_lines == 0 \
                    and new_cutline.length() <= is_valid_dict["length"]:
                is_valid_dict["isInterH"] = False
                is_valid_dict["length"] = new_cutline.length()
                is_valid_dict["theta"] = angle
                is_valid_dict["geom"] = new_cutline
                is_solution = True           

            # If last iteration and still no solution. Widen the theta variation
            # Not optimized because we go through the old variation again. How to exclude it ?
            # Can we loop from angleMin -10 to angle min then angleMax to angleMax +10
            elif angle == angle_max and is_solution is False:
                angle_min -= 10
                angle_max += 10
                angle = angle_min

                if ite == max_ite:
                    is_valid_dict["isInterH"] = False
                    is_valid_dict["length"] = line_to_valid.geometry().length()
                    is_valid_dict["theta"] = line_to_valid["theta"]
                    is_valid_dict["geom"] = line_to_valid.geometry()
                    is_solution = True    
                    break       
 
                ite += 1
            
            angle += 1

        # Snap the new cutLine to the nearest polygon Vertex
        # Problem in some case it snap it to the hedge end node.
        # We have to delete the snap or create a new condition
        # poly_vertex = parent_poly.geometry().closestVertexWithContext(
        #                                     is_valid_dict["geom"].asPolyline()[1])
        # # Convert squared euclidian distance to distance and check if close enough to snap
        # # Small snap value to ensure that our distance minimisation is not canceled
        # if math.sqrt(poly_vertex[0]) < 5:
        #     new_end_point = parent_poly.geometry().vertexAt(poly_vertex[1])
        #     new_cutline = QgsGeometry.fromPolyline([
        #                     QgsPoint(is_valid_dict["geom"].asPolyline()[0]),
        #                     new_end_point])
        #     is_valid_dict["geom"] = new_cutline

        # Build the "optimal" cutline and change isValid field to True
        with edit(incutline):
            incutline.changeGeometry(line_to_valid.id(), is_valid_dict["geom"])
            incutline.changeAttributeValues(line_to_valid.id(),
                                              {idx_theta: is_valid_dict["theta"]
                                               if is_valid_dict["theta"] < 360
                                               else is_valid_dict["theta"] - 360,
                                               idx_lgth: is_valid_dict["length"],
                                               idx_valid: True})

    return incutline


def use_cutline(incutline, inpoly_layer, inarc_layer, innode_layer):
    """
    Perform spatial operation to be able to cut polygons with non linear linestrings
    For that we transform the cutline as polygons with a buffer
    and then retrieve polygons extent with buffer and snaps.
    Finally we also update the relation between arc and poly with a pk and a fk

    Parameters
    ----------
    incutline: QgisObject : QgsVectorLayer : LineString
        Layer containing the cutLine.
    inpoly_layer : QgisObject : QgsVectorLayer: Polygon
        Layer containing polygon representation of hedges.
    inarc_layer: QgisObject : QgsVectorLayer : LineString
        Layer containing lines which represents hedges.
    innode_layer : QgisObject : QgsVectorLayer : Node
        Layer containing nodes which represents hedges limits.
    Returns
    -------
    outpoly_layer : QgisObject : QgsVectorLayer : Polygon
        Cutted polygon with updated pid
    outarc_layer : QgisObject : QgsVectorLayer : LineString
        Updated arc with pid.
    """
    # Buffer cutLine. cutLine at complex nodes (T or more)
    # aren't considered when cutting. Buffering allow performing spatial
    # difference, intersection between polygons,
    # resulting to the same result : individual hedges
    cut_poly = qw.buffer(incutline, 0.0001, True, segments=5)
    del(incutline)

    # Difference between input polygons and buffered cutlines
    hedge_poly = qw.difference(inpoly_layer, cut_poly)
    del(cut_poly)

    # Multiple feature to single feature
    hedge_single = qw.multipart_to_singleparts(hedge_poly)
    del(hedge_poly)

    # Buffering the "cutted" polygons
    hedge_buff = qw.buffer(hedge_single, 0.0001, join_style=1, segments=5)
    del(hedge_single)

    # Repair geometry
    valid_buff, _ = check_valid_geom(hedge_buff, inpoly_layer.sourceCrs())
    del(hedge_buff)

    # Compute spatial index + clip
    hedge_clip = qw.clip(valid_buff, inpoly_layer)
    del(valid_buff)

    # Snap hedgeClip to poly_layer to correct some error caused by the buffer
    hedge_snap = qw.snap(hedge_clip, inpoly_layer, 0.0003, 2)
    del(hedge_clip)

    # Fix topological error caused by the snap
    hedge_valid, _ = check_valid_geom(hedge_snap, inpoly_layer.sourceCrs())

    # Snap hedge_snap with itself to correct O nodes junction
    # alg_name = "native:snapgeometries"
    # params = {"INPUT": hedge_valid,
    #           "REFERENCE_LAYER": innode_layer,
    #           "TOLERANCE": 0.001,
    #           "BEHAVIOR": 2,
    #           "OUTPUT": "TEMPORARY_OUTPUT"}
    # hedge_snap = processing.run(alg_name, params)["OUTPUT"]
    # print(4)
    # # Fix topological error caused by the snap
    # hedge_valid, _ = check_valid_geom(hedge_snap, inpoly_layer.sourceCrs())

    # Compute pid and reload fid to ensure UNIQUE constraint
    # is respected after a dissolve
    field_name_list = ["fid", "pid"]
    for name in field_name_list:
        hedge_valid = qw.field_calculator(hedge_valid, name, 1, 
                                          "$id", 10, 0)

    # Repair geometry of the polygon
    hedge_fix = qw.fix_geometries(hedge_valid)
    del(hedge_valid)
    
    outpoly_layer = qw.multipart_to_singleparts(hedge_fix)
    del(hedge_fix)

    # Repair boundary error
    expression = "$area <= 0.01"
    request = QgsFeatureRequest().setFilterExpression(expression)
    del_feat = [f.id() for f in outpoly_layer.getFeatures(request)]

    outpoly_layer.dataProvider().deleteFeatures(del_feat)
    outpoly_layer.updateExtents()

    # Suboptimal we should compute pid based on eid and not eid from pid.
    
    # Copy input arcs layer
    # outarc_layer = create_layer(inarc_layer, copy_feat=True, copy_field=True)
    # pr = inarc_layer.dataProvider()
    # Create pid field
    # pr.addAttributes([QgsField("pid", QVariant.Int)])
    inarc_layer.updateFields()
    for arc in inarc_layer.getFeatures():
        # Each intersection start/endPoint return an intersection
        _, parent_poly_list = get_clementini(outpoly_layer, arc)
        parent_poly_ids = [poly.id() for poly in parent_poly_list]

        max_len_inter = -1  # In case point intersection (length = 0)
        request = QgsFeatureRequest().setFilterFids(parent_poly_ids)

        # To ensure real intersection we get the longest one
        # and transfer the poly id associated
        for poly in outpoly_layer.getFeatures(request):
            inter_list = arc.geometry().intersection(
                poly.geometry()).asGeometryCollection()
            for seg in inter_list:
                if seg.length() > max_len_inter:
                    max_len_inter = seg.length()
                    parent_poly = poly["pid"]

        with edit(inarc_layer):
            arc["pid"] = parent_poly
            arc["eid"] = parent_poly
            inarc_layer.updateFeature(arc)

    return outpoly_layer, inarc_layer

    # values = []
    # for poly in outpoly_layer.getFeatures():
    #     _, child_arc_list = get_clementini(inarc_layer, poly.geometry().buffer(-0.5, 5))
    #     id = child_arc_list[0].id()
    #     arc = inarc_layer.getFeature(id)
    #     values.append(arc["eid"])
    # outpoly_layer = add_field_at_given_idx(outpoly_layer, "pid", QVariant.Int, 1, values)

    # return outpoly_layer


def get_clementini(inlayer, ingeom, inrequest=None, predicate="intersection"):
    """
    Test a feature spatial relation with another layer features
    or a set of features in a layer.
    Handles intersection, within and contains.
    Function will expand given the needs.

    Parameters
    ----------
    inlayer: QgisObject : QgsVectorLayer : Any geometry
        Layer containingthe features to check intersection against infeature.
    ingeom: QgisObject : QgsFeature/QgsGeometry : Any geometry
        Feature to check intersection against the layer features.
    inrequest: QgsFeatureRequest
        Request to select a set of feature in inlayer if wanted.
    predicate: String : Default : intersection
        Clementini predicate to test
    Returns
    -------
    count_pred : Number of layer features intersecting infeature.
    predicate_list : List of features intersecting infeature.

    TODO : Transform to a class
    """
    # Check if feature or geometry. If feature get geometry
    try:
        geom = ingeom.geometry()
    except AttributeError:
        geom = ingeom

    # Build index
    if inrequest is None:
        index = QgsSpatialIndex(inlayer.getFeatures())
    else:
        index = QgsSpatialIndex(inlayer.getFeatures(inrequest))

    # Get candidates based on bounding box.
    candidate_ids = index.intersects(geom.boundingBox())
    request = QgsFeatureRequest().setFilterFids(candidate_ids)

    # Construct geometry engine.
    engine = QgsGeometry.createGeometryEngine(geom.constGet())
    engine.prepareGeometry()

    # Check real intersection with candidates.
    count_pred = 0
    # Init interList.
    predicate_list = []

    for feat in inlayer.getFeatures(request):
        if predicate == "intersection":
            if engine.intersects(feat.geometry().constGet()):
                count_pred += 1
                predicate_list.append(feat)
        if predicate == "within":
            if engine.within(feat.geometry().constGet()):
                count_pred += 1
                predicate_list.append(feat)
        if predicate == "contains":
            if engine.contains(feat.geometry().constGet()):
                count_pred += 1
                predicate_list.append(feat)
        if predicate == "touches":
            if engine.touches(feat.geometry().constGet()):
                count_pred += 1
                predicate_list.append(feat)

    return count_pred, predicate_list


def remove_special_closed_loops(arc_layer):
    """
    Return an id_list of half of the small closed loops forming a square.
    Those special case are created after r.to.vect when 4 pixels form a square.

    Parameters
    ----------
    arc_layer : QgsVectorLayer :
        LineString.

    Returns
    -------
    del_list : list :
        Ids of halves of closed square loop.
    """
    passed_list = []
    del_list = []

    for line in arc_layer.getFeatures():
        geom = line.geometry()
        if len(geom.asPolyline()) == 3:
            rad = geom.angleAtVertex(1)
            angle = rad * (180 / math.pi)
            # Bissector angle of a square angle --> circa 45°
            if 44 < angle < 46 or 134 < angle < 136 \
                    or 224 < angle < 226 or 314 < angle < 316:
                req = QgsFeatureRequest().setFilterRect(geom.boundingBox())
                _, neighbours = get_clementini(arc_layer, geom)
                potential_halves = [n for n in neighbours if n.id() != line.id() \
                                    and len(n.geometry().asPolyline()) == 3]
                # Majority of case potential halves is only the other half
                for feat in potential_halves:
                    geom = feat.geometry()
                    rad = geom.angleAtVertex(1)
                    angle = rad * (180 / math.pi)
                    # Real other half
                    if (44 < angle < 46 or 134 < angle < 136 \
                        or 224 < angle < 226 or 314 < angle < 316) \
                            and feat.id() not in passed_list:
                        passed_list.append(line.id())
                        del_list.append(feat.id())

    return del_list


def count_line_inside_polygon(arc_layer, poly_layer, workspace,
                              threshold):
    """
    Count line inside polygons and return ids of the one alone in a polygon.
    Return a topologically regrouped LineString layer as well.

    Parameters
    ----------
    arc_layer : QgsVectorLayer :
        LineString.
    poly_layer : QgsVectorLayer :
        Polygon.
    workspace : str :
        Path of the temp folder for temp layer.
    threshold : int :
        Current iteration to name temp layer.

    Returns
    -------
    arc_single : QgsVectorLayer : LineString :
        Topologically regrouped LineString (arc).
    arc_list : list :
        Ids of arc alone in a polygon.
    """
    # Regrouper
    alg_name = "native:dissolve"
    params = {"INPUT": arc_layer,
              "OUTPUT": "TEMPORARY_OUTPUT"}
    arc_dissolve = processing.run(alg_name, params)["OUTPUT"]
    arc_layer = None

    # Single part
    alg_name = "native:multiparttosingleparts"
    params = {"INPUT": arc_dissolve,
              "OUTPUT": workspace + "/clean" + str(threshold) + ".gpkg"}
    arc_single = processing.run(alg_name, params)["OUTPUT"]
    arc_single = QgsVectorLayer(arc_single, "clean_layer", "ogr")

    # iC # not optimal
    arc_list = []
    for poly in poly_layer.getFeatures():
        iC, iF = get_clementini(arc_single, poly)
        if iC == 1:
            arc_list.append(iF[0])

    return arc_single, arc_list


def vertex_add(ingeom, x, y, tol=0.01):
    """
    Add a vertex to a polyline at the given coordinates.

    Parameters
    ----------
    ingeom: QgisObject : QgsGeometry : Any geometry
        Features to be modified.
    x: Latitude coordinates :
        Any latitude.
    y: Longitude coordinates :
        Any longitude.
    tol: float: Any
        Tolerance value defining if the vertex will be added
        in or before/after the polyline.

    Returns
    -------
    outgeom : QgisObject : QgsGeometry : AnyGeometry
        The modified input polyline.

    TODO : Could be a method of a polyline class
    """
    _, at, _, after, _ = ingeom.closestVertex(QgsPointXY(x, y))
    dist, _, to, _ = ingeom.closestSegmentWithContext(QgsPointXY(x, y))

    if at == 0:
        if dist < tol:
            # insert into first segment
            ingeom.insertVertex(x, y, after)
        else:
            # insert before first vertex
            ingeom.insertVertex(x, y, 0)
    elif after == -1:
        if dist < tol:
            # insert after last vertex
            ingeom.insertVertex(x, y, at)
        else:
            # insert into last segment
            last = ingeom.vertexAt(at)
            ingeom.moveVertex(x, y, at)
            ingeom.insertVertex(last.x(), last.y(), at)
    else:
        # insert into any other segment
        ingeom.insertVertex(x, y, to)

    return ingeom


def reverse_polyline(ingeom):
    """
    Reverse the polylines sequence of coordinates.
    Only works for linear polylines (no fork).

    Parameters
    ----------
    ingeom: QgisObject : QgsGeometry : Polyline
        Polyline features to be inversed.

    Returns
    -------
    outgeom : QgisObject : QgsGeometry : Polyline
        Inversed polyline.

    TODO : Could be a method of a polyline class
    """
    nodes = ingeom.asPolyline()
    nodes.reverse()

    outgeom = QgsGeometry.fromPolyline([QgsPoint(node) for node in nodes])

    return outgeom


def get_boundary(polygon, longest=True):
    """
    Compute the boundary of a polygon and return the maximum length boundary.

    Parameters
    ----------
    polygon : QgsGeometry :
        Polygon.
    longest : Boolean :
        Return either longest boundary line or all the boundaries.

    Returns
    -------
    if longest=True:
        line : QgsLineString : Maximum length of the boundary.
    else:
        boundary : list of QgsLineString : Boundary of the polygon.
    """
    boundary = []
    poly = polygon.asPolygon()
    poly = [QgsPoint(p) for p in poly[0]]
    max_len = 0
    for i, pnt in enumerate(poly):
        if pnt != poly[-1] or i == 0:
            line = QgsLineString([pnt, poly[i + 1]])
            boundary.append(line)
            if longest and line.length() > max_len:
                max_len = line.length()
                long_line = line
    if longest:
        return line
    else:
        return boundary


def slice_layer(inlayer, node_max=10):
    """
    Slice the geom in input layer.
    First it tries to subdivide the geom.
    If the subdivided geometry collection is NULL it cuts the geom with a 5 by 5 grid
    Then it merges both of the sliced output.
    Finally, it performs a 0 length buffer to correct auto-intersection
    created by the subdivide function.

    Parameters
    ----------
    inlayer: QgisObject : QgsVectorLayer : Polygon or MultiPolygon
        Layer containing the features to slice.
    node_max: int:
        Number of nodes allowed for each geom output of subdivide function.
        Default 10, min 8. (Default value = None)

    Returns
    -------
    output_layer : Sliced layer
    """
    # Init output layers
    subdivid_layer = utils.create_layer(inlayer)
    pr = subdivid_layer.dataProvider()

    unsub_layer = utils.create_layer(inlayer, copy_field=True)
    pr2 = unsub_layer.dataProvider()

    # Subdivide features geom
    for feat in inlayer.getFeatures():
        geom = feat.geometry()
        success = False
        while success is False:
            subdivided = geom.subdivide(node_max).asGeometryCollection()
            if len(subdivided) != 0 or node_max > 255:
                success = True
            else:
                node_max += 5

        for g in subdivided:
            g = g.buffer(0, 5)  # Correct self intersection
            f = QgsFeature()
            f.setGeometry(g)
            pr.addFeature(f)
            subdivid_layer.updateExtents()

    return subdivid_layer
    #     subdivided = geom.subdivide(node_max).asGeometryCollection()

    #     # If successfull add them in output layer
    #     if len(subdivided) != 0:
    #         for g in subdivided:
    #             f = QgsFeature()
    #             f.setGeometry(g)
    #             pr.addFeature(f)
    #             subdivid_layer.updateExtents()
    #     # Else store them to further processing
    #     else:
    #         f = QgsFeature()
    #         f.setGeometry(geom)
    #         f.setAttributes(feat.attributes())
    #         pr2.addFeature(f)
    #         unsub_layer.updateExtents()

    # # Correct self-interseciton created by subdivide function
    # alg_name = "native:buffer"
    # params = {"INPUT": subdivid_layer,
    #           "DISTANCE": 0,
    #           "SEGMENTS": 5,
    #           "END_CAP_STYLE": 0,  # Round
    #           "JOIN_STYLE": 0,  # Round
    #           "DISSOLVE": False,
    #           "OUTPUT": "TEMPORARY_OUTPUT"}
    # output_layer = processing.run(alg_name, params)["OUTPUT"]

    # if unsub_layer.featureCount() != 0:
    #     # Create grid lines to cut the remaining input features
    #     alg_name = "native:creategrid"
    #     params = {"TYPE": 1,
    #               "EXTENT": unsub_layer,
    #               "HSPACING": 5,
    #               "VSPACING": 5,
    #               "CRS": inlayer,
    #               "OUTPUT": "TEMPORARY_OUTPUT"}
    #     grid_layer = processing.run(alg_name, params)["OUTPUT"]

    #     # Create spatial index
    #     layer_list = [unsub_layer, grid_layer]
    #     for layer in layer_list:
    #         alg_name = "native:createspatialindex"
    #         params = {"INPUT": layer}
    #         processing.run(alg_name, params)

    #     # Cut remaining input features
    #     alg_name = "native:splitwithlines"
    #     params = {"INPUT": unsub_layer,
    #               "LINES": grid_layer,
    #               "OUTPUT": "TEMPORARY_OUTPUT"}
    #     cut_layer = processing.run(alg_name, params)["OUTPUT"]

    #     # Merge both output
    #     alg_name = "native:mergevectorlayers"
    #     params = {"LAYERS": [output_layer, cut_layer],
    #               "CRS": inlayer,
    #               "OUTPUT": "TEMPORARY_OUTPUT"}
    #     output_layer = processing.run(alg_name, params)["OUTPUT"]

    return output_layer


def repair_topology(inlayer, slice_layer):
    """
    Heavy post processing for slicedLayer output to make sure topological
    relation are respected in input and subdivided layer.

    Circa 2 hours for 59 900 features - BDTopo circa 400 squared km.
    Cases :
        - In input: create subdivided features vertex missing in input
        - In subdivided features: create input vertex missing from subdivided
    Some topological error are still persistent (17 out of 59 900 features):
        - 10 are from error in input layer (overlapping feature from BDTopo)
        - 7 of them are an error in unshared boundary.
            Vertex are the sames both in input and subdivided
            Boundary is the same.
            I can't find the source of the error.

    Parameters
    ----------
    inlayer: QgisObject : QgsVectorLayer : Polygon or Multipolygon
        Input layer which as been subdivided.
    slice_layer:  QgisObject : QgsVectorLayer : Polygon or Multipolygon
        Sliced layer.

    Returns
    -------
    outputLayer :
        inLayer with good topological interactions with outSliceLayer.
    outSliceLayer :
        slicelayer with good topological interactions with outputLayer.
    """

    # Copy both input
    output_layer = utils.create_layer(inlayer, copy_feat=True, copy_field=True)
    out_slice_layer = utils.create_layer(slice_layer, copy_feat=True, copy_field=True)

    # Case 1 : Add vertex to input from subdivide
    for poly in output_layer.getFeatures():
        # Save the original geometry
        new_geom = poly.geometry()
        # Transform to geometry collection to handle multipolygon
        gc = new_geom.asGeometryCollection()[0]
        # Remove duplicate vertex (start node)
        pnt_L = list(set([p for p in gc.asPolygon()[0]]))

        # Get all the subdivided features inside this poly
        _, iL = get_clementini(out_slice_layer, poly.geometry())

        # Case 1 : Iteration other subdivided features inside an input poly
        for feature in iL:
            # Geometry collection and removing duplicated vertex
            gc = feature.geometry().asGeometryCollection()[0]
            pnt_L2 = list(set([p for p in gc.asPolygon()[0]]))

            # Case 1 : Iteration other point of a subdivided features
            for pnt in pnt_L2:
                dist, _, to, _ = poly.geometry().closestSegmentWithContext(pnt)
                # Check if pnt is on the edges of parent poly
                if dist == 0:
                    # If on edges check if already a point in parent poly
                    is_pnt = [p for p in pnt_L if p == pnt]
                    # If there is no vertex in the edges of parent poly add it
                    if len(is_pnt) == 0:
                        new_geom = vertex_add(new_geom, pnt.x(), pnt.y())
                        # Add it to vertex list to not be added a second time
                        # from another subdivided fatures
                        pnt_L.append(pnt)

            # Case 2 : We also check for connectivity on neighbouring subdivided features
            # Small buffer as connectivity is sometimes destoyed
            iC2, iL2 = get_clementini(out_slice_layer, feature.geometry().buffer(0.1, 5))
            # Removing feat from intersection list
            iL2 = [f for f in iL2 if f.id() != feature.id()]
            # Save original geometry
            new_sgeom = feature.geometry()

            # Iteration on neighbour
            for feat in iL2:
                # Geometry collection and removing duplicated vertex
                gc = feat.geometry().asGeometryCollection()[0]
                pnt_L3 = list(set([p for p in gc.asPolygon()[0]]))
                # Iteration other neibouring subdivided features point
                for p_pnt in pnt_L3:
                    # Check if pnt is on the edges of parent poly
                    dist, _, to, _ = feature.geometry().closestSegmentWithContext(p_pnt)
                    if dist == 0:
                        # If on edges check if already a point in parent poly
                        is_pnt = [p for p in pnt_L2 if p == p_pnt]
                        # If there is no vertex in the edges of parent poly add it
                        if len(is_pnt) == 0:
                            new_sgeom = vertex_add(new_sgeom, p_pnt.x(), p_pnt.y())
                            # Add it to vertex list to not be added a second time
                            # from another subdivided fatures
                            pnt_L2.append(p_pnt)

            # Case 2 : Add all the modifications on this feature at once
            with edit(out_slice_layer):
                out_slice_layer.changeGeometry(feature.id(), new_sgeom)

        # Case 1 : Add all the modifications on this feature at once
        with edit(output_layer):
            output_layer.changeGeometry(poly.id(), new_geom)

    return output_layer, out_slice_layer


def create_forest_connection(poly_layer, arc_layer, node_layer, forest_layer, forest_id, distance):
    """
    Create a node in junction between arc and forest.
    Store topology in this new node (forest id and vid)


    Parameters
    ----------
    poly_layer: QgsObject : QgsVectorLayer :
        Polygon layer.
    arc_layer: QgsObject : QgsVectorLayer :
        LineString layer.
    node_layer: QgsObject : QgsVectorLayer :
        Node layer.
    forest_layer: QgsObject : QgsVectorLayer :
        Polygon layer of forest.
    forest_id : str :
        Name of forest unique id.
    distance : int ;
        Distance of buffer to connect forest to hedge extremities.

    Returns
    -------
    forest_connection_point : QgsObject : QgsVectorLayer : Node layer
        Connection point of forest inside the hedge network.
    """
    # Create empty node layer
    forest_connection_point = utils.create_layer(node_layer)
    at.create_fields(forest_connection_point, [("vid", QVariant.Int),
                                              (forest_id, QVariant.String)])

    alg_name = "native:pointonsurface"
    params = {"INPUT": forest_layer,
              "ALL_PARTS": True,
              "OUTPUT": "TEMPORARY_OUTPUT"}
    forest_centroid = processing.run(alg_name, params)["OUTPUT"]

    node_map = []
    for poly in poly_layer.getFeatures():
        # Get hedge directly connected to a forest
        iC, iF = get_clementini(forest_layer, poly.geometry().buffer(distance, 5))  # dist, segment
        for f in iF:
            # Retrieve corresponding centroid
            exp = "{} = '{}'".format(forest_id, f[forest_id])
            req = QgsFeatureRequest().setFilterExpression(exp)
            centroid = next(forest_centroid.getFeatures(req))

            # Retrieve arc and determine which end is closer to centroid
            exp = "pid = %s" % poly["pid"]
            req = QgsFeatureRequest().setFilterExpression(exp)
            arc = next(arc_layer.getFeatures(req))

            d1 = QgsGeometry(
                QgsPoint(arc.geometry().asPolyline()[0])).distance(
                centroid.geometry())
            d2 = QgsGeometry(
                QgsPoint(arc.geometry().asPolyline()[-1])).distance(
                centroid.geometry())

            new_feat = QgsFeature()

            if d1 < d2:
                # Retrieve vid
                _, iF = get_clementini(node_layer, QgsGeometry(
                    QgsPoint(arc.geometry().asPolyline()[0])).buffer(0.1, 5))
                # Create new forest connection feature from this node
                node = iF[0]
                new_feat.setAttributes([node["vid"], centroid[forest_id]])
                new_feat.setGeometry(
                    QgsGeometry(QgsPoint(arc.geometry().asPolyline()[0])))
            else:
                # Retrieve vid
                _, iF = get_clementini(node_layer, QgsGeometry(
                    QgsPoint(arc.geometry().asPolyline()[-1])).buffer(0.1, 5))
                # Create new forest connection feature from this node
                node = iF[0]
                new_feat.setAttributes([node["vid"], centroid[forest_id]])
                new_feat.setGeometry(
                    QgsGeometry(QgsPoint(arc.geometry().asPolyline()[-1])))

            node_map.append(new_feat)

    forest_connection_point.dataProvider().addFeatures(node_map)

    return forest_connection_point


def match_parts(line_1, line_2):
    """
    Check which end of a linestring match with ends of another linestring.
    Return tuple of index (0,-1) where first element correspond to
    vertex index of first line and second vertex index of second line.

    Parameters
    ----------
    line_1: QgsLineString
    line_2: QgsLineString

    Returns
    -------
    (int, int) : either last vertex (-1) or first (0)
    """
    if line_1[0] == line_2[0]:
        return 0, 0
    elif line_1[0] == line_2[-1]:
        return 0, -1
    elif line_1[-1] == line_2[0]:
        return -1, 0
    elif line_1[-1] == line_2[-1]:
        return -1, -1


def fuse_parts(line_1, line_2):
    """
    Fuse two linestring if their extremities match.

    Parameters
    ----------
    line_1: QgsLineString
    line_2: QgsLineString

    Returns
    -------
    line_1 : QgsLineString :
        Hard copy (as line_1 take inheritance of line_1 input with line_2 appended).
    """
    # Match poly parts

    index = match_parts(line_1, line_2)

    # Fuse parts
    if index == (-1, 0):
        line_1.append(line_2)
    elif index == (-1, -1):
        line_1.append(line_2.reversed())
    elif index == (0, 0):
        line_1 = line_1.reversed()
        line_1.append(line_2)
        line_1 = line_1.reversed()
    elif index == (0, -1):
        line_1 = line_1.reversed()
        line_1.append(line_2.reversed())
        line_1 = line_1.reversed()

    return line_1


def get_line_between_pnts(geom, pnt_from, pnt_to, longest=True):
    """
    geom : QgsGeometry : (Multi)Polygon or (Multi)LineString
    pnt_from : QgsPointXY or QgsPoint
    pnt_to : QgsPointXY or QgsPoint
    longest : bool : default : True. Only usefull for polygons
    Get lines between 2 points. if points are not vertex it'll get the closest ones.
    Function can either return shortest line or longest one for polygons.
    """
    # Fetch vertex id
    _, vertex_id_from, _, _, _ = geom.closestVertex(QgsPointXY(pnt_from))
    _, vertex_id_to, _, _, _ = geom.closestVertex(QgsPointXY(pnt_to))

    # Compute length metrics
    from_dist_from_start = geom.distanceToVertex(vertex_id_from)
    to_dist_from_start = geom.distanceToVertex(vertex_id_to)
    delta_between_pnt = from_dist_from_start - to_dist_from_start

    if geom.wkbType() == QgsWkbTypes.LineString or geom.wkbType() == QgsWkbTypes.MultiLineString:

        if delta_between_pnt < 0:
            direction = 1  # Numerisation order
        else:
            direction = -1  # Anti numerisation order

        # Construct line between point
        start_vertex = geom.vertexAt(vertex_id_from)
        next_vertex_id = vertex_id_from + direction
        next_vertex = geom.vertexAt(next_vertex_id)
        line = QgsLineString([start_vertex, next_vertex])

        # Add vertex while id does not match id_to
        while next_vertex_id != vertex_id_to:
            next_vertex_id += direction

            next_vertex = geom.vertexAt(next_vertex_id)
            line.addVertex(next_vertex)

    elif geom.wkbType() == QgsWkbTypes.Polygon or geom.wkbType() == QgsWkbTypes.MultiPolygon:
        total_length = geom.length()
        if delta_between_pnt < 0:
            direction = 1  # Numerisation order
            delta_by_start = (total_length - to_dist_from_start) + from_dist_from_start
        else:
            direction = -1  # Anti numerisation order
            delta_by_start = (total_length - from_dist_from_start) + to_dist_from_start

        # Determine which way is the shortest and longest way
        if delta_by_start > abs(delta_between_pnt):
            if longest:
                # 1 is numerisation order and -1 anti numerisation order. None is a fill condition
                construction_way = -1 if direction == 1 else 1 if direction == -1 else None
            else:
                construction_way = 1 if direction == 1 else -1 if direction == -1 else None
        elif delta_by_start < abs(delta_between_pnt):
            if longest:
                construction_way = 1 if direction == 1 else -1 if direction == -1 else None
            else:
                construction_way = -1 if direction == 1 else 1 if direction == -1 else None
        else:
            raise ValueError("Both possible way for hedge fusion are equidistant")

        # Construct line between point
        start_vertex = geom.vertexAt(vertex_id_from)
        next_vertex_id = vertex_id_from + construction_way
        next_vertex = geom.vertexAt(next_vertex_id)
        line = QgsLineString([start_vertex, next_vertex])
        # Last vertex id to handle loop from origin
        _, last_vertex, _, _, _ = geom.closestVertex(
            QgsPointXY(geom.vertexAt(0)))

        # Prevent skipping vertex_id_to 
        if vertex_id_to == last_vertex and construction_way == -1:
            vertex_id_to = 0
        elif vertex_id_to == 0 and construction_way == 1:
            vertex_id_to = last_vertex

        # Add vertex while id does not match id_to
        while next_vertex_id != vertex_id_to:
            # Handling loop from origin. last_vertex equals to 0 vertex
            if construction_way == 1 and next_vertex_id == last_vertex:
                next_vertex_id = 0
            if construction_way == -1 and next_vertex_id == 0:
                next_vertex_id = last_vertex

            next_vertex_id += construction_way

            next_vertex = geom.vertexAt(next_vertex_id)
            line.addVertex(next_vertex)

    return line


def get_extremities_intersection(poly, arc, centroid_1_geom, centroid_2_geom):
    """
    Return first and last intersection points between the line formed with
    the 2 centroids of the shortest sides of a bounding box and his polygon and
    his arc.

    Parameters
    ----------
    poly : Qgsgeometry : Polygon
    arc : QgsGeometry : LineString
    centroid_1_geom : QgsGeometry : Point
    centroid_2_geom : QgsGeometry : Point

    Returns
    -------

    first_last_poly : list :
        First element is the first intersection point with the polygon.
        Second element is the second point.
    first_last_arc : list :
        First element is the first intersection point with the arc.
        Second element is the second point.
    """
    poly_boundary = QgsGeometry(poly.geometry().constGet().boundary())
    center_line_geom = QgsGeometry().fromPolyline(
        [QgsPoint(centroid_1_geom.asPoint()),
         QgsPoint(centroid_2_geom.asPoint())])
    
    # Test dist with centroid to select "first" and "last" intersection
    # instead of slicing --> Handle multiple intersection
    inter = poly_boundary.intersection(center_line_geom).asGeometryCollection()

    dist_from_c1 = [i.distance(centroid_1_geom) for i in inter]
    idx_c1 = dist_from_c1.index(min(dist_from_c1))
    dist_from_c2 = [i.distance(centroid_2_geom) for i in inter]
    idx_c2 = dist_from_c2.index(min(dist_from_c2))
    first_last_poly = [inter[idx_c1], inter[idx_c2]]

    # First and last intersection with arc
    inter = arc.geometry().intersection(center_line_geom).asGeometryCollection()
    if len(inter) <= 1:
        return "ERROR", "ERROR", "ERROR", "ERROR"
    first_last_arc = inter[::len(inter) - 1]

    if first_last_poly[0].distance(centroid_1_geom) \
            > first_last_poly[0].distance(centroid_2_geom):
        first_last_poly.reverse()

    if first_last_arc[0].distance(centroid_1_geom) \
            > first_last_arc[0].distance(centroid_2_geom):
        first_last_arc.reverse()

    return first_last_poly[0], first_last_poly[1], \
           first_last_arc[0], first_last_arc[1]


def match_extremities(node_layer, arc, arc_geom, ext_1, ext_2):
    """
    Match old arc vertex extremities with new arc vertex etremities and nodes.
    As such that start/end vertex are associated with new start/end.

    Parameters
    ----------
    node_layer : QgsVectorLayer : Point :
        Current node layer.
    arc : QgsGeometry : LineString
    arc_geom : QgsLineString : :
        Previously modified geometry of arc (with vertex added where arc
        intersects center line).
    ext_1 : QgsGeometry : Point :
        New extremity.
    ext_2 : QgsGeometry : Point :
        New extremity.

    Returns
    -------
    match : tuple :
        Tuple of list with 2 elements (for the two extremities) :
        (dist between new and old extremities,
        [node id, vertex id][old geometry, new geometry]).

    """
    exp = "vid in {}".format(
        tuple([arc["vid_1"], arc["vid_2"]]))
    req = QgsFeatureRequest().setFilterExpression(exp)
    nodes = list(node_layer.getFeatures(req))
    # Id of last vertex of arc
    end_pnt_xy = QgsPointXY(arc_geom.constGet().endPoint())
    _, last_vertex, _, _, _ = arc_geom.closestVertex(end_pnt_xy)

    # Id node matching first and last vertex
    arc_as_line = arc_geom.asPolyline()
    first_node = \
    [n.id() for n in nodes if n.geometry().asPoint() == arc_as_line[0]][0]
    last_node = \
    [n.id() for n in nodes if n.geometry().asPoint() == arc_as_line[-1]][0]

    # Transform geom to QgsPointXY
    ext_1 = ext_1.asPoint()
    ext_2 = ext_2.asPoint()

    # Create topology
    ids = [[[first_node, 0], "ext_1"],
           [[first_node, 0], "ext_2"],
           [[last_node, last_vertex], "ext_1"],
           [[last_node, last_vertex], "ext_2"]]  # [vid, vertex_id]

    combi = [[arc_as_line[0], ext_1], [arc_as_line[0], ext_2],
             [arc_as_line[-1], ext_1], [arc_as_line[-1], ext_2]]
    possible_lines = [QgsLineString(line) for line in combi]
    distance = [l.length() for l in possible_lines]

    zipped = zip(distance, ids, combi)
    sort = sorted(zipped)
    match = sort[0:2]

    return match


def evaluate_arc_parts(nodes, matches, arc_start, arc_end, thresh_dist_ext,
                       part_max_len):
    """
    Check if distance between new extremities and old extremities are within
    the specified threshold.

    Also check if length of new start and new end part are within the specified
    threshold.

    Parameters
    ----------
    nodes : list :
        QgsFeature of O nodes.
    matches :
        "Topology" tuple from match_extremities.
    arc_start : QgsLineString :
        New possible start part of arc (line between intersection with bounding
        box centerline with polygon and center line intersection with arc).
    arc_end : QgsLineString :
        New possible end part of arc (line between intersection with bounding
        box centerline with polygon and center line intersection with arc).
    thresh_dist_ext : int :
        Threshold distance between new extremities and old extremities.
    part_max_len :
        Maximum possible length for new parts.

    Returns
    -------
    output : dict : {start: bool_start, end: bool_end}
        True if new extremities are below the threshold.
    """
    output = {"start": None, "end": None}
    # Check if node 0
    vid_start = matches[0][1][0][0] if matches[0][1][1] == "ext_1" else \
    matches[1][1][0][0]  # Listception
    vid_end = matches[0][1][0][0] if matches[0][1][1] == "ext_2" else \
    matches[1][1][0][0]
    if vid_start in nodes:  # it's an O node
        dist_curr_new_ext = matches[0][0] if matches[0][1][1] == "ext_1" else \
        matches[1][0]
        new_ext_len = arc_start.length()

        # Check distance end length criteria
        output["start"] = 1 if dist_curr_new_ext < thresh_dist_ext \
                               and new_ext_len < part_max_len else 0
    if vid_end in nodes:
        dist_curr_new_ext = matches[0][0] if matches[0][1][1] == "ext_2" else \
        matches[1][0]
        new_ext_len = arc_end.length()
        # Check distance end length criteria
        output["end"] = 1 if dist_curr_new_ext < thresh_dist_ext \
                             and new_ext_len < part_max_len else 0

    return output


def build_new_arc(nodes, arc_geom, matches, ext_1_arc, ext_2_arc,
                  thresh_dist_ext, part_max_len):
    """
    Split arc in start, body and end parts.
    If new start and new end match distance condition and length condition
    then replace current start and end with new parts.

    Parameters
    ----------
    nodes : list :
        QgsFeature of O nodes.
    arc_geom : QgsLineString : :
        Previously modified geometry of arc (with vertex added when arc
        intersect center line).
    ext_1_arc : Qgsgeometry : Point :
        First intersection between center line from bounding box and arc.
    ext_2_arc : Qgsgeometry : Point :
        Last intersection between center line from bounding box and arc.
    thresh_dist_ext : int :
        Threshold distance between new extremities and old extremities.
    part_max_len :
        Maximum possible length for new parts.
    """
    # Get arc body
    arc_body = get_line_between_pnts(arc_geom,
                                     ext_1_arc.asPoint(),
                                     ext_2_arc.asPoint())
    # Get start and end vertex id
    start_vertex_id = matches[0][1][0][1] \
        if matches[0][1][1] == "ext_1" \
        else matches[1][1][0][1]
    end_vertex_id = matches[0][1][0][1] \
        if matches[0][1][1] == "ext_2" \
        else matches[1][1][0][1]
    # Get start and end vertex
    start_pnt = arc_geom.vertexAt(start_vertex_id)
    end_pnt = arc_geom.vertexAt(end_vertex_id)

    # Get start and end arc part
    arc_start = get_line_between_pnts(arc_geom, ext_1_arc.asPoint(), start_pnt)
    arc_end = get_line_between_pnts(arc_geom, ext_2_arc.asPoint(), end_pnt)

    # Build potential new start and end arc
    new_arc_start = QgsLineString([ext_1_arc.asPoint(),
                                   matches[0][2][1] if matches[0][1][
                                                           1] == "ext_1"
                                   else matches[1][2][1]])
    new_arc_end = QgsLineString([ext_2_arc.asPoint(),
                                 matches[1][2][1] if matches[0][1][1] == "ext_2"
                                 else matches[1][2][1]])

    # Evaluate if new parts will replace old parts
    match_dict = evaluate_arc_parts(nodes, matches, new_arc_start, new_arc_end,
                                    thresh_dist_ext, part_max_len)

    # Build new end and modify arc
    if match_dict["start"] == 1:
        arc_body = fuse_parts(arc_body, new_arc_start)
    else:
        arc_body = fuse_parts(arc_body, arc_start)

    if match_dict["end"] == 1:
        arc_body = fuse_parts(arc_body, new_arc_end)
    else:
        arc_body = fuse_parts(arc_body, arc_end)

    return arc_body, match_dict


def constrained_douglas_peucker(polygon_geom, linestring, epsilon):
    """
    Implementation of douglas-peucker algorithm where the result is forced to
    be inside a polygon.

    Parameters
    ----------
    polygon_geom : QgsGeometry : Polygon :
        Spatial constraint.
    linestring : QgsLineString :
        Linestring to simplify.
    epsilon : float :
        Maximum allowed shift for deleting a vertex.

    Returns
    -------
    result : ite[QgsPoint] :
        Retained vertex after simplification.
    """
    # get the start and end points
    start = linestring.startPoint()
    end = linestring.endPoint()
    line = QgsLineString([start, end])
    line_geom = QgsGeometry.fromPolyline(line)

    # get within condition
    # some T point ar enot considered within.
    # If i change snapping option in use_cutline within works but other topological problems happen
    # So i use a small buffer
    inside = line_geom.within(polygon_geom.buffer(0.1, 5))
    # get vertices
    vertices = linestring.points()
    vertices_geom = [QgsGeometry(v) for v in vertices]

    # find distance from other_points to line formed by start and end
    dist_vertex_to_line = [v.distance(line_geom) for v in vertices_geom]
    # get the index of the vertex with the largest distance
    max_value = max(dist_vertex_to_line)
    max_idx = dist_vertex_to_line.index(max_value)

    result = []
    # divide and conquer (recursive)
    if max_value > epsilon or not inside:
        linestring_left = QgsLineString(vertices[:max_idx + 1])
        result_left = constrained_douglas_peucker(polygon_geom, linestring_left,
                                                  epsilon)
        result += [v for v in result_left if v not in result]

        linestring_right = QgsLineString(vertices[max_idx:])
        result_right = constrained_douglas_peucker(polygon_geom,
                                                   linestring_right, epsilon)
        result += [v for v in result_right if v not in result]
    else:
        result += [start, end]

    return result


def multipolyline_to_line(geometry_collection):
    """
    Check if the lines in a multipolyline (in a geometry collection object)
    are continuous.

    Merge the lines that can be merged together and return a list of single
    lines.

    Parameters
    ----------
    geometry_collection : QgsGeometryCollection

    Returns
    -------
    lines : ite[QgsGeometry : LineString]
    """
    lines = []
    line = geometry_collection[0]
    geometry_collection.pop(0)
    
    while True:
        i_count = 0
        del_list = set()
        for line_2 in geometry_collection:
            if line.intersects(line_2):
                line = line.combine(line_2)
                i_count += 1 
                del_list.add(line_2)
        # Delete parts that had been added
        geometry_collection = list(set(geometry_collection) - del_list)
        # If no more intersecting parts
        if i_count == 0:
            # if len(line.asPolyline()) > 2:
            # Append result
            lines.append(line) 
            # If still parts in gc then init a 
            # new starting point and begin iteration again
            if bool(geometry_collection):
                line = geometry_collection[0]
                geometry_collection.pop(0)
            # If gc is empty then exit while loop and function
            else:
                break
    return lines


def create_new_part(intersection):
    """
    Take a QgsLineString representing an intersection between 2 polygons.

    Or an iterable of QgsLineString representing multiple intersection between 2 polygons.
    The new_part will be validated to be sure it is contained inside geom_1 and geom_2.
    If it does not it'll be modified to respect this constraint [Currently skip this case]
    Then create a new linestring with start and end point of the (each) intersection and return it.

    Parameters
    ----------
    intersection : QgsLineString or ite[QgsLineString]

    Returns
    -------
    new_part : QgsLineString or ite[QgsLineString]
    """
    if isinstance(intersection, list):
        new_part = [QgsLineString([l.startPoint(), l.endPoint()]) for l in
                    intersection]
        return new_part
    else:
        new_part = QgsLineString(
            [intersection.startPoint(), intersection.endPoint()])
        return new_part


def get_new_intersection(polygon_1, polygon_2):
    """
    Return the intersection startPoint and endPoint (only) as a new LineString layer.
    This allow the users to simplify complex intersection between 2 polygonal features.

    Parameters
    ----------
    polygon_1 : QgsGeometry : Polygon
    polygon_2 : QgsGeometry : Polygon

    Returns
    -------
    result : bool : False if error else true
    geometries : ite[QgsGeometry : LineString] : New intersection line
    """
    geometries = []

    intersection = polygon_1.intersection(polygon_2)
    # Transfrom intersection to LineString
    if intersection.wkbType() == 5:  # MultiLineString
        gc = intersection.asGeometryCollection()
        lines_geom = multipolyline_to_line(gc)  # List of QgsGeometry : LineString
        lines = [l.constGet() for l in lines_geom]  # List of LineString
    elif intersection.wkbType() == 2:  # LineString or LineGeometry
        lines = intersection.constGet()  # LineString
    else:  # Invalid intersection for intended use case
        return False, False
    
    # Create new boundary based on intersection between hedge and forest
    new_part = create_new_part(lines)  # LineString or List of LineString
    if isinstance(new_part, list):
        for line in new_part:
            geom = QgsGeometry(line)
            geometries.append(geom)
    else:
        geom = QgsGeometry(new_part)
        geometries.append(geom)

    return True, geometries


def saga_polygon_line_intersection(polygon_layer, line_layer):
    """
    Perform saga polygon/line intersection. This algorithm return all features
    of the input polygon layer.
    Post process in this function allow to retain only geometry that touch the
    line.

    Parameters
    ----------
    polygon_layer : QgsVectorLayer :
        Polygon.
    line_layer : QgsVectorLayer :
        LineString.

    Returns
    -------
    polygon_split : QgsVectorLayer :
        Polygon.
    """
    alg_name = "saga:polygonlineintersection"
    params = {"POLYGONS": polygon_layer,
              "LINES": line_layer,
              "INTERSECT": "TEMPORARY_OUTPUT"}
    polygon_path = processing.run(alg_name, params)["INTERSECT"]
    polygon_split = QgsVectorLayer(polygon_path, "intersect", "ogr")

    p_i_ids = []  # Polygon ids that are intersecting the lines
    p_del = []  # Polygon ids that does not intersect the lines
    p_max = []  # Main polygon (biggest area) that intersect (main part of original polygon)

    for line in line_layer.getFeatures():
        counts, p_i = get_clementini(polygon_split, line.geometry().buffer(0.001, 5))
        if counts > 0:
            # Store all the ids that intersect
            p_i_ids += [feat.id() for feat in p_i]
            # Delete main part of forest
            # For that we get the index of the max area polygon
            p_geom = [f.geometry() for f in p_i]
            p_area = [g.area() for g in p_geom]
            idx_max = p_area.index(max(p_area))
            p_max.append(p_i[idx_max].id())

    # Add features that does not intersect forest/hedge to deletion
    p_ids = [feat.id() for feat in polygon_split.getFeatures()]
    p_del += list(
        set(p_ids) - set(p_i_ids)) + p_max  # Add main polygon part to deletion

    # Deletion
    polygon_split.dataProvider().deleteFeatures(p_del)

    return polygon_split


# START OF FUNCTION FROM MEDIAN AXIS COMPUTATION
def get_min_width(geometry):
    """
    First estimate the mean width with the quadratic formula approximation.
    Then derivate a negative buffer value from mean width and iterate
    incrementing this value until geom is splitted.
    Value who splitted the geom is considered min width.

    Mean width of complex polygon is poorly estimated with this formula.

    NOTE : Min width from already dense vertex geometry (from raster
    classification) could slow down significantly process.

    Parameters
    ----------
    geometry : QgsGeometry :
        Polygon.

    Returns
    -------
    min_width : float
    """
    part = len(geometry.asGeometryCollection())
    perimeter = geometry.length()
    area = geometry.area()
    # Assuming an hedge is a long and thin polygon we can estimate
    # mean width with (2*A)/P. More reliably with a quadratic formula
    value = perimeter**2 - 16*area
    if value >= 0: 
        mean_width = (perimeter - math.sqrt(perimeter**2 - 16*area))/4
    else:
        mean_width = (2*area)/perimeter
        
    semi_min_width = (mean_width*0.5)/64

    ite = 0  # In case there is an invalid geometyr we need an escape
    max_ite = 100
    while part == 1:
        geometry = geometry.buffer(-semi_min_width, 5)
        part = len(geometry.asGeometryCollection())

        if geometry.isEmpty()  or ite == max_ite:
            break
        if part == 1:
            semi_min_width = semi_min_width * math.sqrt(2)

        ite += 1
    min_width = semi_min_width
    return min_width


def prepare_geometry(geometry, min_width):
    """
    Perform a simplification and a densification by distance of the geometry
    based on a min width value.
    simplify value = min width/5
    densification value = min width/2

    Parameters
    ----------
    geometry : QgsGeometry :
        Polygon.
    min_width : int :
        Minimal width of the geometry.
    """
    if min_width == -1:
        min_width = get_min_width(geometry)
        # To prevent adding too much vertex and increasing length of alg
        min_width = min_width if min_width >= 2 else 2
        # Can greatly increse processing length if no floor limit.
        # Further testing to adjust threshold. If too many subgrpah reduce it

    geometry = geometry.simplify(min_width/5)
    geometry = geometry.densifyByDistance(min_width/2)

    return geometry


def compute_skeleton(polygon_geometry):
    """
    Compute the skeleton from a constrained Voronoi diagram.

    Parameters
    ----------
    polygon_geometry : QgsGeometry :
        Polygon.

    Returns
    -------
    skeleton_list : ite[QgsFeature]
        Pseudo graph.
    """
    voronoi_geom = polygon_geometry.voronoiDiagram(edgesOnly=True)

    skeleton_list = []
    errors = []

    # Construct geometry engine
    engine = QgsGeometry.createGeometryEngine(polygon_geometry.constGet())
    engine.prepareGeometry()

    # Geometry of type LineString and 0ish (1e-09) length are created
    # it creates topology issue into the graph.
    # So i put them into error layer to handle them in repair_errors
    for v in voronoi_geom.asGeometryCollection():
        if engine.contains(v.constGet()):
            if v.length() > 1e-06:
                skeleton_list.append(v)
            else:
                errors.append(v)
        # elif engine.intersects(v.constGet()):
        #     inter = engine.intersection(v.constGet())
        #     if inter.geometryType() == "LineString":
        #         if not isinstance(inter, QgsGeometry):
        #             inter = QgsGeometry(inter)
        #         if inter.length() < 1e-03:
        #             errors.append(inter)
        #         else:
        #             skeleton_list.append(inter)
    if errors:
        skeleton_list = repair_errors(errors, skeleton_list)

    return skeleton_list


def repair_errors(errors, skeleton_list):
    """
    Find the lines of skeleton_list directly connected to line in errors.
    Replace the intersecting extremities of the skeleton lines wit error lines
    by centroid of error lines. This way topology are preserved

    Parameters
    ----------
    errors : ite[QgsGeometry ; LineString] :
        0ish lines.
    skeleton_list : ite[QgsGeometry : LineString] :
        Real skeleton lines.

    Returns
    -------
    skeleton_list : ite[QgsGeometry : LineString] :
        Updated skeleton lines.
    """
    for error in errors:
        engine = QgsGeometry.createGeometryEngine(error.constGet())
        engine.prepareGeometry()
        centroid = error.centroid()
        new_pnt = centroid.constGet()
        for i, line in enumerate(skeleton_list):
            if engine.intersects(line.constGet()):
                start = QgsPoint(line.asPolyline()[0])
                end = QgsPoint(line.asPolyline()[1])
                if engine.intersects(start):
                    line = QgsGeometry().fromPolyline([new_pnt, end])
                else:
                    line = QgsGeometry().fromPolyline([start, new_pnt])
                skeleton_list[i] = line

    return skeleton_list
# END OF FUNCTION FROM MEDIAN AXIS COMPUTATION