"""
#TODO add docstring.
"""
from copy import deepcopy

import processing
from qgis.core import (QgsVectorLayer,
                       QgsFields,
                       QgsField,
                       QgsVectorLayerJoinInfo)


def create_fields(layer, fields_list):
    """
    Create field after checking if it already exists.
    If this is the case fields value will be overwritten.

    Parameters
    ----------
    layer : QgisObject : QgsVectorLayer :
        Any geometry.
    fields_list : list :
        List of fields with [("NAME", QVariant, ...)].

    Returns
    -------
    Updated layer.

    """
    fields = [QgsField(i[0], i[1]) for i in fields_list if
              i[0] not in [field.name() for field in layer.fields()]]
    if len(fields) != 0:
        layer.dataProvider().addAttributes(fields)
        layer.updateFields()


def delete_fields(layer, fields_list):
    """
    Delete fields in a layer.

    It allows to prevent error when QgsProcessing tools create them.

    Parameters
    ----------
    layer : QgisObject : QgsVectorLayer :
        Any geometry
    fields_list : list :
        List of fields with ["NAME"].

    Returns
    -------
    Updated layer.

    """
    fields = [layer.fields().indexFromName(i) for i in fields_list if
              i in [field.name() for field in layer.fields()]]
    if len(fields) != 0:
        layer.dataProvider().deleteAttributes(fields)
        layer.updateFields()


def add_fields(feature, fields_name, fields_type):
    """
    Add a field map to a feature.

    Parameters
    ----------
    feature : QgsFeature
        Input feature.
    fields_name : ite[str]
    fields_type : ite[QVariant]

    Returns
    -------
    feature :
        Modified feature.
    """
    fields = QgsFields()
    for name, variant in zip(fields_name, fields_type):
        fields.append(QgsField(name, variant))
    feature.setFields(fields)

    return feature


def set_fields_value(feature, fields_name, values):
    """
    Set a given value to a field in a feature.

    Parameters
    ----------
    feature : QgsFeature
    fields_name : ite[str]
    values : ite[any] :
        Value must be of the correct type.

    Returns
    -------
    feature :
        Modified feature.
    """
    for name, value in zip(fields_name, values):
        feature[name] = value

    return feature


def add_field_at_given_idx(layer, field_name, field_type, field_idx, values):
    """
    Add a field to a given index with the given value.

    It is suggested to do a copy of the layer first to avoid data deletion
    in case of errors. All the fields at the right of the given index will
    be moved.

    Parameters
    ----------
    layer : QgsVectorLayer:
        Any Geometry
    field_name : str :
        name of the new field.
    field_type : QVariant
    field_idx :
        Position of the new field.
    values : ite[Matching QVariant] :
        Values in order of features to be added.

    Returns
    -------
    layer : QgsVectorLayer: Any Geometry:
        Shallow copy of input layer.
    """

    # Create new field map. It is assumed the layer have a fid field.
    fields = layer.fields()
    idxs = [fields.indexOf(name) for name in fields.names() if name != "fid"]

    new_field = QgsField(field_name, field_type)
    fields_list = fields.toList()
    fields_list.insert(field_idx, new_field)    
    new_fields = QgsFields()
    for field in fields_list:
        new_fields.append(field)

    # Create new attributes map
    feats = [f for f in layer.getFeatures()]
    max_attr = len(fields.names())
    attr_map = {f.id(): {i: attribute for (i, attribute)
                         in enumerate(f.attributes())} for f in feats}
    attr_map_copy = deepcopy(attr_map)
    for i, (k, v) in enumerate(attr_map_copy.items()):
        idx = field_idx
        # While loop goal is to move all the fields to the right from given idx.
        while idx < max_attr:
            v[idx+1] = attr_map[k][idx]
            idx += 1
        # Once all the fields are moved, "insert" new field to given idx.
        v[field_idx] = values[i]

    # Delete current fields, add new ones and update values
    layer.dataProvider().deleteAttributes(idxs)
    layer.dataProvider().addAttributes(new_fields)
    layer.updateFields()
    layer.dataProvider().changeAttributeValues(attr_map_copy)

    return layer


def table_join(target_layer, pk_field, join_layer, fk_field, join_field=None,
               join_prefix=""):
    """
    Performs table join from one table to another table.

    Multiple layers can be used at once if list are used in arguments.

    Parameters
    ----------
    target_layer: (QgisObject) QgsVectorLayer :
        Any Geometry.
    pk_field: (String) :
        Name of the primary key field in the source layer.
    join_layer: (QgisObject) : QgsVectorLayer :
        Any Geometry.
    fk_field: (String or list) :
        Name of the foreign key field in the target layer. If multiple target
        layer the fk_field must be a list of the same length unless the same
        field is used for all layers.
    join_field: (String or list) :
        Name of fields to join. Should be a list of list for multiple layers of
        same len as join layer number.
    join_prefix: ite[str] or str if one layer : default : "".
        en of iterable should be equal to the number of layer.
    """
    # Check arguments format
    if isinstance(join_layer, list):
        pass
    else:
        join_layer = [join_layer]

    if isinstance(fk_field, list):
        pass
    else:
        fk_field = [fk_field]

    if isinstance(join_field, list):
        pass
    else:
        join_field = [[join_field]]

    if isinstance(join_prefix, list):
        pass
    else:
        join_prefix = [join_prefix] * len(join_layer)

    # if all elements in fk_field are equal
    if all(element == fk_field[0] for element in fk_field)\
            and len(fk_field) < len(join_layer):

        while len(fk_field) < len(join_layer):
            fk_field.append(fk_field[0])

    # if all elements in target_field are equal
    if all(element == join_field[0] for element in join_field) \
            and len(join_field) < len(join_layer):

        while len(join_field) < len(join_layer):
            join_field.append(join_field[0])

    # Make table join
    info = QgsVectorLayerJoinInfo()
    for layer, fk, prefix, field in \
            zip(join_layer, fk_field, join_prefix, join_field):
        info.setJoinLayerId(layer.id())
        info.setJoinFieldName(fk)
        info.setTargetFieldName(pk_field)
        info.setUsingMemoryCache(True)
        info.setPrefix(prefix)
        info.setJoinFieldNamesSubset(field)
        info.setJoinLayer(layer)
        target_layer.addJoin(info)


def refactor_fields(layer, change_map, output_type="TEMP"):
    """
    Refactor fields of a QgsVectorLayer given a change_map.

    Parameters
    ----------

    layer : QgsVectorLayer : Any geometry
    change_map : list :
        List of dict of form :
        [{{"expression": current name,
           "name": new or current name,
           "type": new or current type,
           "length": new or current length,
           "precision": new or current precision}]
        It is important to use current value if you do not want it to be changed
    output_type : string :
        Either a path to write the layer or TEMP.
        If default value result will be stored in cache.
        (Default value = "TEMP")

    Returns
    -------
    output : QgsVectorLayer :
        Any geometry.
    """
    fields = layer.fields().toList()
    fields_map = [{"expression": field.name(),
                   "name": field.name(),
                   "type": field.type(),
                   "length": field.length(),
                   "precision": field.precision()} for field in fields]

    field_name_2b_changed = [item["expression"] for item in change_map]
    idx_2b_changed = [fields_map.index(item) for item in fields_map
                      if item["expression"] in field_name_2b_changed]

    # Update field map
    for i, idx in enumerate(idx_2b_changed):
        fields_map.pop(idx)
        fields_map.insert(idx, change_map[i])

    alg_name = "native:refactorfields"
    params = {
        "INPUT": layer,
        "FIELDS_MAPPING": fields_map,
        "OUTPUT": "TEMPORARY_OUTPUT" if output_type is "TEMP" else output_type
    }
    output = processing.run(alg_name, params)["OUTPUT"]
    if output_type is not "TEMP":
        output = QgsVectorLayer(output, "refactorised_layer", "ogr")

    return output
