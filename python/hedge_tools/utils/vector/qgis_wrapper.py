"""
Wrappers functions that use QGIS algorithms.
"""
import processing


def buffer(layer, distance, dissolve=False, 
           end_style=0, join_style=0, miter_limit=2, segments=100):
    """
    Perform a buffer with the given distance on a layer.

    Parameters
    ----------
    layer : QgsVectorLayer :
        Polygon.
    distance : int  
    dissolve : bool :
        (Default value = False)
    segments : int
        
    Returns
    -------
    layer : QgsVectorLayer :
        Polygon.
    """    
    alg_name = "native:buffer"
    params = {"INPUT": layer,
              "DISTANCE": distance,
              "SEGMENTS": segments,
              "END_CAP_STYLE": end_style,  # Round
              "JOIN_STYLE": join_style,  # Round
              "MITER_LIMIT": miter_limit,
              "DISSOLVE": dissolve,
              "OUTPUT": "TEMPORARY_OUTPUT"}
    output = processing.run(alg_name, params)["OUTPUT"]

    return output


def closing(self, layer, distance, dissolve=False):
    """
    Perform a closing operation.

    From a layer and a distance value.

    Parameters
    ----------
    layer : QgsVectorLayer :
        Polygon.
    distance : int  
    dissolve : bool :
        (Default value = False)


    Returns
    -------
    layer : QgsVectorLayer :
        Polygon : Closed
    """ 
    # Erosion
    layer = buffer(layer, distance, dissolve)
    
    # Dilation
    output = buffer(layer, -distance, dissolve)

    return output


def opening(layer, distance, dissolve=False):
    """
    Perform an opening.

    From a layer and a distance value.

    Parameters
    ----------
    layer : QgsVectorLayer :
        Polygon.
    distance : int  
    dissolve : bool :
        (Default value = False)

    Returns
    -------
    layer : QgsVectorLayer :
        Polygon : Opened
    """ 
    # Erosion
    layer = buffer(layer, -distance, dissolve)
    
    # Dilation
    output = buffer(layer, distance, dissolve)

    return output


def dissolve(layer, dissolve_field=None, separate_disjoint=False):
    """
    Dissolve a layer.
    
    Parameters
    ----------
    layer : QgsVectorLayer :
        Any geometry.
    dissolve_field : str :
        (Default value = None)
    separate_disjoint : bool
        False

    Returns
    -------
    output : QgsVectorLayer

    """
    dissolve_field = [dissolve_field]
    alg_name = "native:dissolve"
    params = {"INPUT": layer,
              "FIELD": dissolve_field,
              "SEPARATE_DISJOINT": separate_disjoint,
              "OUTPUT": "TEMPORARY_OUTPUT"}
    output = processing.run(alg_name, params)["OUTPUT"]

    return output


def multipart_to_singleparts(layer):
    """
    Convert multiparts layer to single parts layer.

    Parameters
    ----------
    layer : QgsVectorLayer

    Returns
    -------
    output : QgsVectorlayer
    """
    # Multiple features to single features
    alg_name = "native:multiparttosingleparts"
    params = {"INPUT": layer,
              "OUTPUT": "TEMPORARY_OUTPUT"}
    output = processing.run(alg_name, params)["OUTPUT"]
    
    return output


def snap(layer_1, layer_2, tolerance, behavior=0):
    """
    Perform a snap between two layers.

    Parameters
    ----------
    layer_1 : QgsVectorLayer :
        Any geometry.
    layer_2 : QgsVectorLayer :
        Any geometry.
    tolerance : float :
        Tolerance for snapping.
    behavior : int :
        See native:snapgeometries description
        Can be accessed with processing.algorithmHelp("native:snapgeometries")
        in qgis python console. (Default value = 0)
    """
    alg_name = "native:snapgeometries"
    params = {"INPUT": layer_1,
              "REFERENCE_LAYER": layer_2,
              "TOLERANCE": tolerance,
              "BEHAVIOR": behavior,
              "OUTPUT": "TEMPORARY_OUTPUT"}
    snapped = processing.run(alg_name, params)["OUTPUT"]

    return snapped


def extract_by_location(input_layer, intersect_layer, predicate):
    """
    From an input_layer extract the features
    that respect the predicate form the intersect layer.

    Parameters
    ----------
    input_layer : QgsVectorLayer :
        Any geometry.
    intersect_layer : QgsVectorLayer :
        Any geometry.
    predicate : int :
        See native:extractbylocation description.
        Can be accessed with processing.algorithmHelp("native:extractbylocation")
        in qgis python console.

    Returns
    -------
    output : QgsVectorLayer :
        Any geometry.
    """
    # Create spatial index
    layer_list = [input_layer, intersect_layer]
    for layer in layer_list:
        alg_name = "native:createspatialindex"
        params = {"INPUT": layer}
        processing.run(alg_name, params)

    alg_name = "native:extractbylocation"
    params = {"INPUT": input_layer,
              "PREDICATE": predicate,
              "INTERSECT": intersect_layer,
              "OUTPUT": "TEMPORARY_OUTPUT"}
    output = processing.run(alg_name, params)["OUTPUT"]

    return output


def clip(input_layer, overlay):
    """
    Clip an input layer with an overlay layer.

    Parameters
    ----------
    input_layer : QgsVectorLayer :
        Any geometry.
    overlay : QgsVectorLayer :
        Any geometry.

    Returns
    -------
    output : QgsVectorLayer :
        Any geometry.
    """
    # Spatial index
    layer_list = [input_layer, overlay]
    for layer in layer_list:
        alg_name = "native:createspatialindex"
        params = {"INPUT": layer}
        processing.run(alg_name, params)

    # Difference
    alg_name = "native:clip"
    params = {"INPUT": input_layer,
              "OVERLAY": overlay,
              "OUTPUT": "TEMPORARY_OUTPUT"}
    output = processing.run(alg_name, params)["OUTPUT"]

    return output


def difference(input_layer, overlay):
    """
    Perform the difference between an input layer and an overlay layer.

    Parameters
    ----------
    input_layer : QgsVectorLayer :
        Any geometry.
    overlay : QgsVectorLayer :
        Any geometry.

    Returns
    -------
    output : QgsVectorLayer :
        Any geometry.
    """
    # Spatial index
    layer_list = [input_layer, overlay]
    for layer in layer_list:
        alg_name = "native:createspatialindex"
        params = {"INPUT": layer}
        processing.run(alg_name, params)

    # Difference
    alg_name = "native:difference"
    params = {"INPUT": input_layer,
              "OVERLAY": overlay,
              "OUTPUT": "TEMPORARY_OUTPUT"}
    output = processing.run(alg_name, params)["OUTPUT"]

    return output

def extract_by_expression(layer, expression):
    """
    Extract features from a layer by an expression.

    Expression should be in qgis syntax.

    Parameters
    ----------
    layer : QgsVectorLayer :
        Any geometry.
    expression : str

    Returns
    -------
    output : QgsVectorLayer :
        Any geometry.
    """
    alg_name = "native:extractbyexpression"
    params = {"INPUT": layer,
              "EXPRESSION": expression,
              "OUTPUT": "TEMPORARY_OUTPUT"}
    output = processing.run(alg_name, params)["OUTPUT"]

    return output


def merge_layers(layers):
    """
    Merge vector layers together.

    Input layers should be of same geometry.

    Parameters
    ----------
    layers : ite[QgsVectorLayer : Any geometry]

    Returns
    -------
    output : QgsVectorLayer
    """

    alg_name = "native:mergevectorlayers"
    params = {"LAYERS": layers,
              "OUTPUT": "TEMPORARY_OUTPUT"}
    output = processing.run(alg_name, params)["OUTPUT"]

    return output


def field_calculator(layer, field_name, field_type, formula,
                     field_length=0, field_precision=0):
    """
    Create a field in a QgsVectorLayer

    See qgis:fieldcalculator description for further information.
    Can be accessed with processing.algorithmHelp("qgis:fieldcalculator")
    in qgis python console.

    Parameters
    ----------
    layer : QgsVectorLayer :
        Any geometry.
    field_name : str:
    field_type : int:
    formula : str:
    field_length : int:
        (Default value = 0)
    field_precision : int:
        (Default value = 0)


    """
    alg_name = "qgis:fieldcalculator"
    params = {"INPUT": layer,
              "FIELD_NAME": field_name,
              "FIELD_TYPE": field_type,
              "FIELD_LENGTH": field_length,
              "FIELD_PRECISION": field_precision,
              "FORMULA": formula,
              "OUTPUT": "TEMPORARY_OUTPUT"}
    output = processing.run(alg_name, params)["OUTPUT"]

    return output


def remove_holes(layer, min_area=0):
    """
    Remove holes from a layer given an area threshold.

    Parameters
    ----------
    layer : QgsVectorLayer :
        Any geometry.
    min_area : float :
        Area below wich hole will be filled.
        With 0 remove all holes of any size. (Default value = 0)

    Returns
    -------
    output : QgsVectorLayer :
        Any geometry.
    """
    alg_name = "native:deleteholes"
    params = {"INPUT": layer,
              "MIN_AREA": min_area,
              "OUTPUT": "TEMPORARY_OUTPUT"}
    
    output = processing.run(alg_name, params)["OUTPUT"]

    return output


def create_ombb(layer):
    """
    Create the oriented minimum bounding box for features in a layer.

    Parameters
    ----------
    layer : QgsVectorLayer :
        Any geometry.

    Returns
    -------
    output : QgsVectorLayer :
        Polygon.
    """
    alg_name = "native:orientedminimumboundingbox"
    params = {"INPUT": layer,
              "OUTPUT": "TEMPORARY_OUTPUT"}
    output = processing.run(alg_name, params)["OUTPUT"]
    
    return output


def create_convex_hull(layer):
    """
    Create the convex hull for features in a layer.

    Parameters
    ----------
    layer : QgsVectorLayer :
        Any geometry.

    Returns
    -------
    output : QgsVectorLayer :
        Polygon.
    """
    alg_name = "native:convexhull"
    params = {"INPUT": layer,
              "OUTPUT": "TEMPORARY_OUTPUT"}
    output = processing.run(alg_name, params)["OUTPUT"]

    # In some case area field is not populated, do it manually.
    feature = output.getFeature(1)
    if feature["area"] is None:
        idx_area = output.fields().indexFromName("area")
        attr_map = {f.id(): {idx_area: f.geometry().area()} for f in
                    output.getFeatures()}
        output.dataProvider().changeAttributeValues(attr_map)

    return output


def fix_geometries(layer):
    """
    Fix geometries to respect GEOS.

    Parameters
    ----------
    layer : QgsVectorLayer :
        Any geometries.

    Returns
    -------
    output : QgsVectorLayer :
        Any geometries.
    """
    alg_name = "native:fixgeometries"
    params = {"INPUT": layer,
              "OUTPUT": "TEMPORARY_OUTPUT"}
    output = processing.run(alg_name, params)["OUTPUT"]

    return output

def delete_column(layer, fields_list):
    """
    Delete specified columns in a layer.

    Parameters
    ----------
    layer : QgsVectorLayer :
        Any geometry.
    fields_list : ite[str] :
        Name of the fields.

    Returns
    -------
    output : QgsVectorLayer :
        Any geometry.
    """
    alg_name = "native:deletecolumn"
    params = {"INPUT": layer,
              "COLUMN": fields_list,
              "OUTPUT": "TEMPORARY_OUTPUT"}
    output = processing.run(alg_name, params)["OUTPUT"]

    return output

def join_attributes_by_location(layer, join_layer, 
                                    predicate=[0], join_fields=[], 
                                    method=0, discard_nonmatching=False,
                                    prefix=""):
    """
    Perform a spatial join between layer and join_layer

    Parameters
    ----------
    layer : QgsVectorLayer : Any
    join_layer : QgsVectorLayer : Any
        Layer that'll be join
    predicate : ite[int] : 0
        Spatial predicate (clementini) 
        - 0: intersect
		- 1: contain
		- 2: equal
		- 3: touch
		- 4: overlap
		- 5: within
		- 6: cross
    join_fields : ite[str]
        Empty list for all fields
    method : int : 0
        - 0 : 1 to 1
        - 1 : 1 to many
    discard_nonmatching : bool
    prefix : str : ''

    Return
    ------
    output : QgsVectorLayer : Any
    """
    
    alg_name = "native:joinattributesbylocation"
    params = {"INPUT": layer,
              "PREDICATE": predicate,
              "JOIN": join_layer,
              "JOIN_FIELDS": join_fields,
              "METHOD": method,
              "DISCARD_NONMATCHING": discard_nonmatching,
              "PREFIX": prefix,
              "OUTPUT": "TEMPORARY_OUTPUT"}
    output = processing.run(alg_name, params)["OUTPUT"]

    return output

def spatial_index(input_layer):
    alg_name = "native:createspatialindex"
    params = {"INPUT": input_layer}
    processing.run(alg_name, params)
    return input_layer