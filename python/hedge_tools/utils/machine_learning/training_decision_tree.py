"""
Script to train a decision tree to classify hedges according to the AFAC typology.
"""
import os
import pickle
import re

import geopandas as gpd
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn import tree
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.tree import export_text
from typing import List, Any


def plot_confusion_matrix(confusion_mat: confusion_matrix,
                          target_names: List[str],
                          title: str = 'Confusion matrix',
                          cmap: str = None, normalize: bool = True) -> None:
    """
    Make a plot of a sklearn confusion matrix.

    Parameters
    ----------
    confusion_mat : confusion_matrix :
        Confusion matrix from sklearn.metrics.confusion_matrix.
    target_names : List[str] :
        Classes' names. For example: ['high', 'medium', 'low'].
    title :
        The text to display at the top of the matrix
        (Default value = 'Confusion matrix').
    cmap :
        The gradient of the values displayed from matplotlib.pyplot.cm
        see http://matplotlib.org/examples/color/colormaps_reference.html
        (Default value = None).
    normalize :
        If False, plot the raw numbers. If True, plot the proportions.

    Returns
    -------
    None.
    """
    accuracy = np.trace(confusion_mat) / np.sum(confusion_mat).astype('float')
    misclassified = 1 - accuracy

    if cmap is None:
        cmap = plt.get_cmap('Blues')
    plt.figure(figsize=(8, 6))
    plt.imshow(confusion_mat, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    if target_names is not None:
        # target_names = sorted(target_names.items(), key=lambda kv: kv[1])
        # target_names = [i[0] for i in target_names]
        tick_marks = np.arange(len(target_names))
        plt.xticks(tick_marks, target_names, rotation=45)
        plt.yticks(tick_marks, target_names)
    # if normalize:
    #     cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    # thresh = cm.max() / 1.5 if normalize else cm.max() / 2
    # for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
    #     if normalize:
    #         plt.text(j, i, "{:0.4f}".format(cm[i, j]),
    #                  horizontalalignment="center",
    #                  color="white" if cm[i, j] > thresh else "black")
    #     else:
    #         plt.text(j, i, "{:,}".format(cm[i, j]),
    #                  horizontalalignment="center",
    #                  color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label\naccuracy={:0.4f}; misclass={:0.4f}'.format(accuracy, misclassified))
    plt.show()


def save_classifier(model: Any, model_path: str = None) -> None:
    """
    Save a sklearn model in a file.

    Parameters
    ----------
    model :
        A sklearn model instance (RF classifier, SVM, Decision Tree, etc)
        (Default value = None).
    model_path :
        Path of the model file (Default value = None).

    Returns
    -------
    None.

    """
    pickle.dump(model, open(model_path, "wb"))


def load_classifier(path_model: str) -> Any:
    """
    Load a model from its file.

    Parameters
    ----------
    path_model :
        Path of the model's file.

    Returns
    -------
    A sklearn model instance, for example RF classifier, SVM, Decision Tree.

    """
    return pickle.load(open(path_model, 'rb'))


def transform_export_text_to_python_script(exported_text: str,
                                           features_list: list[str],
                                           function_name: str,
                                           script_path: str) -> None:
    """
    Transforms the string obtained by the export_text function to a python
    script.
    
    The input string represents a decision tree trained with sklearn.
    The output script contains this decision tree as a set of ifs and else
    inside a function.

    Parameters
    ----------
    exported_text :
        Text obtained by the export_text function from sklearn.
    features_list :
        List of features' names used as entries of the decision tree.
    function_name :
        Name of the python function name in the output script.
    script_path :
        path of the script file.

    Returns
    -------
    None.

    """
    function_txt = f"def {function_name}({', '.join(features_list)}):\n"
    # rules = export_text.replace("|--- class:", "return")
    rules = re.sub(r"(\|--- class:) (\w+)\n", r"return '\2'\n", exported_text)
    rules = rules.replace("|   ", "    ")
    rules = rules.replace("|---", "if")
    rules = rules.replace("> ", ">")
    rules = re.sub(r"(\d+\.\d+)", r"\1:", rules)

    function_txt += "    " + rules.replace("\n", "\n    ")
    with open(script_path, "w") as file:
        file.write(function_txt)


def tree_classification(
        max_z: float, min_z: float, avg_z: float, max_nnd: float,
        min_nnd: float, avg_nnd: float) -> str:
    """
    Make a hedge classification with a decision tree.
    This function is the output of the transform_export_text_to_python_script
    with the trained decision tree.

    Parameters
    ----------
    max_z :
        Hedge's maximum height.
    avg_z :
        Hedge's average height.
    min_nnd :
        Hedge's minimum nearest neighbor distance.
    min_z :
        Hedge's minimum height.
    max_nnd :
        Hedge's maximum nearest neighbor distance.
    avg_nnd :
        Hedge's average nearest neighbor distance.

    Returns
    -------
    Hedge's type from AFAC typology (taillis, futaie, taillis sous futaie, etc.)

    """
    if max_z <= 9.52:
        return 'taillis'
    if max_z > 9.52:
        if avg_z <= 7.66:
            if max_nnd <= 4.63:
                if max_z <= 13.64:
                    if max_z <= 12.05:
                        return 'taillis_futaie'
                    if max_z > 12.05:
                        if max_nnd <= 3.15:
                            return 'futaie'
                        if max_nnd > 3.15:
                            return 'taillis'
                if max_z > 13.64:
                    return 'taillis_futaie'
            if max_nnd > 4.63:
                if max_z <= 21.76:
                    return 'taillis'
                if max_z > 21.76:
                    if min_z <= -0.61:
                        return 'taillis_futaie'
                    if min_z > -0.61:
                        if avg_nnd <= 0.90:
                            return 'taillis'
                        if avg_nnd > 0.90:
                            return 'futaie'
        if avg_z > 7.66:
            if max_nnd <= 3.48:
                return 'taillis'
            if max_nnd > 3.48:
                return 'futaie'


if __name__ == "__main__":
    training_folder = r"/home/dynafor/Documents/Data/Entrainement/"
    summary_folder = os.path.join(training_folder, "5_Summary")
    metrics_global_path = os.path.join(summary_folder, "Metrics_all_hedges.csv")
    labels_path = os.path.join(summary_folder, "labels_all_hedges.csv")

    # Make labels csv
    raw_folder = os.path.join(training_folder, "1_Brut")
    hedges_path = os.path.join(raw_folder, "Haies_labelisees.gpkg")
    hedges = gpd.read_file(hedges_path)

    hedges_labels = hedges[["data-typologie-type_general_haie", "id_tile"]]
    hedges_labels.columns = ["typologie", "hedge"]
    hedges_labels.to_csv(labels_path, index_label=False, index=False)

    # Training
    features = np.array(pd.read_csv(metrics_global_path, index_col=0))
    labels = np.array(pd.read_csv(labels_path, index_col=1))
    X_train, X_test, y_train, y_test = train_test_split(
        features, labels, test_size=0.33, random_state=42, stratify=labels)
    clf = tree.DecisionTreeClassifier()
    clf = clf.fit(X_train, y_train)

    # Confusion matrix
    labels_list = ['futaie', 'taillis_futaie', 'taillis']
    predictions = clf.predict(X_test)
    cm = confusion_matrix(y_test, predictions, labels=labels_list)
    plot_confusion_matrix(cm, labels_list)

    # Save classifier
    m_path = os.path.join(summary_folder, "DT_test_1")
    save_classifier(clf, m_path)

    # Export decision tree as python function
    clf_load = load_classifier(m_path)
    features_names_list = list(pd.read_csv(metrics_global_path, index_col=0).columns)
    tree_rules = export_text(clf_load, feature_names=features_names_list)
    script_py_path = os.path.join(summary_folder, "tree_rules.py")
    transform_export_text_to_python_script(tree_rules, features_names_list,
                                           "tree_classification",
                                           script_py_path)
