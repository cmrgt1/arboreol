"""
List of utility functions.
"""
import os
import re
import shutil
import time
from typing import List, Callable, Any

from pypdf import PdfReader, PdfWriter


def timeit(function: Callable):
    """
    Decorator who prints the execution time of a function.

    Parameters
    ----------
    function: Callable :
        Function to be executed.

    Returns
    -------
    function's results:
        Function's results.

    """

    def timed(*args: List, **kw: dict):
        """
        Template function.

        Parameters
        ----------
        *args :
            Template function's arguments.
        **kw :
            Template functions arguments values if given with dictionary.

        Returns
        -------
        function's results:
            Function's results.

        """
        ts = time.time()
        print('\nExecuting %r ' % function.__name__)
        result = function(*args, **kw)
        te = time.time()
        print('%r executed in %2.2f s' % (function.__name__, (te - ts)))
        return result

    return timed


def benchmark_time(input_data: Any, nb_repeat: int, *methods: Callable):
    """
    Prints mean execution time of different methods.
    These methods must have the same input data.

    Parameters
    ----------
    input_data :
        Methods' input data.
    nb_repeat :
        Number or execution of the methods.
    *methods :
        List of methods.

    Returns
    -------
    None.

    """
    for method in methods:
        ts = time.time()
        for i in range(nb_repeat):
            method(input_data)
        te = time.time()
        exec_time = (te - ts) / nb_repeat
        print(f"Method mean exec time: {method.__name__} {exec_time}")


def create_folder(folder_path: str) -> None:
    """
    Create a folder if it is not already existing.

    Parameters
    ----------
    folder_path :
        Folder's path.

    Returns
    -------
    None.

    """
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)


def export_pdfs(zotero_export_folder: str, pdfs_output_folder: str) -> None:
    """
    Export all the PDFs inside a folder to another one.
    
    The files in the subfolders are included too.

    Parameters
    ----------
    zotero_export_folder :
        The input folder where to look for PDFs.
    pdfs_output_folder :
        The output folder where PDFs are copied.

    Returns
    -------
    None.

    """
    create_folder(pdfs_output_folder)
    for root, sub_directories, files in os.walk(zotero_export_folder):
        for file in files:
            if re.match(r"[\w\s.\-–—‐=()'’&,]+\.pdf$", file):
                source_path = os.path.join(root, file)
                destination_path = os.path.join(pdfs_output_folder, file)
                shutil.copy(source_path, destination_path)
            else:
                print(file)


def add_metadata_and_rename_pdf(pdf_path: str, title: str, author: str,
                                year: str, output_folder: str = None) -> str:
    """
    Add metadata end rename a PDF file.

    Parameters
    ----------
    pdf_path: str :
        Path of the input PDF file.
    title: str :
        New title of the PDF file.
    author: str :
        Author of the PDF file.
    year: str :
        Publication year of the PDF file.
    output_folder: str :
        If defined, it indicates where the output PDF file must be created.
        (Default value = None)

    Returns
    -------
    Output path:
        Path of the output PDF file.

    """
    # TODO Get first name and surname.
    #  Handle multiple authors, length of filename, problematic characters,
    #  filename format

    if output_folder is None:
        output_folder = os.path.dirname(pdf_path)

    pdf = PdfReader(pdf_path)
    writer = PdfWriter()
    for page in pdf.pages:
        writer.add_page(page)
    writer.add_metadata(
        {
            "/Author": author,
            "/Title": title,
            "/Year": year
        }
    )

    filename = f"{author} - {year} - {title}"[:80] + ".pdf"
    output_path = os.path.join(output_folder, filename)

    with open(output_path, "wb") as f:
        writer.write(f)
    return output_path
