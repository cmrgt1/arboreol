"""
List of mathematical functions.
"""
import numpy as np
from typing import Union
import warnings


def moment(vector: np.ndarray, order: int) -> Union[float, np.ndarray]:
    """
    Compute the nth moment about the mean of a dataset using NumPy.

    Parameters
    ----------
    vector: np.ndarray :
        Data for which the nth moment is calculated.
        
    order: int :
        Order of central moment that is returned.

    Returns
    -------
    moment:
        The nth moment of the input data.

    """
    mean = np.mean(vector)
    return np.mean((vector - mean)**order)


def kurtosis(vector: np.ndarray) -> float:
    """
    Compute the kurtosis of a dataset using NumPy.

    Parameters
    ----------
    vector: np.ndarray :
        Data for which the kurtosis is calculated.

    Returns
    -------
    kurtosis:
        Kurtosis of the input data.

    """
    mu_4 = moment(vector, 4)
    mu_2 = moment(vector, 2)
    return (mu_4 - 3 * mu_2**2) / mu_2**2


def kurtosis_pearson(vector: np.ndarray) -> float:
    """
    Compute the Pearson kurtosis of a dataset using NumPy.

    Parameters
    ----------
    vector: np.ndarray :
        Data for which the Pearson kurtosis is calculated.

    Returns
    -------
    kurtosis:
        Pearson kurtosis of the input data.

    """
    mu_4 = moment(vector, 4)
    return mu_4/np.std(vector)**4


def skewness(vector: np.ndarray) -> float:
    """
    Compute the skewness of a dataset using NumPy.

    Parameters
    ----------
    vector: np.ndarray :
        Data for which the skewness is calculated.

    Returns
    -------
    skewness:
        Skewness of the input data.

    """
    mu_2 = moment(vector, 2)
    mu_3 = moment(vector, 3)
    return mu_3 / mu_2 ** (3/2)


def skewness_fusion(vector: np.ndarray) -> float:
    """
    Compute the skewness of a dataset.
    Formula written in FUSION manual is used as reference.

    Parameters
    ----------
    vector: np.ndarray :
        Data for which the skewness is calculated.

    Returns
    -------
    skewness:
        Skewness of the input data.

    """
    n = np.size(vector)
    mean = np.mean(vector)
    s = np.std(vector)
    numerator = np.sum((vector-mean)**3)
    denominator = (n-1) * s**3
    return numerator / denominator


def kurtosis_fusion(vector: np.ndarray) -> float:
    """
    Compute the kurtosis of a dataset.
    Formula written in FUSION manual is used as reference.

    Parameters
    ----------
    vector: np.ndarray :
        Data for which the kurtosis is calculated.

    Returns
    -------
    kurtosis:
        Kurtosis of the input data.

    """
    n = np.size(vector)
    mean = np.mean(vector)
    s = np.std(vector)
    numerator = np.sum((vector-mean)**4)
    denominator = (n-1) * s**4
    return numerator / denominator


def compute_percentile(values: list[float], percentiles: list[float] = None,
                       threshold: float = 0) -> list[float]:
    """
    Compute percentiles values of a list of sorted values.

    Parameters
    ----------
    values :
        Values of a dimension.
    percentiles :
        List of percentiles values that need to be computed.
    threshold :
        Only values above this threshold will be used for the computation.

    Returns
    -------
    List of percentiles values.

    """
    data_above_threshold = [z for z in values if z > threshold]
    percentiles_values = [data_above_threshold[int(
        len(data_above_threshold) * per / 100)] for per in percentiles]
    return percentiles_values


def thresholding(
        vector: np.ndarray, value: float = None, percentage: float = None,
        above: bool = True) -> np.ndarray:
    """
    Select all values of a vector according to a threshold.

    If above is True, selected values are above threshold.
    If above is False, selected values are under threshold.

    Parameters
    ----------
    vector :
        Data vector (usually normalized heights).
    value :
        Threshold value of the variable (meters for heights).
    percentage :
        Threshold percentage value
    above :
        If above, selected values are above threshold.
        If False, selected values are under threshold.

    Returns
    -------
    Selected values.

    """
    if value and percentage:
        raise ValueError("Threshold can't be defined by a percentage and a value concurrently.")
    if not value and not percentage:
        warnings.warn("No threshold were given, initial vector is returned")
        return vector

    if value and above:
        selected_values = [z for z in vector if z > value]
    elif value and not above:
        selected_values = [z for z in vector if z < value]
    elif percentage:
        if percentage > 1 or percentage < 0:
            raise ValueError("Percentage must be defined in [0;1] interval.")
        threshold_index = round(vector.size * percentage)
        print(percentage, threshold_index)
        if above:
            selected_values = vector[threshold_index:]
        else:
            selected_values = vector[:threshold_index]
    else:
        raise ValueError("Problem")
    return selected_values
