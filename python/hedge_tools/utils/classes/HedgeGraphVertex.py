# -*- coding: utf-8 -*-

from qgis.analysis import QgsGraphVertex


class HedgeGraphVertex(QgsGraphVertex):
    """
    Create a graph vertex herited from QgsGraphVertex
    """

    def __init__(self, qgs_vertex, vid, eids):
        """
        Constructor of the HedgeGraphVertex
        """
        super().__init__(qgs_vertex)

        self.vid = vid
        self.eids = eids
        self.degree = len(self.eids)
    #     self._set_vertex_eids_and_degree()
    #
    # def _set_vertex_eids_and_degree(self):
    #     """
    #     Find a graph vertex degree from incoming and outgoing edges
    #     Set the vertex degree and the eids of incoming and outgoing edges
    #     """
    #
    #     self.eids = eids
    #     self.degree = len(self.eids)
