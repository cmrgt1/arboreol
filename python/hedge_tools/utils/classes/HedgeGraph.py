# -*- coding: utf-8 -*-

import itertools
import uuid

from qgis.PyQt.QtCore import QVariant
from qgis.analysis import QgsGraph
from qgis.core import (QgsFeature,
                       QgsGeometryUtils,
                       QgsGeometry,
                       QgsLineString,
                       QgsPointXY,
                       QgsPoint)

from hedge_tools.utils.classes.HedgeGraphEdge import HedgeGraphEdge
from hedge_tools.utils.classes.HedgeGraphVertex import HedgeGraphVertex
from hedge_tools.utils.vector import attribute_table as at


class HedgeGraph(QgsGraph):
    """
    Create a graph object herited from QgsGraph class
    """
    def __init__(self, line_geometries):
        """
        Constructor of the HedgeGraph
        """
        super().__init__()

        self.vids = []
        self.eids = []
        self.degree_1_vids = []
        self.degree_1_eids = []
        self._main = []  # Used only in connected component analysis if there is disconnected subgraph
        self._subgraphs = []
        self._make_graph(line_geometries)
        self._set_degree_1_id()
        self._nb_leaf = len(self.degree_1_vids)  # Do not update private argument
        # used for leaf correction in case of subgraph.
        # nb_leaf should be accessed with len(self.degree_1_vids) otherwise

    def _make_graph(self, line_geometries):
        """
        Create a graph from an iterable of QgsGeometyr : LineString

        Parameters
        ----------
        line_goemetries : ite[QgsGeometry : LineString]
        """
        already_added = []

        for geom in line_geometries:
            line = geom.asPolyline()
            start = line[0]
            end = line[-1]

            if start in already_added and end in already_added:
                start_id = self.findVertex(start)
                end_id = self.findVertex(end)

            elif start in already_added and end not in already_added:
                end_id = self.addVertex(end)
                start_id = self.findVertex(start)
                already_added.append(end)
                self.vids.append(end_id)

            elif end in already_added and start not in already_added:
                start_id = self.addVertex(start)
                end_id = self.findVertex(end)
                already_added.append(start)
                self.vids.append(start_id)

            else:
                start_id = self.addVertex(start)
                end_id = self.addVertex(end)
                already_added += [start, end]
                self.vids += [start_id, end_id]
            edge_id = self.addEdge(start_id, end_id, [])
            self.eids.append(edge_id)

    def vertex(self, vid):
        """
        Overload of the QgsGraph.vertex method.
        Return the HedgeGrpahVertex of the given vid
        """
        qgs_vertex = super().vertex(vid)
        eids = qgs_vertex.incomingEdges() + qgs_vertex.outgoingEdges()
        eids = [eid for eid in eids if self.hasEdge(eid)]
        return HedgeGraphVertex(qgs_vertex, vid, eids)

    def edge(self, eid):
        """
        Overload of the QgsGraph.edge method.
        Return the HedgeGrpahEdge of the given vid
        """
        qgs_edge = super().edge(eid)
        return HedgeGraphEdge(qgs_edge, eid)

    def get_edge_length(self, eid):
        """
        Get the length of the edge if the given eid

        Parameters
        ----------
        eid : int
        """
        edge = self.edge(eid)
        start = self.vertex(edge.fromVertex()).point()
        end = self.vertex(edge.toVertex()).point()

        return QgsLineString([start, end]).length()

    def _set_degree_1_id(self):
        """
        Set the leaf of the graph into the degree_1_vids attribute
        """
        if not bool(self.degree_1_vids):  # Set leaf id
            for vid in self.vids:
                if self.hasVertex(vid):
                    vertex = self.vertex(vid)
                    if vertex.degree == 1:
                        self.degree_1_vids.append(vid)
                        self.degree_1_eids += vertex.eids

        else:  # Update leaf id
            deleted = []
            self.degree_1_eids = []
            for vid in self.degree_1_vids:
                if self.hasVertex(vid):
                    self.degree_1_eids += self.vertex(vid).eids
                else:
                    deleted.append(vid)
            self.degree_1_vids = list(set(self.degree_1_vids) - set(deleted))

    def remove_edges(self, eids):
        """
        Allow multiple edge deletion

        Parameters
        ----------
        eids : ite[eid]
        """
        for eid in eids:
            self.removeEdge(eid)
            self.eids.remove(eid)
            if bool(self._main):
                if eid in self._main:
                    self._main.remove(eid)
                for i, subgraph in enumerate(self._subgraphs):
                    if eid in subgraph:
                        self._subgraphs[i].remove(eid)

    def get_path(self, vid, eid):
        """
        Use case for d2 or d1 current vertex.
        Given the current vertex id get the next vertex in his path
        Return his index aswell as the edge id linking them and his length

        It'll not handle multiple relation

        Parameters
        ----------
        vid : int : vid of the current vertex
        eid : int : eid of the current edge

        Returns
        -------
        next_vid : int : of the next vertex
        next_eid : int : of the edge linking current vertex and next vertex
        length : float : of the next_edge (linking current vertex and next vertex)
        """
        edge = self.edge(eid)

        next_vid = list(set(edge.vids) - {vid})[0]
        next_vertex = self.vertex(next_vid)
        if next_vertex.degree == 2:
            next_eid = list(set(next_vertex.eids) - {eid})[0]
        else:
            next_eid = None
        return next_vid, next_eid, self.get_edge_length(eid)

    def find_neighbours(self, vid):
        """
        In case we start path at a fork retrieve next vertices and
        next associated eids

        Parameters
        ----------
        vid : int : vid of the current vertex

        Returns
        -------
        neiohbours : set : vid of neighbours vertices of d3 or + vertex
        """
        vertex = self.vertex(vid)
        eids = vertex.eids
        neighbours = set()
        for eid in eids:
            if self.hasEdge(eid):
                neighbours.update(self.edge(eid).vids)

        neighbours -= {vid}

        return neighbours

    def find_neighbours_from_eid(self, eid):
        """
        From a given edge, retrieve eids of adjacent edge.

        Parameters
        ----------
        eid : int : edge id

        Returns
        -------
        neighbours : ite[eid] : edge ids of adjacent edge of input eid
        """
        # Get all eid
        neighbours = set()
        for vid in self.edge(eid).vids:
            if self.hasVertex(vid):
                neighbours.update(self.vertex(vid).eids)

        # Remove the given one
        neighbours -= {eid}

        return neighbours

    @staticmethod
    def store_path(path_dict, vid_1, vid_2, length, eids):
        """
        Store the path from two given vertex in the given pattern
            {vid1: {vid2: [length of path, ite[eid]]}

        Parameters
        ----------
        path_dict : dict
        vid_1 : First key of the dictionnary. Vertex where the path end
        vid_2: Second key of the dict. Vertex where the path start
        length : float : length of path
        eids : ite[eid] : eids of the path

        Returns
        -------
        path_dict : dict
        """
        if vid_1 in path_dict:
            if vid_2 in path_dict[vid_1]:
                name = str(uuid.uuid4())[0:3]
                path_dict[vid_1][str(vid_2) + "_" + name] = [length, eids]
            else:
                path_dict[vid_1][vid_2] = [length, eids]

        else:
            path_dict[vid_1] = {vid_2: [length, eids]}

        return path_dict

    def find_closest_d3(self, vid, path_dict, threshold):
        """
        Given a d1 vertex it will find the closest degree 3 vertex and return
        the path ot it as well as the length of the path and the vid of the d3 vertex

        Parameters
        ----------
        vid : int : Vid of the start vertex
        path_dict : Dictionnary that store the path d1 to d3 of the current iteration
        threshold : float : maximal length allowed for a path

        Returns
        -------
        Boolean : True if path found otherwise False
        path_dict : Updated path dict with the nex path
        """
        start_vid = vid
        eid = self.vertex(vid).eids[0]  # As a leaf it has only one hedge

        total_length = 0
        eids = []

        while True:
            next_vid, next_eid, length = self.get_path(vid, eid)
            next_vertex = self.vertex(next_vid)
            total_length += length
            eids.append(eid)
            if total_length <= threshold:
                if next_vertex.degree >= 3:  # Exit condition
                    path_dict = HedgeGraph.store_path(path_dict, next_vid, start_vid, total_length, eids)
                    return True, path_dict

                elif next_vertex.degree == 2:  # Init with next vertex and next edge
                    vid = next_vid
                    eid = next_eid

                elif next_vertex.degree == 1:  # Do not keep d1 to d1 path
                    return False, False
            else:
                return False, False

    def find_closest_vertex(self, vid, eid, path_dict, visited):
        """
        Given a d1 vertex it will find the closest vertex of degree 1 or 3+ and return
        the path ot it as well and both the end vid and start vid

        Parameters
        ----------
        vid : int : Vid of the start vertex
        eid : int : current eid
        path_dict : Dictionnary that store the path d1 to d3 of the current iteration
        visited : set : eid of visited edge

        Returns
        -------
        Boolean : True if path found otherwise False
        path_dict : Updated path dict with the nex path
        """
        start_vid = vid
        total_length = 0  # Length is not necessary for this method
        eids = []

        while True:
            next_vid, next_eid, length = self.get_path(vid, eid)
            next_vertex = self.vertex(next_vid)
            total_length += length
            eids.append(eid)

            if next_vertex.degree == 1 or next_vertex.degree >= 3:
                path_dict = HedgeGraph.store_path(path_dict, next_vid, start_vid, total_length, eids)
                visited.update(eids)
                return True, path_dict, visited

            elif self.hasEdge(next_eid) is False:
                return False, False, False

            elif next_vertex.degree == 2:  # Init with next vertex and next edge
                vid = next_vid
                eid = next_eid

    def leaf_deletion(self, path_dict):
        """
        Unpack path_dict : {d3_vid : {d1_vid : [length of path, ite[eid]}}
        For each ddegree 3 node (d3_vid) add to del list
        all the eid except the ones in the longest one

        Parameters
        ----------
        path_dict : dict[d3_vid][d1_vid] = [length, ite[eid]]
        local_threshold : int : Maximal value for deletion

        Returns
        -------
        results : boolean : True if path were deleted, False otherwise
        """
        del_dict = {}
        deletion = 0
        min_leaf = 2
        if bool(self._subgraphs):  # Quick fix in case graph is disconnected
            min_leaf *= len(self._subgraphs)

        for t_vid, v in path_dict.items():
            if len(v) > 1:
                # v2[0] == length of path --> get longest path start vid
                if bool(self._subgraphs):

                    # If subgraph check if it's a smallgraph of just one degree 3 node
                    # If that's the case there will be a path deletion where
                    # nb_leaf doesn't change, so we add one at our nb_leaf count
                    t_node_eids = list(itertools.chain(*[eid for (_, eid) in v.values()]))
                    t_node_eids.sort()
                    if t_node_eids in self._subgraphs:
                        self._nb_leaf += 1
                temp = {o_vid: v2[0] for (o_vid, v2) in v.items()}
                id_max = max(temp, key=temp.get)
                for o_vid, (length, id_path) in v.items():
                    if o_vid != id_max:
                        del_dict[length] = id_path
            else:
                for o_vid, (length, id_path) in v.items():
                    del_dict[length] = id_path

        while bool(del_dict):
            idx_min = min(del_dict)
            # if idx_min < local_threshold or len(del_dict[idx_min]) == 1:
            self.remove_edges(del_dict[idx_min])
            del_dict.pop(idx_min)
            deletion += 1
            self._nb_leaf -= 1
            # Exit condition ether nb_leaf = min_leaf
            # or all the key of the dict are >= thresh value
            if self._nb_leaf == min_leaf:  #or all([k >= local_threshold for k in del_dict.keys()]):
                break

        if deletion != 0:
            return True
        else:
            return False

    def correct_leaf(self, polygon_geometry):
        """
        Will snap the leaf to the closest vertex of an englobing polygon
        Update graph accordingly

        This method should be call on for an instance that does not have been
        subjected to edge or vertex deletion

        Parameters
        ----------
        polygon_geometry : QgsGeometry : Polygon
        """
        temp_d1_vids = []
        temp_d1_eids = []
        del_edges = []
        for eid in self.degree_1_eids:
            edge = self.edge(eid)
            start_vid = edge.vids[0]
            end_vid = edge.vids[-1]
            if start_vid in self.degree_1_vids and end_vid not in self.degree_1_vids:
                # Thats not a one edge graph, proceed to correction
                leaf = self.vertex(start_vid)
                vertex = self.vertex(end_vid)
            elif start_vid not in self.degree_1_vids and end_vid in self.degree_1_vids:
                # Thats not a one edge graph, proceed to correction
                leaf = self.vertex(end_vid)
                vertex = self.vertex(start_vid)
            else:
                continue
            # new_leaf, _, _, _, _ = polygon_geometry.closestVertex(leaf.point())
            polygon = polygon_geometry.constGet()
            new_leaf = QgsPointXY(
                QgsGeometryUtils.closestPoint(polygon, QgsPoint(leaf.point())))

            vid = self.addVertex(new_leaf)
            new_eid = self.addEdge(vid, vertex.vid, [])
            del_edges.append(eid)

            # Update vid and eid
            self.vids.remove(leaf.vid)
            self.vids.append(vid)
            temp_d1_vids.append(vid)

            self.eids.append(new_eid)
            temp_d1_eids.append(new_eid)
        self.degree_1_vids = temp_d1_vids
        self.degree_1_eids = temp_d1_eids
        self.remove_edges(del_edges)

    def remove_fork(self, threshold=50):
        """
        From a degree 1 vertex search for the closest degree 3 vertex.
        Store all the path going from a d3 to the closest d1.
        Delete all of them except the longest one if there are below threshold

        Parameters
        ----------
        graph : QgsGraph
        threshold : int : maximum length allowed for deletion
        """
        # Init break condition
        max_ite = 100
        iteration = 0
        min_leaf = 2
        if bool(self._subgraphs):  # Quick fix in case graph is disconnected
            min_leaf *= len(self._subgraphs)

        while self._nb_leaf > min_leaf:
            # Init second break condition and path/deletion storage
            path_dict = {}
            # Start ?DFS?
            for vid in self.degree_1_vids:
                if self.hasVertex(vid):
                    local_threshold = 5*(iteration+1)
                    local_threshold = local_threshold if local_threshold < threshold else threshold
                    results = self.find_closest_d3(vid, path_dict, local_threshold)
                    if results[0]:  # If true there is a path else do not loose current path_dict
                        path_dict = results[1]
            if bool(path_dict):  # Empty dict/ite evlauate to False
                # To avoid deleting long path with lower amount of bifurcation
                # than other we compute à tmep threshold in the 2 first iteration
                # We could incrmeent this "local threshold" to keep more bifurcation below threshold
                # if iteration < 2:
                #     local_threshold = 10 if threshold > 10 else threshold
                #     # local_threshold = (threshold // 4) + (2 * iteration)
                #     # local_threshold = local_threshold if local_threshold < threshold \
                #     # else threshold
                # else:
                #     local_threshold = threshold
                # local_threshold = threshold
                results = self.leaf_deletion(path_dict)
            else:
                results = False

            # Exit or loop condition
            if results:
                self._set_degree_1_id()  # Reload degree 1
            elif results is False and local_threshold == threshold:
                # No deletion in current loop
                # And iteration must be >= 2 to handle local_threshold
                break

            iteration += 1
            if iteration == max_ite:
                break

    def path_to_closest_vertex(self):
        """
        Construct the path to the closets retained vertex.
        This method is used to find the path that'll be regrouped a sa polyline

        path_dict dict : {arrival_vid : {start_vid : [eid of the path]}}
        """
        path_dict = {}
        visited = set()

        for vid in self.vids:
            if self.hasVertex(vid):
                if self.vertex(vid).degree >= 3 or self.vertex(vid).degree == 1:
                    for eid in self.vertex(vid).eids:
                        if self.hasEdge(eid) and eid not in visited:
                            # Init path
                            # eid = list(set(self.vertex(vid).eids).
                            #            intersection(set(self.vertex(neighbour).eids)))[0]
                            results = self.find_closest_vertex(vid, eid, path_dict, visited)
                            if results[0]:
                                path_dict = results[1]
                                visited = results[2]

        return path_dict

    def DFSUtils(self, temp, vid, eid, visited):
        """
        Recursive function inspired by DFS traversal. It checks the neighbours of a vertex
        and for each vertex will go to the end of the path.
        Each edge is added to a visited set to avoid going through multiple time in the same path

        Parameters
        ----------
        temp : ite[eid] : edge of the current subgraph
        vid : int : current vertex id
        eid : int : current edge id
        visited : set : edge id of visited edge

        Returns
        -------
        temp : ite[eid] : edge id of the current subgraph
        """
        visited.add(eid)
        temp.append(eid)

        neighbours = self.find_neighbours(vid)
        for vid in neighbours:
            for eid in self.vertex(vid).eids:
                if eid not in visited:
                    temp = self.DFSUtils(temp, vid, eid, visited)
        temp.sort()
        return temp

    def connected_component(self, return_type=False):
        """
        Connected component analysis to find out if the graph have several disconnected subgraph.
        The main graph (biggest ids) will be returned as main graph. The others will be tagged as error

        Parameters
        ----------
        return_type : boolean : default False : If True will return the connected component list

        Returns
        -------
        cc : ite[ite[eid] : list of list of subgraph. One list if all the edge are connected
        """
        visited = set()
        cc = []
        for vid in self.vids:
            for eid in self.vertex(vid).eids:
                if eid not in visited:
                    temp = []
                    cc.append(self.DFSUtils(temp, vid, eid, visited))

        if len(cc) > 1:
            self._main = max(cc, key=len)
            self._subgraphs = cc
            # self._error = list(itertools.chain.from_iterable([elem for elem in cc if len(elem) != len(self._main)]))

        if return_type:
            return cc

    def non_recursive_connected_component(self, return_type=False):
        """
        In a multithreading environment, system is conservative on the stack size
        attributed for the recursion. The maximum depth recursion depth is
        quickly reach. Higher recursion limit is a quick fix for this problem
        but shoud be scalable with number of thread thus the stack size could
        reach the entire memory resulting in a hard crash.

        This method aims to do the same thing as the connect_component method but iteratively.

        Connected component analysis to find out if the graph have several disconnected subgraph.
        The main graph (biggest ids) will be returned as main graph. The others will be tagged as error

        Parameters
        ----------
        return_type : boolean : default False : If True will return the connected component list

        Returns
        -------
        cc : ite[ite[eid] : list of list of subgraph. One list if all the edge are connected
        """
        visited = set()
        cc = []
        for eid in self.eids:
            if eid not in visited:
                temp = []  # Init subgraph
                queue = set()  # Init alternative path that'll be explored
                                # once the end of current path is reached
                visited.add(eid)
                temp.append(eid)
                queue.add(eid)
                while queue:  # While alternative path
                    queue.remove(eid)  # Remove current from queue
                    neighbours = self.find_neighbours_from_eid(eid)  # adjacent eids
                    queue.update(neighbours)
                    queue -= visited  # Remove already visited from queue
                    for eid in queue:
                        visited.add(eid)
                        temp.append(eid)
                        break
                temp.sort()
                cc.append(temp)  # If queue empty all the subgrpah is explored

        if len(cc) > 1:  # Multiple subgraph get the biggest one as main graph
            self._main = max(cc, key=len)
            self._subgraphs = cc

        if return_type:
            return cc

    def edges_to_polylines(self, output_type=False):
        """
        Export the edge of the grpah as features after regrouping
        the ones between a d1 vertex to a d3 vertex
        Create topological arc

        Parameters
        ----------
        output_type : boolean : If true return features. Return geometries otherwise

        Returns
        -------
        features : ite[QgsFeature/QgsGeometry]

        todo : handle duplicate relying the same o_nodes (allez/retour)
               handle closed loop
        """
        path_dict = self.path_to_closest_vertex()
        features = []
        geometries = []
        errors = []

        for end_vid, v in path_dict.items():
            for o_vid, (_, eids) in v.items():
                feature = QgsFeature()
                polyline = []
                if isinstance(o_vid, int):
                    vid = o_vid
                else:
                    vid = int(o_vid.split("_")[0])
                for eid in eids:
                    if self.hasEdge(eid):
                        # Find which edge vertex is connected to previous one
                        if self.edge(eid).vids[0] == vid:
                            start_vid = self.edge(eid).vids[0]
                            vid = self.edge(eid).vids[1]
                        else:
                            start_vid = self.edge(eid).vids[1]
                            vid = self.edge(eid).vids[0]
                        # Add the new vertex as point
                        point = QgsPoint(self.vertex(start_vid).point())
                        polyline.append(point)
                # Add t_vid vertex as last point
                end = QgsPoint(self.vertex(end_vid).point())
                polyline.append(end)
                # Create geometry and feature
                geometry = QgsGeometry().fromPolyline(polyline)
                if output_type:
                    feature.setGeometry(geometry)
                    if bool(self._main):  # Only if connect component has been run
                        if bool(set(eids).intersection(self._main)):
                            features.append(feature)
                        else:
                            errors.append(feature)
                    else:
                        features.append(feature)
                else:
                    geometries.append(geometry)
        if output_type:
            if bool(self._main):
                return features, errors
            else:
                return features
        else:
            return geometries

    def edges_to_lines(self, output_type=False):
        """
        Convert the edge of the instance of the HedgeGraph to a list of features

        Parameters
        ----------
        output_type : boolean : If true return features. Return geometries otherwise

        Returns
        -------
        ite[QgsFeature or QgsGeometry]

        """
        features = []
        errors = []
        geometries = []

        for eid in self.eids:
            if self.hasEdge(eid):
                edge = self.edge(eid)
                start = self.vertex(edge.fromVertex()).point()
                end = self.vertex(edge.toVertex()).point()
                line = QgsLineString([start, end])
                geom = QgsGeometry().fromPolyline(line)

                if output_type:
                    feature = QgsFeature()
                    feature.setGeometry(geom)
                    if bool(self._main):  # Only if connect component has been run
                        if eid not in self._main:
                            errors.append(feature)
                        else:
                            features.append(feature)
                    else:
                        features.append(feature)
                else:
                    geometries.append(geom)
        if output_type:
            if bool(self._main):
                return features, errors
            else:
                return features
        else:
            return geometries

    def vertices_to_nodes(self, output_type=False):
        """
        Convert the vertices of the instance of the HedgeGraph to a list of features

        Parameters
        ----------
        output_type : boolean : If true return features. Return geometries otherwise

        Returns
        -------
        ite[QgsFeature or QgsGeometry]
        """
        features = []
        geometries = []
        errors = []

        already_added = []

        if output_type:
            fields_name = ["fid", "vid", "Degree"]
            fields_type = [QVariant.Int, QVariant.Int, QVariant.Int]

        for eid in self.eids:
            if self.hasEdge(eid):
                edge = self.edge(eid)
                start = self.vertex(edge.fromVertex())
                end = self.vertex(edge.toVertex())
                if start.vid not in already_added:
                    start_pnt = QgsPoint(start.point())
                    start_geom = QgsGeometry(start_pnt)
                    feature = QgsFeature()
                    if output_type:
                        feature = at.add_fields(feature, fields_name, fields_type)
                        feature = at.set_fields_value(feature, ["Degree"], [start.degree])
                        feature.setGeometry(start_geom)
                        if bool(self._main):  # Only if connect component has been run
                            if eid not in self._main:
                                errors.append(feature)
                            else:
                                features.append(feature)
                        else:
                            features.append(feature)
                    else:
                        geometries.append(start_geom)
                    already_added.append(start.vid)

                if end.vid not in already_added:
                    end_pnt = QgsPoint(end.point())
                    end_geom = QgsGeometry(end_pnt)
                    feature = QgsFeature()
                    if output_type:
                        feature = at.add_fields(feature, fields_name, fields_type)
                        feature = at.set_fields_value(feature, ["Degree"], [end.degree])
                        feature.setGeometry(end_geom)
                        if bool(self._main):  # Only if connect component has been run
                            if eid not in self._main:
                                errors.append(feature)
                            else:
                                features.append(feature)
                        else:
                            features.append(feature)
                    else:
                        geometries.append(end_geom)

                    already_added.append(end.vid)
        if output_type:
            if bool(self._main):
                return features, errors
            else:
                return features
        else:
            return geometries
