# -*- coding: utf-8 -*-

from qgis.analysis import QgsGraphEdge


class HedgeGraphEdge(QgsGraphEdge):
    """
    Create a graph edge herited from QgsGraphEdge
    """

    def __init__(self, qgs_edge, eid):
        """
        Constructor of the HedgeGraphEdge
        """
        super().__init__(qgs_edge)
        self.eid = eid
        self.vids = []
        self._set_edge_vids()

    def _set_edge_vids(self):
        """
        Set the vid connected to this edge in vids attribute
        """
        self.vids.append(self.toVertex())
        self.vids.append(self.fromVertex())
