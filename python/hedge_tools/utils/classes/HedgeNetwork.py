# -*- coding: utf-8 -*-
from qgis.core import (QgsLineString)

# UN RESEAU DE HAIE EST IL FORCEMENT PLANAIRE ? OUI JE PENSE
# CAR LES FORMULES DIFFERES SI NON PLANAIRE
class HedgeNetwork():
    """
    Used to compute connectivity index inside a connected hedge network
    Assumption : The network is planar --> Different formula for non planar graphs

    I use and store node features instead of node_count in case 
    we want to compute vertex network metrics

    Note that unconnected hedge will min/max most of the metrics, 
    might need to filter before or return NULL (surely) to avoid statistical bias

    TODO : HedgeGraph inheritance with only first and last nodes of the 
    multipolyline segment if we go for more advanced metrics
    Might be better to use a composition as it's more logical to return the metrics 
    in one feature (subgraph) over a set of lines. Composition allow to unpack 
    the subgraph then construct the graph when inheritance will 
    throw an error during graph construction
    """
    def __init__(self, arc_feature, node_features):
        """
        Constructor of HedgeNetwork

        Parameters
        ----------
        arc_feature : QgsFeature
        nodes_feature : ite[QgsFeature]
        """
        self.arc_feature = arc_feature
        self.node_features = node_features
        self.geometry = self.arc_feature.geometry()
        self._nb_link = len(self.geometry.asGeometryCollection())
        self._nb_node = len(self.node_features)
        self._cyclomatic_number = self.nb_link - self.nb_node + 1      
        self._total_length = None

    @property
    def nb_link(self):
        """
        Return
        ------
        self._nb_link : int
        """
        return self._nb_link
    
    @property
    def nb_node(self):
        """
        Return
        ------
        self._nb_node : int
        """
        return self._nb_node
    
    @property
    def cyclomatic_number(self):
        """
        Getter of cyclomatic number
        Usually number of edges - number of vertex + number of subgraph. 
        In this use case number of subgraph is 1. This ensure that the value is not negative
        Usually show the number of route in a graph --> measure of route redundancy

        Return
        ------
        self._cyclomatic_number : int
        """
        return self._cyclomatic_number
    
    @property
    def total_length(self):
        """
        Return
        ------
        self._total_length : float
        """
        return self._total_length
    
    def compute_network_length(self):
        """
        Compute the total length of the network

        Return
        ------
        self.total_length : float
        """
        self._total_length = round(self.geometry.length(), 2)

        return self.total_length
    
    def compute_alpha_index(self):
        """
        Measure of existing circuits to the maximum possible circuits
        Note : Formula seems to change, took the one in the indicator sheet

        Return
        ------
        float
        """
        if self.nb_node > 2:
            return round(self.cyclomatic_number/(2 * self.nb_node - 5), 2)
        else:
            return 0
    
    def compute_beta_index(self):
        """
        Average number of edges per vertex (i.e average number of links per node)

        Return
        ------
        float
        """
        return round(self.nb_link/self.nb_node , 2)

    def compute_gamma_index(self):
        """
        Relationship between the number of observed links and the
        number of possible links.
        Note : Formula seems to change, took the one in the indicator sheet

        Return
        ------
        float
        """
        if self.nb_node > 2:
            return round(self.nb_link/(3 * (self.nb_node - 2)), 2)
        else:
            return 1
    
    def compute_eta_index(self):
        """
        Average length per link. Complex network tend to have a low eta value

        Return
        ------
        float
        """
        if self.total_length is None:
            self.compute_network_length()

        return round(self.total_length/self.nb_node, 2)
    
    def compute_detour_index(self):
        """
        Measure network efficiency.
        Straight distance (sd) over transport distance (total_length).

        Return
        ------
        float
        """
        if self.total_length is None:
            self.compute_network_length()

        sd = sum([QgsLineString([pnts[0], pnts[-1]]).length()
                  for pnts in self.geometry.asMultiPolyline()])

        return round(sd/self.total_length, 2)
    
    def compute_network_density(self):
        """
        Territorial occupation of the network
        Total length over the total bounding box area

        Return
        ------
        float
        """
        if self.total_length is None:
            self.compute_network_length()

        return round(self.total_length/self.geometry.boundingBox().area(), 2)
