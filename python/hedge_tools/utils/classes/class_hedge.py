import collections
import os
import statistics as stats
import tempfile
from itertools import combinations

import numpy as np
import processing
from osgeo import gdal, ogr, osr
from qgis.PyQt.QtCore import QVariant
from qgis.core import (QgsVectorLayer,
                       QgsGeometry,
                       QgsPoint,
                       QgsPointXY,
                       QgsFeatureRequest,
                       QgsFeature,
                       QgsRasterLayer,
                       NULL)
from qgis.core.additions.edit import edit

from hedge_tools.utils.raster import utils as rutils
from hedge_tools.utils.vector import attribute_table as at
from hedge_tools.utils.vector import geometry as g
from hedge_tools.utils.vector import utils as vutils


class Hedges():
    def __init__(self, in_poly_layer, in_arc_layer, in_node_layer):
        """
         A class used to facilitate switching between geometry of a hedge.

         Parameters
         ---
         in_poly_layer (QgsVectorLayer) : Polygon layer representing the hedges
         in_arc_layer (QgsVectorLayer) : Polyline layer representing the hedges
         in_node_layer (QgsVectorLayer) : Node layer representing the hedges
         """

        self.poly_layer = in_poly_layer
        self.arc_layer = in_arc_layer
        self.node_layer = in_node_layer
        self._relation_dict = collections.defaultdict(dict)
        self._make_relation()

    def _make_relation(self):
        """
         Build a nested dict storing the relation of the different geometries in an hedges.
         Ex : {key : {pid: int, eid: int, vids: [int, int]}
         """

        for idx, arc in enumerate(self.arc_layer.getFeatures()):
            # arc.setFields(self.arc_layer.fields(), initAttributes=False)
            self._relation_dict[idx]["pid"] = arc.attribute("pid")
            self._relation_dict[idx]["eid"] = arc.attribute("eid")
            self._relation_dict[idx]["vids"] = \
                [arc.attribute("vid_1"), arc.attribute("vid_2")]

    @property
    def relation(self):
        """
         Getter of the hedge relation nested dict

         Return
         ---
         self._relation_dict (nested dict) : Dict storing geometries relation of hedges
         """

        return self._relation_dict

    def get_fid_from_relation(self, in_id_type, in_id):
        """
         Getter of a specific element from the hedge relation nested dict.

         Parameters
         ---
         id_type (string) : Secondary key (id_type) of the nested dict
         id (int) : Id value of respective id_type

         Return
         ---
         pid, eid, vids : id value of related features corresponding to the id in input.

         TODO : Return both topology for nodes incase not O
         """

        if in_id_type == "pid" or in_id_type == "eid":
            row_dict = {k: v for k, v in self.relation.items() if v[in_id_type] == in_id}
        if in_id_type == "vids":
            row_dict = {k: v for k, v in self.relation.items() if in_id in v[in_id_type]}

        pid, eid, vids = [v for k, v in row_dict[list(row_dict)[0]].items()]

        poly = next(self.poly_layer.getFeatures(QgsFeatureRequest().
                                                setFilterExpression("pid = %d" %pid)))
        arc = next(self.arc_layer.getFeatures(QgsFeatureRequest().
                                                setFilterExpression("eid = %d" %eid)))
        node_1 = next(self.node_layer.getFeatures(QgsFeatureRequest().
                                                  setFilterExpression("vid = %d" %vids[0])))
        node_2 = next(self.node_layer.getFeatures(QgsFeatureRequest().
                                                  setFilterExpression("vid = %d" % vids[1])))

        return poly.id(), arc.id(), [node_1.id(), node_2.id()]

    def split_by_orientation(self, context, feedback, simpl=5, intern_angle=150,
                             ignore_dist=0, node_type_field="Node_type"):
        """
        Allow to split the hedges by orientation.
        Intra feature orientation must be computed.
        Therefore, an over simplification of the median axis is done to avoid noise.
        An option to ignore L node close to extremities( 25 by default) is given

        Parameters
        ----------
        self.poly_layer: QgisObject : QgsVectorLayer : Polygon
            Polygons layer
        self.arc_layer: QgisObject : QgsVectorLayer : LineString
            Skeleton of the polygons layer
        self.node_layer: QgisObject : QgsVectorLayer : Nodes
            Node layer representing hedge extrimities
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        simpl : float : Default 5 : Simplification value for the arc.
            Arcs must reflect orientation that you need.
        intern_angle : int : Default 150 : Intern angle below a node will be classified as L (vertex)
        ignore_dist : int : Default 0 : If different of 0.
            erase one of the two cutline too close to each other
        node_type_field : string : Default Node_type :
            Field used to store node type (O,L,T, ...)

        Returns
        -------
        outpoly_layer : QgisObject : QgsVectorLayer : Polygon
            New polygon layer split with orientation
        outarc_layer : QgisObject : QgsVectorLayer : LineString
            New arc layer split with orientation
        node_layer : QgisObject : QgsVectorLayer : Node
            New node layer split with orientation
        """
        # Init progress bar steps
        alg_number = 9
        step_per_alg = int(100/alg_number)

        # Create copy of the layers as we don't want to modify our base class
        poly_layer = self.poly_layer
        arc_layer = self.arc_layer
        node_layer = self.node_layer

        # Get pr
        pr_node = node_layer.dataProvider()

        # Get field index
        idx_node_id = node_layer.fields().indexFromName("vid")
        idx_node_fid = node_layer.fields().indexFromName("fid")

        idx_fk_node_1 = arc_layer.fields().indexFromName("vid_1")
        idx_fk_node_2 = arc_layer.fields().indexFromName("vid_2")

        feedback.pushInfo("Removing small orientation changes")

        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Simplify it to retain only big orientation change
        alg_name = "hedgetools:modifymedianaxis"
        params = {"INPUT_POLY": poly_layer,
                  "INPUT_ARC": arc_layer,
                  "INPUT_NODE": node_layer,
                  "INPUT_ALG": [1],
                  "INPUT_DEV": simpl,
                  "OUTPUT_ARC": "TEMPORARY_OUTPUT",
                  "OUTPUT_NODE": "TEMPORARY_OUTPUT"}
        arc_simp = processing.run(alg_name, params)["OUTPUT_ARC"]

        # Set progress
        feedback.setProgress(step_per_alg)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Get node
        alg_name = "native:extractvertices"
        params = {"INPUT": arc_simp,
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        node_dupe = processing.run(alg_name, params, context=context)["OUTPUT"]

        # Set progress
        feedback.setProgress(step_per_alg * 2)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Delete dupe node
        alg_name = "native:deleteduplicategeometries"
        params = {"INPUT": node_dupe,
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        vertex_layer = processing.run(alg_name, params, context=context)["OUTPUT"]
        del(node_dupe)

        # Set progress
        feedback.setProgress(step_per_alg * 3)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Computing changes in orientation and reporting it to original geometry")

        # Get field max value
        max_id = node_layer.maximumValue(idx_node_id)
        max_fid = node_layer.maximumValue(idx_node_fid)
        # Create nodes that respect angle condition
        feature_dict = {}
        for vertex in vertex_layer.getFeatures():
            # Get parent arc
            _, fid_arc, _ = self.get_fid_from_relation("eid", vertex["eid"])
            arc = arc_simp.getFeature(fid_arc)
            if ignore_dist != 0:
                pnt = vertex.geometry().asPoint()
                dist_start, dist_end = g.distance_from_extremities(arc, pnt)

                # If too close of an extremities, ignore it
                if dist_start < ignore_dist or dist_end < ignore_dist:
                    continue

            # Compute angle
            _, _, before, after, _ = arc.geometry().closestVertex(vertex.geometry().asPoint())
            if before != -1 and after != -1:
                prev_node = arc.geometry().vertexAt(before)
                next_node = arc.geometry().vertexAt(after)
                angle = g.get_angle(vertex.geometry().asPoint(), QgsGeometry(prev_node), QgsGeometry(next_node))
                if angle < intern_angle:
                    # Get real node on real median axis and add it to a new node_layer
                    real_arc = arc_layer.getFeature(fid_arc)
                    real_vertex = real_arc.geometry().nearestPoint(vertex.geometry())

                    # Add new node
                    new_node = QgsFeature()
                    new_node.setGeometry(real_vertex)
                    new_node.setFields(node_layer.fields())
                    new_node["fid"] = max_fid + 1
                    new_node["vid"] = max_id + 1
                    new_node["Degree"] = 2
                    new_node[node_type_field] = "L"
                    if vertex["eid"] in feature_dict:
                        feature_dict[vertex["eid"]].append(new_node)
                    else:
                        feature_dict[vertex["eid"]] = [new_node]
                    max_fid += 1
                    max_id += 1
        del(vertex_layer)

        # Remove vertex in feature dict too close to each other
        expression = "eid in {}".format(tuple(feature_dict.keys()))
        request = QgsFeatureRequest().setFilterExpression(expression)
        for arc in arc_simp.getFeatures(request):
            arc_geom = arc.geometry()
            if len(feature_dict[arc["eid"]]) > 1:
                for n_1, n_2 in combinations(feature_dict[arc["eid"]], 2):
                    pnt_1 = n_1.geometry().asPoint()
                    pnt_2 = n_2.geometry().asPoint()

                    arc_geom = g.vertex_add(arc_geom, pnt_1.x(), pnt_1.y())
                    arc_geom = g.vertex_add(arc_geom, pnt_2.x(), pnt_2.y())
                    arc_feat = QgsFeature()
                    arc_feat.setGeometry(arc_geom)
                    # Check for distance between them
                    dist_start_1, _ = g.distance_from_extremities(arc_feat,
                                                                pnt_1)
                    dist_start_2, _ = g.distance_from_extremities(arc_feat,
                                                                pnt_2)
                    if abs(dist_start_2 - dist_start_1) < ignore_dist:
                        try:
                            feature_dict[arc["eid"]].remove(n_2)
                        except ValueError:
                            pass

        # Transform feature_dict to a list
        feature_list = sum(feature_dict.values(), [])
        pr_node.addFeatures(feature_list)

        # Set progress
        feedback.setProgress(step_per_alg * 4)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Building new arcs and updating topology")

        # Run the topological network algorithm to create arcs between nodes
        arc_layer = g.update_arc_and_identifiers(node_layer, arc_layer)

        # Set progress
        feedback.setProgress(step_per_alg * 5)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # As fk_vid has been recomputed by topological network
        # we delete old ones who were truncated by v.net
        with edit(arc_layer):
            arc_layer.deleteAttributes([idx_fk_node_1, idx_fk_node_2])

        # Cut polygon
        feedback.pushInfo("Cut lines creation")
        request = QgsFeatureRequest().setFilterExpression("%s = 'L'" %node_type_field)
        cutline_layer = g.make_cutline(poly_layer, arc_layer, node_layer,
                                        inrequest=request)
        # Set progress
        feedback.setProgress(step_per_alg * 6)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Cut lines validation")
        cutline_valid = g.validate_cutline(cutline_layer, arc_layer, poly_layer)
        del(cutline_layer)

        # Set progress
        feedback.setProgress(step_per_alg * 7)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Cutting polygons with cut lines")
        outpoly_layer, outarc_layer = g.use_cutline(cutline_valid, poly_layer,
                                                     arc_layer, node_layer)
        del(cutline_valid)

        # Set progress
        feedback.setProgress(step_per_alg * 8)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        return outpoly_layer, outarc_layer, node_layer

    def split_by_distance(self, context, feedback, distance, ignore_dist=0, node_type_field="Node_type"):
        """
        Split regularly the hedges by a length criteria given by the users.
        Nodes created this way are labelled "L"

        Parameters
        ----------
        self.poly_layer: QgisObject : QgsVectorLayer : Polygon
            Polygons layer
        self.arc_layer: QgisObject : QgsVectorLayer : LineString
            Skeleton of the polygons layer
        self.node_layer: QgisObject : QgsVectorLayer : Nodes
            Node layer representing hedge extrimities
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        distance : int : Distance criteria to split the hedges
        ignore_dist : int : Default 0 : If different of 0.
            erase one of the two cutline too close to each other
        node_type_field : string : Default Node_type :
            Field used to store node type (O,L,T, ...)

        Returns
        -------
        outpoly_layer : QgisObject : QgsVectorLayer : Polygon
            New polygon layer split by length
        outarc_layer : QgisObject : QgsVectorLayer : LineString
            New arc layer split by length
        node_layer : QgisObject : QgsVectorLayer : Node
            New node layer split by length
        """
        # Init progress bar steps
        alg_number = 8
        step_per_alg = int(100/alg_number)

        # Create copy of the layers as we don't want to modify our base class
        poly_layer = self.poly_layer
        arc_layer = self.arc_layer
        node_layer = self.node_layer

        pr_node = node_layer.dataProvider()

        # Get field idx
        idx_node_id = node_layer.fields().indexFromName("vid")
        idx_node_fid = node_layer.fields().indexFromName("fid")
        idx_node_type = node_layer.fields().indexFromName("%s" % node_type_field)
        idx_fk_node_1 = arc_layer.fields().indexFromName("vid_1")
        idx_fk_node_2 = arc_layer.fields().indexFromName("vid_2")

        feedback.pushInfo("Creating nodes at given interval")

        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Create point along line given distance criteria
        alg_name = "native:pointsalonglines"
        params = {"INPUT": arc_layer,
                  "DISTANCE": distance,
                  "START_OFFSET": distance,
                  "END_OFFSET": distance,
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        new_node_layer = processing.run(alg_name, params, context=context)["OUTPUT"]

        # Set progress
        feedback.setProgress(step_per_alg)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Integrating nodes into arcs")

        # Ignore_dist selection and adding new nodes in node_layer
        max_id = node_layer.maximumValue(idx_node_id)
        max_fid = node_layer.maximumValue(idx_node_fid)
        features = []
        for node in new_node_layer.getFeatures():
            # Get parent arc
            _, fid_arc, _ = self.get_fid_from_relation("eid", node["eid"])
            arc = arc_layer.getFeature(fid_arc)
            if ignore_dist != 0:
                # Get vertex id of the node
                arc_length = arc.geometry().length()
                upper = arc_length - ignore_dist
                lower = ignore_dist
                if node["distance"] < lower or node["distance"] > upper:
                    continue

            # max_id = node_layer.maximumValue(idx_node_id)
            # max_fid = node_layer.maximumValue(idx_node_fid)

            # Add new node
            max_fid += 1
            max_id += 1

            new_node = QgsFeature()
            new_node.setGeometry(node.geometry())
            new_node.setFields(node_layer.fields())
            new_node["fid"] = max_fid
            new_node["vid"] = max_id
            new_node["Degree"] = 2
            new_node[node_type_field] = "temp"
            features.append(new_node)
            
            # pr_node.addFeature(new_node)
            # node_layer.updateExtents()
        pr_node.addFeatures(features)
        node_layer.updateExtents()
        
        # Set progress
        feedback.setProgress(step_per_alg * 2)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        alg_name = "native:snapgeometries"
        params = {"INPUT": arc_layer,
                  "REFERENCE_LAYER": node_layer,
                  "TOLERANCE": 0.1,
                  "BEHAVIOR": 0,
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        arc_layer = processing.run(alg_name, params, context=context)["OUTPUT"]

        # Set progress
        feedback.setProgress(step_per_alg * 3)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Building new arcs and updating topology")

        # Run the topological network algorithm to create arcs between nodes
        arc_layer = g.update_arc_and_identifiers(node_layer, arc_layer)
        
        # Set progress
        feedback.setProgress(step_per_alg * 4)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # As fk_vid has been recomputed by topological network
        # we delete old ones who were truncated by v.net
        # with edit(arc_layer):
        #     arc_layer.deleteAttributes([idx_fk_node_1, idx_fk_node_2])

        # Cut polygon
        feedback.pushInfo("Cut lines creation")
        request = QgsFeatureRequest().setFilterExpression("%s = 'temp'" % node_type_field)
        cutline_layer = g.make_cutline(poly_layer, arc_layer, node_layer,
                                        inrequest=request)

        # Set progress
        feedback.setProgress(step_per_alg * 5)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Cut lines validation")
        cutline_valid = g.validate_cutline(cutline_layer, arc_layer, poly_layer)
        del(cutline_layer)

        # Set progress
        feedback.setProgress(step_per_alg * 6)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Cutting polygons with cut lines")
        outpoly_layer, outarc_layer = g.use_cutline(cutline_valid, poly_layer,
                                                     arc_layer, node_layer)
        del(cutline_valid)

        # Set progress
        feedback.setProgress(step_per_alg * 7)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Rename temp node to L
        # Init attr map
        node_attr_map = {}
        for node in node_layer.getFeatures(request):
            node_attr_map[node.id()] = {idx_node_type: "L"}
        # Edit node layer with map
        node_layer.dataProvider().changeAttributeValues(node_attr_map)

        return outpoly_layer, outarc_layer, node_layer

    def split_by_interface(self, context, feedback, itf_layer, ori_bound=45,
                           ignore_dist=25,
                           node_type_field="Node_type"):
        """
        Split regularly the hedges by an adjacent interface changes
        Nodes created this way are labelled "L"
        The accuracy of the algorithm is directly linked to numerisation quality of interface layer.
        A little correction (snap) will be done in option but it is strongly advised
        that you correct the layer if you use one with overlap or unsnapped geometries

        Parameters
        ----------
        self.poly_layer: QgisObject : QgsVectorLayer : Polygon
            Polygons layer
        self.arc_layer: QgisObject : QgsVectorLayer : LineString
            Skeleton of the polygons layer
        self.node_layer: QgisObject : QgsVectorLayer : Node
            Nodes layer representing hedge extremities
        self.itf_layer : QgisObject : QgsVectorLayer : Polygons
            Polygon layer representing the interface to cut hedges with.
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        itf_layer : QgisObject : QgsVectorLayer : Polygon
            Interface layer
        ori_bound : int : Junction orientation bounds to allow cutting the hedges with it.
            ori junction - 45 > ori hedge or ori junction + 45 < ori_hedge
        ignore_dist : int : Default 0 : If different from 0,
            erase one of the two cutline too close to each other
        node_type_field : string : Default Node_type :
            Field used to store node type (O,L,T, ...)

        Returns
        -------
        outpoly_layer : QgisObject : QgsVectorLayer : Polygon
            New polygon layer split by length
        outarc_layer : QgisObject : QgsVectorLayer : LineString
            New arc layer split by length
        node_layer : QgisObject : QgsVectorLayer : Node
            New node layer split by length
        """
        # Init progress bar steps
        alg_number = 7
        step_per_alg = int(100 / alg_number)

        # Create copy of the layers as we don't want to modify our base class
        poly_layer = self.poly_layer
        arc_layer = self.arc_layer
        node_layer = self.node_layer

        # Set progress
        feedback.setProgress(step_per_alg * 2)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Cut lines creation")
        cutline_layer = g.make_cutline_from_itf(poly_layer,
                                                 arc_layer,
                                                 itf_layer,
                                                 ori_bound=ori_bound)

        # Set progress
        feedback.setProgress(step_per_alg * 3)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Cut lines validation and nodes creation")
        cutline_layer, outnode_layer = g.validate_cutline_from_itf(
                                                    arc_layer, node_layer,
                                                    cutline_layer,
                                                    ignore_dist=ignore_dist,
                                                    node_type_field=node_type_field)

        # Set progress
        feedback.setProgress(step_per_alg * 3)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Updating topology
        feedback.pushInfo("Building new arcs and updating topology")

        arc_layer = g.update_arc_and_identifiers(outnode_layer, arc_layer)
        
        # Set progress
        feedback.setProgress(step_per_alg * 4)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Validation
        feedback.pushInfo("Cut lines validation")

        cutline_valid = g.validate_cutline(cutline_layer, arc_layer,
                                            poly_layer)
        del(cutline_layer)

        # Set progress
        feedback.setProgress(step_per_alg * 5)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Cut polygons with cutline
        feedback.pushInfo("Cutting polygons with cut lines")

        outpoly_layer, outarc_layer = g.use_cutline(cutline_valid, poly_layer,
                                                     arc_layer, node_layer)

        # Set progress
        feedback.setProgress(step_per_alg * 6)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        return outpoly_layer, outarc_layer, outnode_layer

    def compute_width(self, context, feedback, method, pnt_dist=3):
        """
         Compute the median width and the average width (with no outliers)
         of each polygon in a ht_width field in a copy of the input polygons layer.
         Width calculation is estimated with regular dense transect along the medial axis.

         Parameters
         ---
         self.poly_layer: QgisObject : QgsVectorLayer : Polygon
             Layer containing the polygons to measure.
         self.arc_layer: QgisObject : QgsVectorLayer : LineString
             Skeleton of the polygons layer
         context : QgsContext : Context in which the tools are called
         feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
         method: Int : Either transect (0) method, bounding box(1) method or both.
         pnt_dist: Int: Default 3.
             Distance step for width computation.

         Return
         ---
         ioself.poly_layer : Poly layer with 2 new attributes

         TODO: Handle progress bar when both of method are used
               Handle transect cut better to avoid getting 200+ meter wide hedge
         """

        if 0 in method:
            # Init progress bar steps
            alg_number = 6
            step_per_alg = int(100 / alg_number)

            feedback.pushInfo("Creating transect")

            # Check for cancellation
            if feedback.isCanceled():
                return {}

            # Make copy of arcs layer
            arc_copy = vutils.create_layer(self.arc_layer, copy_feat=True,
                                       copy_field=True)

            # Create dense line of points on the medial axis
            alg_name = "native:pointsalonglines"
            params = {"INPUT": arc_copy,
                      "DISTANCE": pnt_dist,
                      "START_OFFSET": 1,
                      "END_OFFSET": 1,
                      "OUTPUT": "TEMPORARY_OUTPUT"}
            tr_node_layer = processing.run(alg_name, params, context=context)["OUTPUT"]

            # Set progress
            feedback.setProgress(step_per_alg)
            # Check for cancellation
            if feedback.isCanceled():
                return {}

            # Init node attr map
            node_geom_map = {}

            with edit(arc_copy):
                # Add nodes layer as vertex to arcs layer
                for node in tr_node_layer.getFeatures():

                    # Get parent arc
                    # Just in case there is some bug
                    if node["eid"] != NULL:
                        _, fid_arc, _ = self.get_fid_from_relation("eid", node["eid"])
                        parent_arc = arc_copy.getFeature(fid_arc)
                        # Don't duplicate nodes on arc vertex in this version.
                        # Small negligeable bias.

                        # Insert node into the arc
                        x = node.geometry().asPoint().x()
                        y = node.geometry().asPoint().y()

                        # If it's not a small end lines created just for snapping skelet to hedges
                        if parent_arc.geometry().length() > 2:
                            # If x,y is start vertex or end vertex move it a little to avoid duplication
                            _, at, _, after, _ = parent_arc.geometry().closestVertex(
                                QgsPointXY(x, y))
                            # That's the start vertex
                            if at == 0:
                                x2, y2 = parent_arc.geometry().interpolate(2).asPoint()
                            # That's the end vertex
                            elif after == -1:
                                # Reverse the line before moving the transect point
                                reversedArc = g.reverse_polyline(parent_arc.geometry())
                                x2, y2 = reversedArc.interpolate(2).asPoint()
                            else:
                                x2, y2 = x, y

                            # Add the vertex to the arc
                            new_geom = g.vertex_add(parent_arc.geometry(), x2, y2)

                            # Edit changes in arc
                            arc_copy.changeGeometry(parent_arc.id(), new_geom)

                            # Add node modificaiton in node_attr_map
                            node_geom_map[node.id()] = QgsGeometry(QgsPoint(x2, y2))

            # Edit all the changes at once
            tr_node_layer.dataProvider().changeGeometryValues(node_geom_map)

            # Set progress
            feedback.setProgress(step_per_alg * 2)
            # Check for cancellation
            if feedback.isCanceled():
                return {}

            # Compute transect
            alg_name = "native:transect"
            params = {"INPUT": arc_copy,
                      "LENGTH": 200,
                      "ANGLE": 90,
                      "SIDE": 2,  # Both side
                      "OUTPUT": "TEMPORARY_OUTPUT"}
            tr_layer = processing.run(alg_name, params, context=context)["OUTPUT"]
            del(arc_copy)

            # Set progress
            feedback.setProgress(step_per_alg * 3)
            # Check for cancellation
            if feedback.isCanceled():
                return {}

            # Spatial index
            layer_list = [tr_layer, tr_node_layer]
            alg_name = "native:createspatialindex"

            for layer in layer_list:
                params = {"INPUT": layer}
                processing.run(alg_name, params, context=context)

            # Extract the transect of the input nodes
            alg_name = "native:extractbylocation"
            params = {"INPUT": tr_layer,
                      "PREDICATE": 0,  # Intersects
                      "INTERSECT": tr_node_layer,
                      "OUTPUT": "TEMPORARY_OUTPUT"}
            width_tr_layer = processing.run(alg_name, params, context=context)["OUTPUT"]
            del(tr_layer)

            # Set progress
            feedback.setProgress(step_per_alg * 4)
            # Check for cancellation
            if feedback.isCanceled():
                return {}

            feedback.pushInfo("Clipping transect by polygons")

            # Init geom map
            tr_geom_map = {}
            # Cut by parent poly
            for tr in width_tr_layer.getFeatures():

                # Get parent poly
                fid_poly, _, _ = self.get_fid_from_relation("pid", tr["pid"])
                parent_poly = self.poly_layer.getFeature(fid_poly)
                # expression = "pid = %d" %tr["pid"]
                # request = QgsFeatureRequest().setFilterExpression(expression)
                # parent_poly = next(self.poly_layer.getFeatures(request))

                # Get "parent" node
                _, parent_node_list = g.get_clementini(tr_node_layer, tr)
                parent_node = parent_node_list[0]

                # Get line from first boundary crossing
                closest_inter = tr.geometry().intersection(
                    parent_poly.geometry()).asGeometryCollection()

                # Handle error when only point intersection exist
                len_inter = -1  # In case point intersection (length = 0)

                # To ensure correct intersection we get the one intersecting the parent node
                for idx, seg in enumerate(closest_inter):
                    if seg.intersects(parent_node.geometry().buffer(0.1, 5)):
                        len_inter = seg.length()
                        real_idx = idx

                # If point there is a problem skip it
                if len_inter <= 0:
                    continue

                # Add changes to map
                tr_geom_map[tr.id()] = QgsGeometry(closest_inter[real_idx])

            # Edit geometry changes
            width_tr_layer.dataProvider().changeGeometryValues(tr_geom_map)
            del(tr_geom_map)

            # Set progress
            feedback.setProgress(step_per_alg * 5)
            # Check for cancellation
            if feedback.isCanceled():
                return {}

            # Prepare fields_list for creation
            fields_list = [("ht_width_med", QVariant.Double),
                           ("ht_width_avg", QVariant.Double)]

            # Create new attributes if they already exist they will be overwritten
            at.create_fields(self.poly_layer, fields_list)

            # Get fields index
            idx_med = self.poly_layer.fields().indexFromName("ht_width_med")
            idx_avg = self.poly_layer.fields().indexFromName("ht_width_avg")

            feedback.pushInfo("Computing and storing width fields")

            # Init attributes map
            poly_attr_map = {}

            # Store the len of each transect for a polygon,
            # Get median width and average width then store them in parent polygon
            for poly in self.poly_layer.getFeatures():

                # Init length List
                len_list = []

                # Get poly transect
                request = QgsFeatureRequest().setFilterExpression(
                    "pid = %d" % poly["pid"])
                for tr in width_tr_layer.getFeatures(request):
                    len_list.append(tr.geometry().length())

                if len(len_list) != 0:
                    # Compute median
                    med = round(stats.median(len_list), 2)

                    #  Get IQR
                    q25, q75 = np.percentile(len_list, 25), np.percentile(
                        len_list, 75)
                    iqr = q75 - q25

                    # Get outliers thresh
                    thresh = iqr * 1.5
                    lower, upper = q25 - thresh, q75 + thresh

                    # Remove outliers
                    # Special case of small polygons where there is not enough transect
                    if q25 == q75:
                        no_outliers_list = len_list
                    else:
                        no_outliers_list = [l for l in len_list if
                                            l > lower and l < upper]

                    # Compute average
                    if len(no_outliers_list) != 0:
                        avg = round(sum(no_outliers_list) / len(no_outliers_list), 2)

                    # Store attributes value in map
                    poly_attr_map[poly.id()] = {idx_med: med, idx_avg: avg
                                                if len(no_outliers_list) != 0 else NULL}
                else:
                    continue

            # Edit layer with map
            self.poly_layer.dataProvider().changeAttributeValues(poly_attr_map)

            # Set progress
            feedback.setProgress(step_per_alg * 6)
            # Check for cancellation
            if feedback.isCanceled():
                return {}

        if 1 in method:

            feedback.pushInfo("Computing width with shape")

            # Init progress bar steps
            count = self.arc_layer.featureCount()

            # Init output field
            fields_list = [("ht_width_ratio", QVariant.Double)]
            at.create_fields(self.poly_layer, fields_list)
            idx_ratio = self.poly_layer.fields().indexFromName("ht_width_ratio")

            # Init attributes map
            poly_attr_map = {}

            # Building request to iterate in the same order over two layers
            request_poly = QgsFeatureRequest()
            clause_poly = QgsFeatureRequest.OrderByClause("pid",
                                                          ascending=True)
            order_poly = QgsFeatureRequest.OrderBy([clause_poly])
            request_poly.setOrderBy(order_poly)
            ite_poly = self.poly_layer.getFeatures(request_poly)

            request_arc = QgsFeatureRequest()
            clause_arc = QgsFeatureRequest.OrderByClause("pid",
                                                         ascending=True)
            order_arc = QgsFeatureRequest.OrderBy([clause_arc])
            request_arc.setOrderBy(order_arc)
            ite_arc = self.arc_layer.getFeatures(request_arc)

            for current, (poly, arc) in enumerate(zip(ite_poly, ite_arc)):
                bb_area = poly.geometry().area()
                arc_lgth = arc.geometry().length()

                width = round(bb_area/arc_lgth, 2)

                poly_attr_map[poly.id()] = {idx_ratio: width}

                # Set progress
                feedback.setProgress(current//count * 100)
                # Check for cancellation
                if feedback.isCanceled():
                    return {}

            # Edit layer with map
            self.poly_layer.dataProvider().changeAttributeValues(poly_attr_map)

    def compute_length(self, context, feedback):
        """
        Compute the length of the median axis as a new field on median axis layer.

        Parameters
        ----------
        self.poly_layer: QgisObject : QgsVectorLayer : Polygon
            Layer containing the polygons to measure.
        self.arc_layer: QgisObject : QgsVectorLayer : LineString
            Skeleton of the polygons layer
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        Returns
        -------
        ioself.arc_layer : Arc layer with a length attribute
        """
        # Init progress bar steps
        count = self.arc_layer.featureCount()

        feedback.pushInfo("Computing and storing length field")

        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Prepare fields_list for creation
        fields_list = [("ht_length", QVariant.Double)]

        # Create new attributes if they already exist they will be overwriten
        at.create_fields(self.arc_layer, fields_list)

        # Get field index
        idx = self.arc_layer.fields().indexFromName("ht_length")
        # Init attr_map
        arc_attr_map = {}
        # Populate length field
        for current, arc in enumerate(self.arc_layer.getFeatures()):
            length = round(arc.geometry().length(), 2)
            arc_attr_map[arc.id()] = {idx: length}

            # Set progress
            feedback.setProgress(int((current//count) * 100))

        # Edit with map
        self.arc_layer.dataProvider().changeAttributeValues(arc_attr_map)

        # Check for cancellation
        if feedback.isCanceled():
            return {}

    def compute_orientation(self, context, feedback):
        """
        Compute the orientation (clokcwise from North in degree)
        in a new attributes of the polygon layer. The arc are used to compute orientation
        as it is more accurate than polygon

        Parameters
        ----------
        self.arc_layer: QgisObject : QgsVectorLayer : LineString
            Skeleton of the polygons layer
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        Returns
        -------
        ioself.arc_layer : Arc layer with an orientation attributes
         """
        # Init progress bar steps
        count = self.arc_layer.featureCount()

        feedback.pushInfo("Computing and storing orientation field")

        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Prepare fields_list for creation
        fields_list = [("ht_orientation", QVariant.Double)]

        # Create new attributes if they already exist they will be overwriten
        at.create_fields(self.arc_layer, fields_list)

        # Get field index
        idx_ori = self.arc_layer.fields().indexFromName("ht_orientation")

        # Init attr map
        arc_attr_map = {}
        # Get main orientation
        for current, arc in enumerate(self.arc_layer.getFeatures()):
            # This method does 0 to 360 degree. Worth ?
            #start_pnt = arc.geometry().vertexAt(0)
            #end_pnt = arc.geometry.vertexAt(-1)
            #angle = start_pnt.azimuth(end_pnt)
            _, _, angle, _, _ = arc.geometry().orientedMinimumBoundingBox()
            arc_attr_map[arc.id()] = {idx_ori: round(angle, 2)}
            # Set progress
            feedback.setProgress(int((current // count) * 100))

        # Edit layer with attr map
        self.arc_layer.dataProvider().changeAttributeValues(arc_attr_map)

        # Check for cancellation
        if feedback.isCanceled():
            return {}

    def compute_shape_metrics(self, context, feedback):
        """
        Compute elongation, compacity and convexity of the polygon in three new attributes.

        Parameters
        ----------
        self.poly_layer: QgisObject : QgsVectorLayer : LineString
            Polygons layer
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        Returns
        -------
        ioself.poly_layer : Polygon layer with three shape attributes
        """
        # Init progress bar steps
        count = self.poly_layer.featureCount()

        feedback.pushInfo("Computing and storing shape metrics fields")

        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Prepare fields_list for creation
        fields_list = [("ht_elongation", QVariant.Double),
                       ("ht_compacity", QVariant.Double),
                       ("ht_convexity", QVariant.Double)]

        # Create new attributes if they already exist they will be overwritten
        at.create_fields(self.poly_layer, fields_list)

        # Get fields index
        idx_elong = self.poly_layer.fields().indexFromName("ht_elongation")
        idx_compac = self.poly_layer.fields().indexFromName("ht_compacity")
        idx_convex = self.poly_layer.fields().indexFromName("ht_convexity")

        # Init attr map
        poly_attr_map = {}
        # Compute metrics
        for current, poly in enumerate(self.poly_layer.getFeatures()):
            _, _, _, width, height = poly.geometry().orientedMinimumBoundingBox()
            area = poly.geometry().convexHull().area()

            elongation = round(width / height, 2)
            compactness = round(4 * np.pi * (poly.geometry().area() / poly.geometry().length() ** 2), 2)
            convexity = round(poly.geometry().area() / area, 2)

            poly_attr_map[poly.id()] = {idx_elong: elongation,
                                        idx_compac: compactness,
                                        idx_convex: convexity}
            # Set progress
            feedback.setProgress(int((current / count) * 100))

        # Edit layer with attr map
        self.poly_layer.dataProvider().changeAttributeValues(poly_attr_map)

        # Check for cancellation
        if feedback.isCanceled():
            return {}

    def compute_height_metrics_from_CHM(self, context, feedback, mnh):
        """
        Compute height metrics as new fields : mean, median, min, max, var

        Parameters
        ----------
        self.poly_layer: QgisObject : QgsVectorLayer : Polygon
            Polygons layer
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        mnh: QgisObject : QgsRasterLayer : Raster

        Returns
        -------
        outpoly_layer : QgisObject : QgsVectorLayer : Polygon
            Polygons layer with height metrics field.
        """
        # Init progress bar steps
        count = self.arc_layer.featureCount()

        feedback.pushInfo("Computing zonal statistics")

        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Prepare fields_list for deletion and creation
        fields_list = [("ht_h_mean", QVariant.Double),
                       ("ht_h_med", QVariant.Double),
                       ("ht_h_min", QVariant.Double),
                       ("ht_h_max", QVariant.Double),
                       ("ht_h_std", QVariant.Double)]

        # Delete fields if it already exists as it'll cause an error in zonal_stat
        at.delete_fields(self.poly_layer, [field[0] for field in fields_list])

        # Call zonal stat tool
        alg_name = "native:zonalstatisticsfb"
        params = {"INPUT": self.poly_layer,
                  "INPUT_RASTER": mnh,
                  "RASTER_BAND": 1,
                  "COLUMN_PREFIX": "ht_h_",
                  "STATISTICS": "2, 3, 5, 6, 4",
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        poly_layer_output = processing.run(alg_name, params)["OUTPUT"]

        # Set progress
        feedback.setProgress(50)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Creating height fields")
        # Create fields in self.poly_layer
        at.create_fields(self.poly_layer, fields_list)

        # Get field index from self.poly_layer
        idx_mean = self.poly_layer.fields().indexFromName("ht_h_mean")
        idx_med = self.poly_layer.fields().indexFromName("ht_h_med")
        idx_min = self.poly_layer.fields().indexFromName("ht_h_min")
        idx_max = self.poly_layer.fields().indexFromName("ht_h_max")
        idx_std = self.poly_layer.fields().indexFromName("ht_h_std")

        # Init attr map
        poly_attr_map = {}
        # Fill map of self.poly_layer with poly_layer_output fields
        for current, (poly1, poly2) in enumerate(zip(self.poly_layer.getFeatures(), poly_layer_output.getFeatures())):
            poly_attr_map[poly1.id()] = {idx_mean: round(
                                            poly2["ht_h_mean"], 2),
                                        idx_med: round(
                                            poly2["ht_h_median"], 2),
                                        idx_min: round(
                                            poly2["ht_h_min"], 2),
                                        idx_max: round(
                                            poly2["ht_h_max"], 2),
                                        idx_std: round(
                                            poly2["ht_h_stdev"], 2)}

            # Set progress
            feedback.setProgress(50 + int((current / count) * 50))
            # Check for cancellation
            if feedback.isCanceled():
                return {}

        # Edit layer with map
        self.poly_layer.dataProvider().changeAttributeValues(poly_attr_map)

        # Check for cancellation
        if feedback.isCanceled():
            return {}

    def strata_overlap_from_CHM(self, context, feedback, mnh, bins, idx_list,
                                spatial_output=False):
        """
        Compute the percentage of height bins for each hedge.

        Parameters
        ----------
        self.poly_layer : QgsVectorLayer : Polygon : Hedges polygons
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        mnh : QgsRasterLayer : MNH/MNC. Height raster.
        bins : list : List of values representing the height class to create.
        idx_list : list : Field index of indicators field.
        spatial_output : Boolean : If True output a new layer representing each catgeroy as a feature

        Returns
        -------
        outpoly_layer : QgisObject : QgsVectorLayer : Polygon
            Polygons layer with a field for each bin
            representing the percentage of this bin in the hedge.
        """
        total = self.poly_layer.featureCount()

        feedback.pushInfo(
            "Processing geomorphology, given raster resolution it could be slow")

        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Get path
        raster_path = mnh.dataProvider().dataSourceUri()
        vector_driver = self.poly_layer.dataProvider().storageType()
        # vector_path = self.poly_layer.dataProvider().dataSourceUri()

        if vector_driver == "GPKG":
            # Fetch correct layer in ogr
            vector_path, layer_idx = vutils.get_gpkg_path(self.poly_layer)
            # vector_path = vector_path.split("|")[0]

        # Output arg
        mem_driver = ogr.GetDriverByName("Memory")
        mem_driver_gdal = gdal.GetDriverByName("MEM")
        shp_name = "temp"

        # Open layer
        r_ds = gdal.Open(raster_path)
        p_ds = ogr.Open(vector_path)

        # Raster info
        geot = r_ds.GetGeoTransform()
        no_data = r_ds.GetRasterBand(1).GetNoDataValue()

        # Load vector layer
        lyr = p_ds.GetLayer(layer_idx)
        p_feat = lyr.GetNextFeature()
        current = 1

        # If spatial output
        if spatial_output:
            # Init output QgsVectorLayer
            q_layer = vutils.create_layer(self.poly_layer, multi=True)
            pr = q_layer.dataProvider()
            fields_list = [("ht_Id_strata", QVariant.Int),
                           ("pid", QVariant.Int),
                           ("ht_strata", QVariant.String),
                           ("ht_overlap", QVariant.Double)]
            at.create_fields(q_layer, fields_list)

            # Get extra raster information
            projection = r_ds.GetProjection()
            gdal_dtype = r_ds.GetRasterBand(1).DataType

        # Init attr map
        poly_attr_map = {}
        while p_feat:
            if p_feat.GetGeometryRef() is not None:
                tp_ds = mem_driver.CreateDataSource(shp_name)
                tp_lyr = tp_ds.CreateLayer("polygons", None, ogr.wkbPolygon)
                tp_lyr.CreateFeature(p_feat.Clone())
                offsets = rutils.bounding_box_to_offsets(
                    p_feat.GetGeometryRef().GetEnvelope(), geot)
                new_geot = rutils.geot_from_offsets(offsets[0], offsets[2], geot)

                tr_ds = mem_driver_gdal.Create("", offsets[3] - offsets[2],
                                               offsets[1] - offsets[0], 1,
                                               gdal.GDT_Byte)

                tr_ds.SetGeoTransform(new_geot)
                gdal.RasterizeLayer(tr_ds, [1], tp_lyr, burn_values=[1])
                tr_array = tr_ds.ReadAsArray()

                r_array = r_ds.GetRasterBand(1).ReadAsArray(offsets[2],
                                                            offsets[0],
                                                            offsets[3] -
                                                            offsets[2],
                                                            offsets[1] -
                                                            offsets[0])

                id = p_feat.GetFID()

                if r_array is not None:
                    # Create catgeory and unique id for each of them with D8
                    ind = np.digitize(r_array, bins)
                    mask_ar = np.ma.MaskedArray(ind,
                                                mask=np.logical_or(
                                                    r_array == no_data,
                                                    np.logical_not(
                                                        tr_array)))

                    if mask_ar is not None:
                        # Get overlap and object
                        unique, count = np.unique(mask_ar[~mask_ar.mask],
                                                  return_counts=True)
                        overlap = (count / count.sum()) * 100

                        fld_dict = {}
                        for idx, idx_f in enumerate(idx_list):
                            fld_dict[idx_f] = float(
                                round(overlap[unique == idx][0], 2)) \
                                if len(unique[unique == idx]) != 0 else 0
                            if self.poly_layer.fields().names()[
                                idx_f] == "ht_nb_strata":
                                fld_dict[idx_f] = len(
                                    [v for v in list(fld_dict.values())[0:idx]
                                     if v != 0])
                            elif self.poly_layer.fields().names()[
                                idx_f] == "ht_dom_strata":
                                fld_dict[idx_f] = \
                                [self.poly_layer.fields().names()[k]
                                 for k, v in fld_dict.items()
                                 if v == max(fld_dict.values())][0]
                        poly_attr_map[id] = fld_dict

                        # Check for cancellation
                        if feedback.isCanceled():
                            return {}

                        if spatial_output:
                            # Init multipoly output:
                            multipoly_d = {}
                            # Burn mask and expand to 3 dim to fit in gdal
                            out_ar = np.where(mask_ar.mask, -1,
                                              mask_ar.data + 1)  # Need to add +1 because the 0 class is not write as id when polygonizing
                            exp_ar = np.expand_dims(out_ar, axis=2)
                            # Get shape
                            nb_row, nb_col, nb_band = np.shape(exp_ar)

                            # Output initialisation
                            driver = gdal.GetDriverByName("MEM")
                            out_ds = driver.Create("", nb_col, nb_row, nb_band,
                                                   gdal_dtype)
                            out_ds.SetGeoTransform(new_geot)
                            out_ds.SetProjection(projection)

                            # Write array in raster
                            for idx_band in range(nb_band):
                                out_band = out_ds.GetRasterBand(idx_band + 1)
                                out_band.WriteArray(exp_ar[:, :, idx_band])

                                if no_data is not None:
                                    out_band.SetNoDataValue(no_data)
                                out_band.FlushCache()

                            # Get feature raster ds
                            src_band = out_ds.GetRasterBand(1)

                            # Create out layer and populate
                            dst_layername = "mnh_vector"
                            drv = ogr.GetDriverByName("MEMORY")
                            dst_ds = drv.CreateDataSource("")

                            sp_ref = osr.SpatialReference()
                            sp_ref.ImportFromWkt(projection)

                            dst_layer = dst_ds.CreateLayer(dst_layername,
                                                           srs=sp_ref)

                            fld = ogr.FieldDefn("id", ogr.OFTInteger)
                            dst_layer.CreateField(fld)
                            dst_field = dst_layer.GetLayerDefn().GetFieldIndex(
                                "id")

                            # Polygonize
                            gdal.Polygonize(src_band, src_band, dst_layer,
                                            dst_field,
                                            ["8CONNECTED=8"], callback=None)

                            # Check for cancellation
                            if feedback.isCanceled():
                                return {}

                            # Get geom
                            v_feat = dst_layer.GetNextFeature()
                            while v_feat:
                                geom_as_wkt = v_feat.GetGeometryRef().ExportToWkt()
                                # Store in QgsVectorLayer
                                q_feat = QgsFeature()
                                q_geom = QgsGeometry().fromWkt(geom_as_wkt)
                                # Write if first occurence or store if already
                                # a poly with same id_strata in q_layer
                                if v_feat["id"] in multipoly_d:
                                    multipoly_d[v_feat["id"]].append(q_geom)
                                else:
                                    multipoly_d[v_feat["id"]] = []
                                    q_feat.setGeometry(q_geom)
                                    # Get alt_class
                                    sup = unique[unique == v_feat["id"] - 1][
                                        0]  # - 1 to retrieve real class because we added +1 to id
                                    str_class = "[%(1)s/%(2)s[" \
                                                % {"1": bins[sup - 1] if
                                    bins[sup] != bins[0] else "0.0",
                                                   "2": bins[sup] if
                                                   bins[sup] != bins[
                                                       -1] else "+"}

                                    q_feat.setAttributes(
                                        [v_feat["id"], id, str_class,
                                         round(float(overlap[unique == v_feat[
                                             "id"] - 1]),
                                               2)])  # - 1 to retrieve real class because we added +1 to id
                                    pr.addFeature(q_feat)
                                    q_layer.updateExtents()

                                    # Check for cancellation
                                    if feedback.isCanceled():
                                        return {}

                                v_feat = dst_layer.GetNextFeature()

                        if spatial_output:
                            out_ds = None
                            dst_ds = None
                            dst_layer = None

                            # Init geom map
                            q_geom_map = {}

                            # Once we have finished to iterate over v_feat.
                            # Check if there is a need to add part to polygons to create multipoly
                            req = QgsFeatureRequest().setFilterExpression(
                                "pid = %d" % id)
                            for feat in q_layer.getFeatures(req):
                                geom = feat.geometry()
                                if len(multipoly_d[feat["ht_Id_strata"]]) == 0:
                                    pass
                                else:
                                    for g in multipoly_d[feat["ht_Id_strata"]]:
                                        geom.addPartGeometry(g)
                                    # Add new geom to geom map
                                    q_geom_map[feat.id()] = geom
                            # Edit q_layer geometries
                            q_layer.dataProvider().changeGeometryValues(
                                q_geom_map)

            p_feat = lyr.GetNextFeature()
            tp_ds = None
            tp_lyr = None
            tr_ds = None
            # Set progress
            feedback.setProgress(int((current / total) * 100))
            current += 1

        # Edit poly_layer with attr map
        self.poly_layer.dataProvider().changeAttributeValues(poly_attr_map)

        # Check for cancellation
        if feedback.isCanceled():
            return {}

        if spatial_output:
            return q_layer

    def interface_type(self, context, feedback, layer_dict):
        """
        Retrieve intersecting interface (crop field, river, road, railway, building)
        and store their id in fields inside the polygon layer

        Parameters
        ----------
        self.poly_layer : QgisObject : QgsVectorLayer : Polygon.
            Polygon layer
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        layer_dict : Dict : Contains interface type as key
                            and corresponding layer aswell as his unique id in dict values

        Returns
        -------
        self.poly_layer : QgisObject : QgsVectorLayer : Polygon.
            Polygon layer with new fields corresponding to intersecting features
        """
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Init progress bar steps
        count = self.poly_layer.featureCount() * len(layer_dict)

        # Deleting existing ht_Adj_* fields if corresponding layers is informed by user
        del_fields_list = [f for f in self.poly_layer.fields().names()
                           if f[:-2] in ["ht_Adj_%s"%k for k in layer_dict.keys()]]
        at.delete_fields(self.poly_layer, del_fields_list)

        # Init fields related variable and result dictionnary
        max_idx = max(self.poly_layer.attributeList())
        fields_list = []
        poly_attr_map = {}

        feedback.pushInfo("Retrieving number of attributes to create")

        # Retrieve maximum intersection for each layer
        current = 0
        for key, (layer, _) in layer_dict.items():
            max_inter = 0
            for poly in self.poly_layer.getFeatures():
                i_count, _ = g.get_clementini(layer, poly.geometry())
                max_inter = i_count if max_inter < i_count else max_inter

                # Set progress
                current += 1
                feedback.setProgress(int((current / count) * 50))
                # Check for cancellation
                if feedback.isCanceled():
                    return {}

            layer_dict[key].append(max_inter)

        feedback.pushInfo("Retrieving unique id of intersecting interface")
        # Retrieve interface for each hedges
        current = 0
        for key, (layer, id_field, max_inter) in layer_dict.items():
            for poly in self.poly_layer.getFeatures():
                idx = max_idx + 1
                i_count, i_list = g.get_clementini(layer, poly.geometry())
                while i_count < max_inter:
                    i_list.append(NULL)
                    i_count += 1
                for feat in i_list:
                    if poly.id() not in poly_attr_map:
                        poly_attr_map[poly.id()] = {idx: feat[id_field] # Id_field
                                                    if feat is not NULL else feat}
                    else:
                        poly_attr_map[poly.id()] = {**poly_attr_map[poly.id()],
                                                    **{idx: feat[id_field]
                                                    if feat is not NULL else feat}}
                    idx += 1

                current += 1
                # Set progress
                feedback.setProgress(50 + int((current / count) * 45))
                # Check for cancellation
                if feedback.isCanceled():
                    return {}

            max_idx += max_inter

            # Format output fields
            fields_list += [("ht_Adj_%s_%d" %(key, n), QVariant.String) for n in
                            range(1, max_inter + 1)]

        feedback.pushInfo("Formating output")

        # Create fields and fill them
        at.create_fields(self.poly_layer, fields_list)
        self.poly_layer.dataProvider().changeAttributeValues(poly_attr_map)

        # Set progress
        feedback.setProgress(100)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

    def shortest_distance(self, context, feedback, forest_layer, forest_id_field, distance):
        """
        Compute the shortest distance on the fly between a hedge and a forest
        The distance is then stored in arc layer attribute table with the forest id.

        Parameters
        ----------
        self.arc_layer: QgisObejct : QgsVectorLayer : LineString
            Arc layer
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        forest_layer :  QgisObejct : QgsVectorLayer : Polygon or Point
        forest_id_field : Integer : Idx of the field to use as unique identifier
        distance : Integer : Distance of the buffer to connext forest to hedge extremities

        Returns
        -------
        self.arc_layer: QgisObejct : QgsVectorLayer : LineString
            Updated arc layer with the shortest distance to forest field.
        """
        # Init progress bar variable
        alg_number = 4
        step_per_alg = int(100 / alg_number)
        count = self.node_layer.featureCount()

        feedback.pushInfo("Creating forest hubs")

        forest_connection_point = g.create_forest_connection(self.poly_layer,
                                                              self.arc_layer,
                                                              self.node_layer,
                                                              forest_layer,
                                                              forest_id_field,
                                                              distance)
        #
        # # Create regular point on forest boundary
        # alg_name = "native:pointsalonglines"
        # params = {"INPUT": forest_layer,
        #           "DISTANCE": 10,
        #           "START_OFFSET": 0,
        #           "END_OFFSET": 0,
        #           "OUTPUT": "TEMPORARY_OUTPUT"}
        # pnt_layer = processing.run(alg_name, params)["OUTPUT"]
        #
        # # Set progress
        # feedback.setProgress(step_per_alg)
        # # Check for cancellation
        # if feedback.isCanceled():
        #     return {}

        feedback.pushInfo("Computing nearest neghbor analysis for hedge extremities")

        # Compute nearest distance between hedge nodes and forest point boundary
        alg_name = "qgis:distancetonearesthubpoints"
        params = {"INPUT": self.node_layer,
                  "HUBS": forest_connection_point,
                  "FIELD": forest_id_field,
                  "UNIT": 0, # Meters
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        dist_layer = processing.run(alg_name, params)["OUTPUT"]

        # Set progress
        feedback.setProgress(step_per_alg * 2)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Init output dict and output fields
        node_attr_map = {}

        # Retrieve field type of forest_id
        field_type = forest_layer.fields().at(
            forest_layer.fields().indexFromName(forest_id_field)).typeName()
        if field_type == "String":
            variant = QVariant.String
        elif field_type == "Real":
            variant = QVariant.Double
        else:
            variant = QVariant.Int

        at.create_fields(self.node_layer, [("ht_forest_id_fly", variant),
                                           ("ht_dist_fly", QVariant.Double)])
        # Get output fields index
        idx_hub = self.node_layer.fields().indexFromName("ht_forest_id_fly")
        idx_dist = self.node_layer.fields().indexFromName("ht_dist_fly")

        feedback.pushInfo("Writing results")

        for current, feat in enumerate(self.node_layer.getFeatures()):
            req = QgsFeatureRequest().setFilterExpression(
                                        "vid = %d" %feat["vid"])\
                                       .setFlags(QgsFeatureRequest.NoGeometry)\
                                       .setSubsetOfAttributes(
                                        ["HubName", "HubDist"],
                                        dist_layer.fields())

            node = next(dist_layer.getFeatures(req))

            # Fill node_attr_map
            node_attr_map[feat.id()] = {idx_hub: node["HubName"],
                                        idx_dist: round(node["HubDist"], 2)}

            # Set progress
            feedback.setProgress((step_per_alg * 2) + int((current / count)))
            # Check for cancellation
            if feedback.isCanceled():
                return {}

        # Update arc_layer
        self.node_layer.dataProvider().changeAttributeValues(node_attr_map)

    def topographic_position(self, context, feedback, mnt, search=100, skip=50, kernel=3):
        """
        Compute terrain morphology and determine hedges topographic position based on grass geomorphon
        Default values are determined for a 5m resolution dem

        Parameters
        ----------
        self.poly_layer : QgsVectorLayer : Polygon : Hedges polygons
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        mnt : QgsRasterLayer : MNT/MNE. Should be bigger than poly_layer extent
            in case their is a skip value
        search : int : Radius in meters to search for terrain form.
            Higher value will result to larger terrain form
        skip : int : Radius in meters to skip when searching for terrain form.
            Allow to ignore small form
        kernel : int : Radius of the majority filter kernel (must be odd)

        Returns
        -------
        outpoly_layer : QgisObject : QgsVectorLayer : Polygon
            Polygons layer with topographic position field.
        """
        alg_number = 5
        step_per_alg = int(100 / alg_number)

        # Get extent of polygon layer
        extent = self.poly_layer.sourceExtent()

        # Clip MNE to poly_layer extent + skip
        if extent.area() < mnt.extent().area():
            alg_name = "gdal:cliprasterbyextent"
            params = {"INPUT": mnt,
                      "PROJWIN": extent.buffered(skip + mnt.rasterUnitsPerPixelX() * 2),
                      # Skip as it'll reduce the raster size in geomorphon output if skip != 0.
                      # rasterunit * 2 as geomorphon output is reduced by that even with skip = 0.
                      "OUTPUT": "TEMPORARY_OUTPUT"}
            output = processing.run(alg_name, params)["OUTPUT"]
            mnt = QgsRasterLayer(output)

        # Set progress
        feedback.setProgress(step_per_alg)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Processing geomorphology, given raster resolution it could be slow")
        # Geomorphon
        alg_name = "grass7:r.geomorphon"
        params = {"elevation": mnt,
                  "search": search,
                  "skip": skip,
                  "-m": True, # meters instead of cells for unit
                  "forms": "TEMPORARY_OUTPUT"}
        output = processing.run(alg_name, params)["forms"]
        geomorphon = QgsRasterLayer(output)

        # Set progress
        feedback.setProgress(step_per_alg * 2)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Applying majority filter")

        # Apply majority filter
        alg_name = "grass7:r.neighbors"
        params = {"input": geomorphon,
                  "method": 2,
                  "size": kernel,
                  "output": "TEMPORARY_OUTPUT"}
        output = processing.run(alg_name, params)["output"]
        geomorphon = QgsRasterLayer(output)

        # Set progress
        feedback.setProgress(step_per_alg * 3)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Computing zonal statistics")

        # Extract to poly_layer
        alg_name = "native:zonalstatisticsfb"
        params = {"INPUT": self.poly_layer,
                  "INPUT_RASTER": geomorphon,
                  "RASTER_BAND": 1,
                  "COLUMN_PREFIX": "ht_",
                  "STATISTICS": "9",
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        poly_layer_output = processing.run(alg_name, params)["OUTPUT"]

        # Set progress
        feedback.setProgress(step_per_alg * 4)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Creating topographic position field")

        # Prepare fields_list for deletion
        fields_list = [("ht_topo_pos", QVariant.String)]

        # Delete ht_topo_pos if it exists
        at.create_fields(self.poly_layer, fields_list)

        # Get field index
        idx_topo = self.poly_layer.fields().indexFromName("ht_topo_pos")

        # Init attr map
        poly_attr_map = {}

        # Fill map with result
        for poly1, poly2 in zip(self.poly_layer.getFeatures(), poly_layer_output.getFeatures()):
            poly_attr_map[poly1.id()] = {idx_topo: rutils.map_morph(
                                                  int(poly2["ht_majority"]))}

        # Edit layer with map
        self.poly_layer.dataProvider().changeAttributeValues(poly_attr_map)

        # Set progress
        feedback.setProgress(step_per_alg * 5)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

    def relative_orientation(self, context, feedback, mnt,
                             kernel_radius, buffer_size, bound, min_slope):
        """
        Extract the relative orientation of the hedge inside the main slope.
        Hedge can either be perpendicular, parallel or diagonal to the main slope.
        If there is no slope the algorithm return  the no slope

        Parameters
        ----------
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        mnt: QgsRasterLayer : MNT/DEM. Elevation raster.
        kernel_radius: int : Radius of the kernel (odd)
        buffer_size: int: Buffer size
        bound: int: Angle bounds
        min_slope:int: Minimimum slope to determine if there is a slope.

        Returns
        -------
        outpoly_layer : QgisObject ; QgsVectorlayer ; Polygon
            Polygons layer with a relative orientation field.

        TODO:
        Tester si GRASS → fill river comble bien les cours d'eau
        Sinon considérons que si haie proche cours d’eau → plat
        Sinon tester prendre haie en bordure cours d’eau → one side buffer opposé au cours d'eau
        """
        alg_number = 9
        step_per_alg = int(100 / alg_number)
        count = self.poly_layer.featureCount()

        temp_path = tempfile.gettempdir() + "/mnt_int.tif"

        # Creating output field and initializing output variable
        fields = [("ht_slope_pos", QVariant.String)]
        at.create_fields(self.poly_layer, fields)

        idx_pos = self.poly_layer.fields().indexFromName("ht_slope_pos")
        poly_attr_map = {}

        # # Opening raster in gdal
        # ds = gdal.Open(mnt.dataProvider().dataSourceUri())
        # array = ds.GetRasterBand(int(band)).ReadAsArray()
        #
        # # Transform mnt to int (usefull if we go for majority zonal stats)
        # output = np.round(array)
        # ht.write_image(temp_path, output, ds, memory=False)
        # mnt = QgsRasterLayer(temp_path, "mnt_int")

        feedback.pushInfo("Applying median filter to the DEM")

        # Applying median filter
        alg_name = "grass7:r.neighbors"
        params = {"input": mnt,
                  "method": 1,  # median
                  "size": kernel_radius,  # odd
                  "output": "TEMPORARY_OUTPUT"}
        mnt = processing.run(alg_name, params)["output"]
        mnt = QgsRasterLayer(mnt, "mnt_med")

        # Set progress
        feedback.setProgress(step_per_alg * 2)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Creating exposition and slope raster")

        # Compute exposition
        alg_name = "native:aspect"
        params = {"INPUT": mnt,
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        expo = processing.run(alg_name, params)["OUTPUT"]
        
        # Set progress
        feedback.setProgress(step_per_alg * 3)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Compute slope with gdal to have slope in % (5.7° = 10%, 26.6°= = 50%)
        alg_name = "gdal:slope"
        params = {"INPUT": mnt,
                  "BAND": 1,
                  "AS_PERCENT": True,
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        slope = processing.run(alg_name, params)["OUTPUT"]

        # Set progress
        feedback.setProgress(step_per_alg * 4)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Compute hedge orientation if not already computed
        if "ht_orientation" not in self.arc_layer.fields().names():
            feedback.pushInfo("Computing hedge orientation")
            alg_name = "hedgetools:computeori"
            params = {"INPUT_POLY": self.poly_layer,
                      "INPUT_ARC": self.arc_layer,
                      "INPUT_NODE": self.node_layer}
            processing.run(alg_name, params)

        feedback.pushInfo("Buffering hedges")
        # Buffer
        alg_name = "native:buffer"
        params = {"INPUT": self.poly_layer,
                  "DISTANCE": buffer_size,
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        buffer = processing.run(alg_name, params)["OUTPUT"]
        # Set progress
        feedback.setProgress(step_per_alg * 5)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        # Substract hedge to buffer to remove embankment and river aspect/slope
        if buffer.hasSpatialIndex() != 2:
            buffer.dataProvider().createSpatialIndex()

        alg_name = "native:difference"
        params = {"INPUT": buffer,
                  "OVERLAY": self.poly_layer,
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        buffer = processing.run(alg_name, params)["OUTPUT"]

        # Set progress
        feedback.setProgress(step_per_alg * 5)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Zonal statistics to retrieve main exposition and slope around hedges")
        # Zonal stat
        alg_name = "native:zonalstatisticsfb"
        params = {"INPUT": buffer,
                  "INPUT_RASTER": expo,
                  "RASTER_BAND": 1,
                  "COLUMN_PREFIX": "ht_",
                  "STATISTICS": "3",  # Median
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        zonal_expo = processing.run(alg_name, params)["OUTPUT"]
        # Set progress
        feedback.setProgress(step_per_alg * 7)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        alg_name = "native:zonalstatisticsfb"
        params = {"INPUT": buffer,
                  "INPUT_RASTER": slope,
                  "RASTER_BAND": 1,
                  "COLUMN_PREFIX": "ht_",
                  "STATISTICS": "2",  # Mean
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        zonal_slope = processing.run(alg_name, params)["OUTPUT"]

        # Set progress
        feedback.setProgress(step_per_alg * 8)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Computing relative orientation")

        # Make table join
        fk_field = ["pid", "pid", "pid"]
        join_layer = [zonal_expo, zonal_slope, self.arc_layer]
        join_field = [["ht_median"], ["ht_mean"], ["ht_orientation"]]
        at.table_join(self.poly_layer, "pid", join_layer, fk_field, join_field)

        # Computing relative orientation
        for current, poly in enumerate(self.poly_layer.getFeatures()):

            # Halving slope orientation to match hedge orientation and reduce conditionnal argument
            slope_ori = poly["ht_median"] if poly["ht_median"] <= 180 \
                else poly["ht_median"] - 180
            if poly["ht_mean"] < min_slope:
                poly_attr_map[poly.id()] = {idx_pos: "No slope"}
            elif abs(slope_ori - poly["ht_orientation"]) <= 0 + bound or abs(
                    slope_ori - poly["ht_orientation"]) >= 180 - bound:
                poly_attr_map[poly.id()] = {idx_pos: "Parallel"}
            elif 90 - bound < abs(
                    slope_ori - poly["ht_orientation"]) < 90 + bound:
                poly_attr_map[poly.id()] = {idx_pos: "Perpendicular"}
            else:
                poly_attr_map[poly.id()] = {idx_pos: "Diagonal"}

            # Set progress
            feedback.setProgress(89 + int((current / count) * 11))
            # Check for cancellation
            if feedback.isCanceled():
                return {}

        # Updating output field and temp file deletion
        self.poly_layer.dataProvider().changeAttributeValues(poly_attr_map)

        # Removing table join
        self.poly_layer.removeJoin(zonal_expo.id())
        self.poly_layer.removeJoin(zonal_slope.id())
        self.poly_layer.removeJoin(self.arc_layer.id())

    def embankment_ditch(self, context, feedback, dem, band,
                        search_distance, overlap_thresh):
        """
        Compute embankment and ditch in a dem and check if they are overlapping with an hedge.
        If this is the case return True in two new boolean fields in the polygon layer data provider.

        Parameters
        ----------
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        dem: QgsRasterLayer : MNT/DEM. Elevation raster.
        band : int : gdal band number of the DEM.
        search_distance: int : search distance in meters
        overlap_thresh: int: Threshold to determine if an hedge have an embankment or a ditch

        Returns
        -------
        outpoly_layer : QgisObject ; QgsVectorlayer ; Polygon
            Polygons layer with a relative orientation field.
        """
        alg_number = 5
        step_per_alg = int(100 / alg_number)

        at.create_fields(self.poly_layer, [("ht_embankment", QVariant.Bool),
                                      ("ht_ditch", QVariant.Bool)])
        idx_embank = self.poly_layer.fields().indexFromName("ht_embankment")
        idx_ditch = self.poly_layer.fields().indexFromName("ht_ditch")

        feedback.pushInfo("Computing terrain forms")

        alg_name = "grass7:r.geomorphon"
        params = {"elevation": dem,
                  "search": search_distance,
                  "skip": 0,
                  "-m": True,  # meters instead of cells for unit
                  "forms": "TEMPORARY_OUTPUT"}
        output = processing.run(alg_name, params)["forms"]
        geomorphon = QgsRasterLayer(output)

        # Set progress
        feedback.setProgress(step_per_alg)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Retrieving embankment and ditch")

        alg_name = "gdal:rastercalculator"
        params = {"INPUT_A": geomorphon,
                  "BAND_A": 1,
                  "FORMULA": "where(logical_and(A !=3, A != 9), 0, A)",
                  "NO_DATA": 0,
                  "RTYPE": 0,
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        output = processing.run(alg_name, params)["OUTPUT"]
        geo_binary = QgsRasterLayer(output)

        # Set progress
        feedback.setProgress(step_per_alg * 2)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Retrieving number of cells of embankment and ditch inside the hedge")

        alg_name = "native:zonalhistogram"
        params = {"INPUT_RASTER": geo_binary,
                  "RASTER_BAND": 1,
                  "INPUT_VECTOR": self.poly_layer,
                  "COLUMN_PREFIX": "ht_",
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        histo_layer = processing.run(alg_name, params)["OUTPUT"]

        # Set progress
        feedback.setProgress(step_per_alg * 3)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Computing overlap percentage")

        request = QgsFeatureRequest().setFlags(
            QgsFeatureRequest.NoGeometry).setSubsetOfAttributes(
            ["fid", "ht_NODATA", "ht_3", "ht_9"], histo_layer.fields())

        feature_dict = {poly["fid"]:
                            [poly["ht_3"], poly["ht_9"],
                             poly["ht_3"] + poly["ht_9"] + poly["ht_NODATA"]]
                        for poly in histo_layer.getFeatures(request)}

        # Set progress
        feedback.setProgress(step_per_alg * 4)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        poly_attr_map = {k: {idx_embank: (ht_3 / ht_sum) * 100 > overlap_thresh,
                             idx_ditch: (ht_9 / ht_sum) * 100 > overlap_thresh} for
                         k, (ht_3, ht_9, ht_sum) in feature_dict.items()}

        feedback.pushInfo("Writing results")

        self.poly_layer.dataProvider().changeAttributeValues(poly_attr_map)

        # Set progress
        feedback.setProgress(step_per_alg * 5)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

    def forest_connection(self, context, feedback, forest_layer, forest_id,
                         distance):
        """
        Return if a hedge is connected to a forest or not.

        Parameters
        ----------
        self.arc_layer: QgisObejct : QgsVectorLayer : LineString
            Arc layer
        context : QgsContext : Context in which the tools are called
        feedback : QgsFeedback : Allow communication with the users (message, progress bar, ...)
        forest_layer :  QgisObejct : QgsVectorLayer : Polygon or Point
        forest_id : QgsField : Field name of the forest unique identifier
        distance : Integer : Distance to connect forest to network.

        Returns
        -------
        self.arc_layer: QgisObejct : QgsVectorLayer : LineString
            Updated arc layer with a boolean field.
                True if connected to a forest
                False if not connected to a forest
        """
        # Init progress bar steps
        alg_number = 5
        step_per_alg = int(100 / alg_number)

        feedback.pushInfo("Creating network")

        forest_connection_point = g.create_forest_connection(self.poly_layer,
                                                             self.arc_layer,
                                                             self.node_layer,
                                                             forest_layer,
                                                             forest_id,
                                                             distance)
        # Create empty node layer
        # forest_connection_point = ht.create_layer(self.node_layer)
        # ht.create_fields(forest_connection_point, [("vid", QVariant.Int),
        #                                           (forest_id, QVariant.String)])

        # feedback.pushInfo("Creating center point - Forest")

        # alg_name = "native:pointonsurface"
        # params = {"INPUT": forest_layer,
        #           "ALL_PARTS": True,
        #           "OUTPUT": "TEMPORARY_OUTPUT"}
        # forest_centroid = processing.run(alg_name, params)["OUTPUT"]
        #
        # # Set progress
        # feedback.setProgress(step_per_alg)
        # # Check for cancellation
        # if feedback.isCanceled():
        #     return {}
        #
        # feedback.pushInfo("Include forest center to hedge network")
        # node_map = []
        # for poly in self.poly_layer.getFeatures():
        #     # Get hedge directly connected to a forest
        #     iC, iF = ht.get_clementini(forest_layer, poly.geometry().buffer(20, 5))  # dist, segment
        #     for f in iF:
        #         # Retrieve corresponding centroid
        #         exp = "{} = '{}'" .format(forest_id, f[forest_id])
        #         req = QgsFeatureRequest().setFilterExpression(exp)
        #         centroid = next(forest_centroid.getFeatures(req))
        #
        #         # Retrieve arc and determine which end is closer to centroid
        #         _, fid_arc, _ = self.get_fid_from_relation("pid",
        #                                                    poly["pid"])
        #         arc = self.arc_layer.getFeature(fid_arc)
        #         # exp = "pid = %s" % poly["pid"]
        #         # req = QgsFeatureRequest().setFilterExpression(exp)
        #         # arc = next(self.arc_layer.getFeatures(req))
        #
        #         d1 = QgsGeometry(
        #                 QgsPoint(arc.geometry().asPolyline()[0])).distance(
        #                     centroid.geometry())
        #         d2 = QgsGeometry(
        #                 QgsPoint(arc.geometry().asPolyline()[-1])).distance(
        #                     centroid.geometry())
        #
        #         # Retrieve vid
        #         _, iF = ht.get_clementini(self.node_layer, QgsGeometry(
        #             QgsPoint(arc.geometry().asPolyline()[0])).buffer(0.1, 5))
        #
        #         # Create new forest connexion feature from this node
        #         node = iF[0]
        #         new_feat = QgsFeature()
        #         new_feat.setAttributes([node["vid"], centroid["ID"]])
        #
        #         if d1 < d2:
        #             new_feat.setGeometry(
        #                 QgsGeometry(QgsPoint(arc.geometry().asPolyline()[0])))
        #         else:
        #             new_feat.setGeometry(
        #                 QgsGeometry(QgsPoint(arc.geometry().asPolyline()[-1])))
        #
        #         node_map.append(new_feat)
        #
        # forest_connection_point.dataProvider().addFeatures(node_map)

        # Set progress
        feedback.setProgress(step_per_alg * 3)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Network analysis")

        # v.net.distance
        network_path = tempfile.gettempdir() + "/network_temp.gpkg"

        alg_name = "grass7:v.net.distance"
        params = {"input": self.arc_layer,
                  "flayer": self.node_layer,
                  "tlayer": forest_connection_point,
                  "threshold": distance,
                  "output": network_path}
        network_path = processing.run(alg_name, params)["output"]
        network_layer = QgsVectorLayer(network_path, "network_temp")

        # Set progress
        feedback.setProgress(step_per_alg * 4)
        # Check for cancellation
        if feedback.isCanceled():
            return {}

        feedback.pushInfo("Writing results")

        # Retrieve field type of forest_id
        field_type = forest_layer.fields().at(
            forest_layer.fields().indexFromName(forest_id)).typeName()
        if field_type == "String":
            variant = QVariant.String
        elif field_type == "Real":
            variant = QVariant.Double
        else:
            variant = QVariant.Int

        # Create output fields and get their index
        at.create_fields(self.poly_layer,
                         [("ht_forest_connect", QVariant.Bool)])
        at.create_fields(self.node_layer, [("ht_forest_dist", QVariant.Double),
                                           ("ht_forest_id", variant)])

        idx_forest_co = self.poly_layer.fields().indexFromName(
            "ht_forest_connect")
        idx_forest_dist = self.node_layer.fields().indexFromName(
            "ht_forest_dist")
        idx_forest_id = self.node_layer.fields().indexFromName("ht_forest_id")

        # Init results buffer
        poly_attr_map = {}
        node_attr_map = {}

        # Add poly that have node connected to a forest to results
        for feat in network_layer.getFeatures():
            _, iF = g.get_clementini(self.poly_layer,
                                      feat.geometry().buffer(0.1, 5))
            # Get forest_id
            req = QgsFeatureRequest().setFilterFid(feat["tcat"])
            curr_id = next(forest_connection_point.getFeatures(req))[forest_id]

            for f in iF:
                if f.id() not in poly_attr_map:
                    poly_attr_map[f.id()] = {idx_forest_co: True}
                if feat["cat"] not in node_attr_map:
                    node_attr_map[feat["cat"]] = {
                        idx_forest_dist: round(feat["dist"], 2),
                        idx_forest_id: curr_id}

        # Add poly that have a direct forest connexion to results
        # We can enhance speed by excluding a list of curr_id in expression
        for node in forest_connection_point.getFeatures():
            _, iF = g.get_clementini(self.poly_layer,
                                      node.geometry().buffer(0.1, 5))
            exp = "vid = %d" % node["vid"]
            req = QgsFeatureRequest().setFilterExpression(exp)
            fid_node = next(self.node_layer.getFeatures(req)).id()
            for f in iF:
                poly_attr_map[f.id()] = {idx_forest_co: True}
                node_attr_map[fid_node] = {idx_forest_dist: round(0, 2),
                                           idx_forest_id: node[forest_id]}

        # Add results in layers
        self.poly_layer.dataProvider().changeAttributeValues(poly_attr_map)
        self.node_layer.dataProvider().changeAttributeValues(node_attr_map)

        # Remove v.net.distance layer
        del (network_layer)
        os.remove(network_path)

        # Set progress
        feedback.setProgress(step_per_alg * 5)
        # Check for cancellation
        if feedback.isCanceled():
            return {}
