"""
Utility functions to handle raster data.
"""
import os
from typing import Tuple

# import affine
# import geopandas as gpd
import numpy as np
import processing
# import rasterio
from osgeo import gdal
from qgis.core import QgsRasterLayer
# from rasterio.mask import mask
# from rasterio.merge import merge


def qgis_to_numpy_dtype(indtype):
    """
    Take a Qgis.DataType object and return corresponding numpy dtype.
    Note : Unknown dtype (0) and specific dtype (10 to 13) are not handled yet.

    Parameters
    ----------
    indtype : Qgs.DataType :
        Object describing data type.

    Returns
    -------
    outdtype : np.dtype :
        dtype in the numpy format.
    """
    # Init map
    map_dict = {1: np.int8,
                2: np.uint16,
                3: np.int16,
                4: np.uint32,
                5: np.int32,
                6: np.float32,
                7: np.float64,
                8: np.intc,
                9: np.intc}

    return map_dict[indtype]


def map_morph(n):
    """
    Take an int value and return a string indicating the terrain type

    Parameters
    ----------
    n: int or list :
        Class of r.geomorphon.

    Returns
    -------
    terrain_type : string :
        Topographic position in a drainage basin.
    """
    map_dict = {-1: "No_data",
                1: "Flat",
                2: "Peak",
                3: "Ridge",
                4: "Shoulder",
                5: "Spur",
                6: "Slope",
                7: "Hollow",
                8: "Footslope",
                9: "Valley",
                10: "Pit",
                255: "No_data"}

    return map_dict[n]


def bounding_box_to_offsets(bbox, geot):
    """
    Given a gdal geotransform object,
    compute the position in the input raster of the feature.

    Parameters
    ----------
    bbox : tuple :
        Coordinates tuple of a osgeo.ogr.Geometry.
    geot : tuple :
        Geotransform (resolution and top left corner coordinate)
        of a osgeo.gdal.Dataset.

    Returns
    -------
    row1, row2, col1, col2 : list(int) :
        Relative position of the vector feature in the raster.
    """
    col1 = int((bbox[0] - geot[0]) / geot[1])
    col2 = int((bbox[1] - geot[0]) / geot[1]) + 1
    row1 = int((bbox[3] - geot[3]) / geot[5])
    row2 = int((bbox[2] - geot[3]) / geot[5]) + 1
    return [row1, row2, col1, col2]


def geot_from_offsets(row_offset, col_offset, geot):
    """
    Compute a geotransform (resolution and top left corner coordinates)
    from a raster, a geotransform and an object relative position in a raster.

    Parameters
    ----------
    row_offset : int :
        Relative position of the top left of the bounding box object
        in the raster rows.
    col_offset : int :
        Relative position of the top left of the bounding box object
        in the raster cols.
    geot : tuple :
        Geotransform (resolution and top left corner coordinate)
        of a osgeo.gdal.Dataset (input raster).

    Returns
    -------
    new_geot : tuple :
        Geotransform representing the position (coordinates) of the object.
    """
    new_geot = [geot[0] + (col_offset * geot[1]),
                geot[1],
                0.0,
                geot[3] + (row_offset * geot[5]),
                0.0,
                geot[5]]
    return new_geot


def write_image(out_filename, array, data_set=None, gdal_dtype=None,
                no_data=None, memory=False):
    """
    Write numpy array as GeoTiff or in memory.

    Parameters
    ----------
    out_filename : str
        Path of the output raster.
        If in memory is True it will be ignored or you can use and empty string.
    array : numpy.ndarray
        Array to write.
    data_set : osgeo.gdal.Dataset
        Gdal object used to get dtype, driver, projection.
        (Default value = None)
    gdal_dtype : int
        Gdal data type, if unregistered it will take the dataset data type.
        (Default value = None)
    no_data : int
        No data value. (Default value = None)
    memory ; boolean
        If true will write in a memory raster.
        (Default value = False)

    Returns
    -------
    None.

    """

    # Dimensions de l'image
    nb_col = array.shape[1]
    nb_ligne = array.shape[0]
    array = np.atleast_3d(array)
    nb_band = array.shape[2]

    # Informations à récupérer sur le data_set
    transform = data_set.GetGeoTransform()
    projection = data_set.GetProjection()
    gdal_dtype = gdal_dtype if gdal_dtype is not None \
        else data_set.GetRasterBand(1).DataType
    driver_name = data_set.GetDriver().ShortName if memory is False else "MEM"
    out_filename = out_filename if memory is False else ""
    # Initialisation de l'image en sortie
    driver = gdal.GetDriverByName(driver_name)
    output_data_set = driver.Create(out_filename, nb_col, nb_ligne, nb_band,
                                    gdal_dtype)
    output_data_set.SetGeoTransform(transform)
    output_data_set.SetProjection(projection)

    # Remplissage et écriture de l'image
    for idx_band in range(nb_band):
        output_band = output_data_set.GetRasterBand(idx_band + 1)
        output_band.WriteArray(array[:, :, idx_band])

        if no_data is not None:
            output_band.SetNoDataValue(no_data)
        output_band.FlushCache()

    # Fermeture des données en entrée
    del(output_band)
    del(output_data_set)


def raster_as_array(inraster):
    """
    Take a QgsRasterLayer and return a numpy ndarray containing the value.
    Note : QgsRasterCalculator have better performance for doing band operation.
    Array is for specific operation or visualisation.

    Parameters
    ----------
    inraster: QgisObject : QgsRasterLayer :
        Raster layer.

    Returns
    -------
    outarray : numpy.ndarray :
        Numpy array of the input raster.
    """
    # Get raster metadata as converting to array remove all shape information
    row = inraster.height()
    col = inraster.width()
    extent = inraster.extent()
    band_number = inraster.bandCount()
    qgis_dtype = inraster.dataProvider().block(1, extent, col, row).dataType()
    np_dtype = qgis_to_numpy_dtype(qgis_dtype)

    # Create empty output array
    outarray = np.empty(shape=(row, col, band_number), dtype=np_dtype)

    # We got raster shape. We want to get the data, but it's serialized.
    # First go get the bytes
    for band in range(0, band_number):
        block = inraster.dataProvider().block(band + 1, extent, col, row)
        # Ok so we have a serialized bytes array and is shape.
        # We want to unserialize it and put back the shape
        deserialized_bytes = np.frombuffer(block.data(), dtype=np_dtype)
        array = np.reshape(deserialized_bytes, newshape=(row, col))
        # Then we want to write the unserialized array in the output in the correct band
        outarray[:, :, band] = array

    return outarray


def get_count(array):
    """
    Take an array containing r.geomorphon output value for a feature
    and return the max occurence.

    Parameters
    ----------
    array: numpy.ndarray :
        Array containing geomorphon class for an hedge.

    Returns
    -------
    max_occ : list or int :
        Max occurence of geomorphon class.
    """
    val, count = np.unique(array, return_counts=True)
    max_occ = val[count == np.max(count)]
    if len(max_occ) > 1:
        return list(max_occ)
    else:
        return int(max_occ)


def rasterize_and_clip(vector, raster):
    """
    Clip one raster by the other to have the same extents in both.
    Note : Both raster should have the same resolution.

    Parameters
    ----------
    vector : QgsVectorLayer :
        Any geometry.
    raster : QgsRasterLayer :
        Raster image.

    Returns
    -------
    out_raster_vector : QgsRasterLayer :
        Rasterised vector layer.
    out_other_raster : QgsRasterLayer :
        Clipped raster image.
    """
    resolution_x = raster.rasterUnitsPerPixelX()
    resolution_y = raster.rasterUnitsPerPixelY()

    # Convert poly_layer to raster layer using gdal
    alg_name = "gdal:rasterize"
    params = {"INPUT": vector,
              "FIELD": "pid",
              "UNITS": 1,  # CRS units
              "WIDTH": resolution_x,
              "HEIGHT": resolution_y,
              "EXTENT": vector.extent().buffered(resolution_x + resolution_y),  # to Get geoemorphon to compute edges
              "NODATA": 0,
              "DATA_TYPE": 1,  # Int16
              "OUTPUT": "TEMPORARY_OUTPUT"}
    output = processing.run(alg_name, params)["OUTPUT"]
    raster_vector = QgsRasterLayer(output)

    if raster_vector.extent().area() < raster.extent().area():
        alg_name = "gdal:cliprasterbyextent"
        params = {"INPUT": raster,
                  "PROJWIN": raster_vector.extent(),
                  "OUTPUT": "TEMPORARY_OUTPUT"}
        output = processing.run(alg_name, params)["OUTPUT"]
        raster = QgsRasterLayer(output)

    return raster_vector, raster


# def save_cropped_band(meta: dict, band_array: np.ndarray, save_path: str,
#                     out_transformation: affine.Affine = None,
#                     force: bool = False) -> None:
#     """
#     Write a cropped_band_array into the save_path file.

#     Add the out_transformation info.

#     Parameters
#     ----------
#     meta :
#         Metadata of original band.
#     band_array :
#         Data array of the cropped image.
#     save_path :
#         Path of the cropped image.
#     out_transformation :
#         Affine transformation used to compute pixel true coordinates.
#         (Default value = None)
#     force :
#         If true, rewrite data on existing one. If false do nothing.
#         (Default value = False)

#     Returns
#     -------
#     None.

#     """
#     # Update meta with cropped band characteristics
#     if band_array.dtype == "int64":
#         meta.update({
#             "driver": "GTiff",
#             "dtype": "int32"})
#     else:
#         meta.update({
#             "driver": "GTiff",
#             "dtype": band_array.dtype})
#     if out_transformation is not None:
#         meta.update({
#             "height": band_array.shape[1],
#             "width": band_array.shape[2],
#             "transform": out_transformation})
#     if not force and (os.path.exists(save_path)):
#         print(f"File already exists : {save_path}")
#     else:
#         # Write in file
#         with rasterio.open(save_path, "w", **meta) as dataset:
#             if len(band_array.shape) == 2:
#                 dataset.write(np.array([band_array]))
#             else:
#                 dataset.write(band_array)


def clip_raster_on_shapes(raster_path: str, shapes_path: str,
                          cropped_raster_path: str = None,
                          force: bool = False) -> Tuple[np.ndarray, dict, dict]:
    """
    Clip a raster image with a vector shapefile.

    Parameters
    ----------
    raster_path :
        Path of raster to be clipped.
    shapes_path :
        Path of the vector file.
    cropped_raster_path :
        Path of the output clipped raster. (Default value = None)
    force :
        Force output overwrite if True. (Default value = False)

    Returns
    -------
    Cropped data array, meta information dict and affine transformation.

    """
    shapes = gpd.read_file(shapes_path)
    with rasterio.open(raster_path) as src:
        meta = src.meta
        raster_crs = src.crs
        shapes = shapes.to_crs(raster_crs)
        cropped_array, out_transformation = mask(src, shapes=shapes.geometry,
                                                 all_touched=True, crop=True)
    if cropped_raster_path is not None:
        save_cropped_band(meta, cropped_array, cropped_raster_path,
                          out_transformation, force)
    else:
        return cropped_array, meta, out_transformation


def add_crs_to_raster(raster_path: str, epsg: int = 2154) -> None:
    """
    Add a missing CRS to a raster thanks to an EPSG code.

    Parameters
    ----------
    raster_path :
        Path of the input raster file.
    epsg :
        CRS EPSG code.  (Default value = 2154)

    Returns
    -------
    None.

    """
    with rasterio.open(raster_path, 'r+') as rds:
        rds.crs = rasterio.crs.CRS.from_epsg(code=epsg)


def merge_rasters(rasters_list: list[str], merged_path: str, force=False) -> None:
    """
    Merge multiples rasters into one.

    Parameters
    ----------
    rasters_list :
        Paths' list of rasters to be merged.
    merged_path :
        Path of the merged raster file.
    force :
        If True, force overwriting if a file with merged_path already exists.
        (Default value = False)

    Returns
    -------
    None.

    """
    dest, out_transform = merge(rasters_list)
    with rasterio.open(rasters_list[0], "r") as src:
        meta = src.meta
    save_cropped_band(meta, dest, merged_path, out_transform, force=force)


def raster_subtraction(raster_1: str, raster_2: str, subtraction_path: str,
                       force=False):
    """
    Subtract raster 2 to raster 1.

    Parameters
    ----------
    raster_1 :
        Path of the reference raster file.
    raster_2 :
        Path of the raster that is subtracted.
    subtraction_path :
        Path of the output subtraction raster file.
    force :
        If True, force overwriting if a file already exists.
        (Default value = False)

    Returns
    -------
    None.

    """
    with rasterio.open(raster_1, "r") as src:
        meta = src.meta
        arr_1 = src.read()
    with rasterio.open(raster_2, "r") as src:
        arr_2 = src.read()
    save_cropped_band(meta, arr_1 - arr_2, subtraction_path, force=force)
