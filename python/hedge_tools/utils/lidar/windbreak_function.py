from qgis.core import *
from qgis.core import QgsVertexIterator
from qgis.core import QgsFeedback
from qgis.utils import iface
import processing
from processing.core.Processing import Processing
from qgis.PyQt.QtCore import QVariant
import math
import numpy
import os
import hedge_tools.utils.vector.utils as vutils



# Function to convert the 'wind_orientation' angle into a trigonometric coordinate system.
def convert_wind_orientation(wind_orientation:float):
    """
    Allows to calculate the direction of the wind in the trigonometric coordinate system.

    Parameters
    ---
    wind_orientation (float): Angle in degrees indicating the origin of the wind.
    Return
    ---
    theta_rad (float): Angle in radians of the wind direction.
    """
    a = (wind_orientation + 180) % 360
    b = (90 - a) % 360
    theta_rad = math.radians(b)
    return theta_rad



# Function to calcul the coordinates of a vertex.
def get_coordinate_vertex(vertex:QgsVertexIterator):
    """
    Allows to calculate the coordinates (x, y) of each vertex and to store in list.
    
    Parameters
    ---
    vertex (vertices): Result of a Java-style iterator for transversal of all the geometry. 
    Return
    ---
    coordinates_vertex (list): List containing the (x,y) coordinates of a vertex.
    """
    coordinates_vertex = [vertex.x(), vertex.y()]
    return coordinates_vertex



# Function to calcul the translated coordinates of a vertex.
def translate_coordinates(coordinates_vertex:list,
                          height:float,
                          theta_rad:float,
                          max_extent:float):
    """
    Allows to calculate the coordinates in x, y of the point translated in relation to the top of the hedge, 
    the height and the direction of the wind.
    
    Parameters
    ---
    coordinates_vertex (list): List containing the x y coordinates of a hedge vertex.
    height (float): Hedge height.
    theta_rad (float): Angle in radians of the wind direction.
    max_extent (float): Factor of the height of the hedge characterizing the maximum distance to the hedge.
    Return
    ---
    translated_coordinates (list) : List containing the x y coordinates translation of a hedge vertex.
    """
    translated_coordinates = [coordinates_vertex[0] + height * max_extent * math.cos(theta_rad),
                              coordinates_vertex[1] + height * max_extent * math.sin(theta_rad)]
    return translated_coordinates



# Function to generate a list of points that delimits the shelter area.
def compute_shelter_area_points(geom:QgsGeometry,
                                vertices:QgsVertexIterator,
                                theta_rad:float,
                                height:float,
                                max_extent:float):
    """
    Allows to create a list of points, increment in a precise direction and will allow to generate a polygon of shelter area.
    
    Parameters
    ---
    geom (QgsGeometry): Geometrie of the hedge. 
    vertices (QgsVertexIterator): Geometric vertex generator from geometrie of the hedge.
    theta_rad (float): Angle in radians indicating the orientation of the shelter area.
    height (float) : Hedge height.
    max_extent (float): Factor of the height of the hedge characterizing the maximum distance to the hedge.
    Return
    ---
    line (list): List points to connect.
    """
    line = []
    max_vertex = len(list(geom.vertices()))  # counts the number of vertices in a vertex
    for count, vertex in enumerate(vertices, start=1):
        coordinates_vertex = get_coordinate_vertex(vertex)
        translated_coordinates = translate_coordinates(coordinates_vertex, height, theta_rad, max_extent)
        pnt = QgsPoint(translated_coordinates[0],
                       translated_coordinates[1])  # creating a point from translation coordinates
        if count == 1:
            line.append(vertex)
        line.append(vertex)
        line.insert(0, pnt)
        if count == max_vertex:
            line.insert(0,vertex)  # if count corresponds to the last vertex, add the point at the beginning of the list
    return line



# Function to create polygon shelter.
def compute_hedges_shelter(data_hedge:QgsLineString,
                           wind_orientation:float,
                           max_extent:float,
                           output_folder:str,
                           height_field_name:str="height",
                           porosity_field_name:str="op",
                           width_field_name:str="width",
                           hedge_id_field_name:str="id"):
    """
    Allows to create polygons representing the areas protected by hedges.
    
    Parameters
    ---
    data_hedge (QgsLineString): Layer to copy the attributes, geometry type and crs. 
    wind_orientation (float): Angle in degrees indicating the origin of the wind.
    max_extent (float): Factor of the height of the hedge characterizing the maximum distance to the hedge.
    output_folder (str): Path of file in which the output layers will be deposited.
    height_field_name (str): Name of the height field containing in the `data_hedge`. Defaults to "height".
    porosity_field_name (str): Name of the optical porosity field containing the `data_hedge`. Defaults to "op".
    width_field_name (str): Name of the width field containing in the `data_hedge`. Defaults to "width".
    hedge_id_field_name (str): Name of the id of the hedge field containing in the `data_hedge`. Defaults to "id".
    Return
    ---
    shelters_area (QgsPolygon) : Layer delimitation of shelter areas.
    """

    alg_name = "native:explodelines"
    params = { 'INPUT' : data_hedge,
                'OUTPUT' : 'TEMPORARY_OUTPUT' }
    explode_hedge = processing.run(alg_name, params)['OUTPUT']    
    explode_hedge_features = explode_hedge.getFeatures()
    
    hedges_shelter_path = os.path.join(output_folder , 'shelters_area.shp')
    shelters_area = vutils.create_layer(explode_hedge, geom_type='Polygon',
                                 copy_feat=False, copy_field=False, data_provider="memory")
    shelter_feat_list = []
    shelters_area_prov = shelters_area.dataProvider()
    shelters_area_prov.addAttributes([QgsField("id_shelter", QVariant.Int)])
    shelters_area_prov.addAttributes([QgsField(height_field_name, QVariant.Double, 'double', 10, 2)])
    shelters_area_prov.addAttributes([QgsField(width_field_name, QVariant.Double, 'double', 10, 2)])
    shelters_area_prov.addAttributes([QgsField(porosity_field_name, QVariant.Double,'double', 10, 2)])
    shelters_area_prov.addAttributes([QgsField("id_hedge", QVariant.Int)])
    shelters_area.updateFields()
    fid_idx = shelters_area.fields().indexFromName("id_shelter")  # get the index of the "fid" field
    height_field_idx = shelters_area.fields().indexFromName(height_field_name)
    width_field_idx = shelters_area.fields().indexFromName(width_field_name)
    porosity_field_idx = shelters_area.fields().indexFromName(porosity_field_name)
    hedge_id_field_idx = shelters_area.fields().indexFromName("id_hedge")
    max_fid = 0
    theta_rad = convert_wind_orientation(wind_orientation)

    for hedge in explode_hedge_features:
        height = hedge[height_field_name]  # creating a list containing the values ​​of the height field
        width = hedge[width_field_name]
        op = hedge[porosity_field_name]
        id_hedge = hedge[hedge_id_field_name]
        geom = hedge.geometry()  # creation of a list containing the geometries of the entities
        vertices = geom.vertices()  # geometric vertex generator from list geom
        line = compute_shelter_area_points(geom, vertices, theta_rad,
                                           height, max_extent)
        max_fid += 1
        field_list = [NULL] * 5  # creation of an empty list identical to the number of fields of the layer
        field_list[fid_idx] = max_fid
        field_list[height_field_idx] = height
        field_list[width_field_idx] = width
        field_list[porosity_field_idx] = op
        field_list[hedge_id_field_idx] = id_hedge
        linestring = QgsLineString(line)
        shelter_area = QgsPolygon(linestring)
        geom_shelter = QgsGeometry(shelter_area)
        new_feat = QgsFeature()
        new_feat.setAttributes(field_list)  # definition of entity attributes
        new_feat.setGeometry(geom_shelter)
        shelter_feat_list.append(new_feat)

    shelters_area.dataProvider().addFeatures(shelter_feat_list)
    alg_name = "native:fixgeometries"
    params = {
        'INPUT': shelters_area,
        'OUTPUT': "TEMPORARY_OUTPUT"}
    fix_shelters_area = processing.run(alg_name, params)["OUTPUT"]
    alg_name = "native:dissolve"
    params = { 'FIELD' : ['id_hedge'], 
                'INPUT' : fix_shelters_area, 
                'OUTPUT' : hedges_shelter_path, 
                'SEPARATE_DISJOINT' : False }
    shelters_area_process = processing.run(alg_name, params)['OUTPUT']
    shelters_area = QgsVectorLayer(shelters_area_process, "", 'ogr')
    return shelters_area



# Function to create mesh point.
def get_grid(resolution:float,
             shelters_area:QgsPolygon):
    """
    Allows to creates a vector layer with a grid covering a given extent. 
    The size and/or placement of each item in the grid is defined using horizontal and vertical spacing (resolution).    
    
    Parameters
    ---
    resolution (float): Layer to copy the attributes, geometry type and crs. 
    shelters_area (QgsPolygon): Layer delimitation of shelter areas. 
    Return
    ---
    pnt_mesh_bash (QgsPoint): Mesh result.
    """
    alg_name = "native:creategrid"
    params = {'CRS': QgsCoordinateReferenceSystem('EPSG:2154'),
              'EXTENT': shelters_area,
              'HOVERLAY': 0,
              'HSPACING': resolution,
              'OUTPUT': 'TEMPORARY_OUTPUT',
              'TYPE': 0,
              'VOVERLAY': 0,
              'VSPACING': resolution}
    pnt_mesh_bash = processing.run(alg_name, params)['OUTPUT']
    pnt_mesh_bash.dataProvider().createSpatialIndex()
    return pnt_mesh_bash



# Function to intersect grids point with shelter area.
def compute_pnt_intersect(shelters_area:QgsPolygon,
                          pnt_mesh_bash:QgsPoint,
                          data_hedge:QgsLineString,
                          wind_orientation:float,
                          feedback:QgsFeedback,
                          height_field_name:str="height",
                          porosity_field_name:str="op",
                          width_field_name:str="width",
                          hedge_id_field_name:str="id"):
    """
    Allows to select only the points of the grid layer that intersect the shelter areas.    
    
    Parameters
    ---
    shelters_area (QgsPolygon): Layer delimitation of shelter areas. 
    pnt_mesh_bash (QgsPoint): Mesh point layer.
    data_hedge (QgsLineString): Layer to copy the attributes, geometry type and crs. 
    wind_orientation (float): Angle in degrees indicating the origin of the wind.
    feedback (QgsFeedback) : Allow communication with the users (message, progress bar, ...).
    height_field_name (str): Name of the height field containing in the `data_hedge`. Defaults to "height".
    porosity_field_name (str): Name of the optical porosity field containing the `data_hedge`. Defaults to "op".
    width_field_name (str): Name of the width field containing in the `data_hedge`. Defaults to "width".
    Return
    ---
    pnt_intersect (QgsPoint): Result of intersection.
    """
    # Init progress bar steps
    alg_number = 8
    step_per_alg = int(100/alg_number)
    # Set progress
    feedback.setProgress(step_per_alg*3)
    feedback.pushInfo("3/8 - Intersect pnt in shelters area")
    alg_name = "native:fixgeometries"
    params = {'INPUT': shelters_area,
              'OUTPUT': "TEMPORARY_OUTPUT"}
    fix_shelters_area = processing.run(alg_name, params)["OUTPUT"]
    fix_shelters_area.dataProvider().createSpatialIndex()
    alg_name = "native:intersection"
    params = {'GRID_SIZE': None,
              'INPUT': pnt_mesh_bash,
              'INPUT_FIELDS': ['fid'],
              'OUTPUT': 'TEMPORARY_OUTPUT',
              'OVERLAY': fix_shelters_area,
              'OVERLAY_FIELDS': ['id_hedge', 'id_shelter', height_field_name,
                                 porosity_field_name, width_field_name],
              'OVERLAY_FIELDS_PREFIX': ''}
    pnt_intersect = processing.run(alg_name, params)['OUTPUT']
    #pnt_intersect = QgsVectorLayer(pnt_intersect, "pnt intersect", 'ogr')
    # Set progress
    feedback.setProgress(step_per_alg*4)
    feedback.pushInfo("4/8 - Compute minimal distance to the hedge")
    pnt_intersect = compute_dist_to_the_hedge(pnt_intersect, data_hedge,
                                               wind_orientation, hedge_id_field_name)
    # Check for cancellation
    if feedback.isCanceled():
        return {}
    del fix_shelters_area
    return pnt_intersect



# Function to select the most geometry near a point.
def select_intersect_near_pnt(pnt_intersect:QgsPoint,
                              pnt_geom:QgsGeometry):
    """
    Allows to select the most geometry near a point of a multipoint from `pnt_intersection` layer, result from an
    intersection between `pnt_mesh_bash` and `shelters_area`.
    
    Parameters
    ---
    pnt_intersect (QgsPoint): Geometry from points of the grid layer that intersect the shelter areas.  
    pnt_geom (QgsGeometry): Geometry from points of `pnt_intersect` layer.
    Return
    ---
    real_geom (point): Geometry select.
    """
    real_geom = []
    for geom in pnt_intersect.asGeometryCollection():
        min_dist = 99999
        dist = pnt_geom.distance(geom)
        if dist < min_dist:
            min_dist = dist
            real_geom = geom
    return real_geom



# Other method to calculate distance between two features.
def compute_dist_to_the_hedge(pnt_intersect:QgsPoint,
                              data_hedge:QgsLineString,
                              wind_orientation:float,
                              hedge_id_field_name:str):
    """
    Allows to calculate for each intersected point (point from `pnt_intersect`) the minimum distance to the hedge (
    polyline from `data_hedge`). This function directly modifies input layer and adds field `dist`.

    Parameters
    ---
    pnt_intersect (QgsPoint): Points of the grid layer that intersect the shelter areas.  
    data_hedge (QgsLineString): Layer to copy the attributes, geometry type and crs. 
    wind_orientation (float): Angle in degrees indicating the origin of the wind.
    hedge_id_field_name (str): Name of the id of the hedge field containing in the `data_hedge`. Defaults to "id".
    Return
    ---
    pnt_intersect (QgsPoint): Update pnt_intersect.
    """
    pnt_intersect.dataProvider().addAttributes([QgsField("dist", QVariant.Double, 'double', 10, 2)])
    pnt_intersect.dataProvider().addAttributes([QgsField("id", QVariant.Int)])
    pnt_intersect.updateFields()
    dist_idx = pnt_intersect.fields().indexFromName('dist')
    id_idx = pnt_intersect.fields().indexFromName('id')
    max_id = 0
    attr_map = {}
    wind_orientation = 90 - wind_orientation
    wind_theta = math.radians(wind_orientation)
    for pnt in pnt_intersect.getFeatures():
        max_id += 1
        expression = "{id} = {id_hedge}".format(id=hedge_id_field_name, id_hedge = pnt["id_hedge"])
        request = QgsFeatureRequest().setFilterExpression(expression)
        pnt_geom = pnt.geometry()
        d = 20000
        x1 = pnt_geom.asPoint().x()
        y1 = pnt_geom.asPoint().y()
        x2 = x1 + d * math.cos(wind_theta)
        y2 = y1 + d * math.sin(wind_theta)
        new_pnt = QgsPoint(x2, y2)
        new_line = QgsGeometry().fromPolyline([QgsPoint(pnt.geometry().asPoint()), new_pnt])
        hedge = next(data_hedge.getFeatures(request))
        length_dist = 0
        real_geom = []
        if new_line.intersects(hedge.geometry()):
            pnt_intersection = new_line.intersection(hedge.geometry())
            if pnt_intersection.wkbType() == QgsWkbTypes.Point:
                real_geom = pnt_intersection
            elif pnt_intersection.wkbType() == QgsWkbTypes.MultiPoint:
                real_geom = select_intersect_near_pnt(pnt_intersection, pnt_geom)
            else:
                pass
            dist = QgsGeometry().fromPolyline([QgsPoint(pnt.geometry().asPoint()), QgsPoint(real_geom.asPoint())])
            length_dist = dist.length()
            attr_map[pnt.id()] = {dist_idx: round(length_dist, 2), id_idx: max_id}
    pnt_intersect.dataProvider().changeAttributeValues(attr_map)
    pnt_intersect.updateFields()
    return pnt_intersect



# Function to extract duplicates points.
def extract_pnt_overlay(pnt_intersect:QgsPoint):
    """
    Allows to extract overlay points contained in the intersect point layer. 
    
    Parameters
    ---
    pnt_intersect (QgsPoint): Points of the grid layer that intersect the shelter areas.
    Return
    ---
    pnt_overlay (QgsPoint): Result overlay points.
    """
    pnt_intersect.dataProvider().createSpatialIndex()
    alg_name = "qgis:joinbylocationsummary"
    params = {'DISCARD_NONMATCHING': False,
              'INPUT': pnt_intersect,
              'JOIN': pnt_intersect,
              'JOIN_FIELDS': ['id'],
              'OUTPUT': 'TEMPORARY_OUTPUT',
              'PREDICATE': [0],
              'SUMMARIES': [0]}
    joint_count = processing.run(alg_name, params)["OUTPUT"]
    expression = "id_count >= 2"
    request = QgsFeatureRequest().setFilterExpression(expression)
    overlay_fids = [pnt["id"] for pnt in joint_count.getFeatures(request)]
    expression = "id in {}".format(tuple(overlay_fids))
    alg_name = "native:extractbyexpression"
    params = {"INPUT": pnt_intersect,
              "EXPRESSION": expression,
              "OUTPUT": 'TEMPORARY_OUTPUT'}
    pnt_overlay = processing.run(alg_name, params)["OUTPUT"]
    return pnt_overlay



# Function to extract uniques points.
def extract_pnt_without_overlay(pnt_intersect:QgsPoint,
                                output_folder:str):
    """
    Allows to extract uniques points, without overlay, contained in the intersect point layer.
    
    Parameters
    ---
    pnt_intersect (QgsPoint): Points of the grid layer that intersect the shelter areas.
    output_folder (str): Path of file in which the output layers will be deposited.
    Return
    ---
    pnt_without_overlay (QgsPoint): Result uniques points.
    """
    pnt_no_duplicates_path = os.path.join(output_folder + '/pnt_in_shelter_hedge.shp')
    alg_name = "qgis:joinbylocationsummary"
    params = {'DISCARD_NONMATCHING': False,
              'INPUT': pnt_intersect,
              'JOIN': pnt_intersect,
              'JOIN_FIELDS': ['id'],
              'OUTPUT': 'TEMPORARY_OUTPUT',
              'PREDICATE': [0],
              'SUMMARIES': [0]}
    joint_count = processing.run(alg_name, params)["OUTPUT"]
    expression = "id_count = 1"
    request = QgsFeatureRequest().setFilterExpression(expression)
    unique_fids = [pnt["id"] for pnt in joint_count.getFeatures(request)]
    expression = "id in {}".format(tuple(unique_fids))
    alg_name = "native:extractbyexpression"
    params = {"INPUT": pnt_intersect,
              "EXPRESSION": expression,
              "OUTPUT": pnt_no_duplicates_path}
    pnt_without_overlay = processing.run(alg_name, params)["OUTPUT"]
    pnt_without_overlay = QgsVectorLayer(pnt_without_overlay, "pnt_intersect_without_overlay", 'ogr')
    return pnt_without_overlay



# Function to generates a final single point layer.
def clean_pnt_overlay (pnt_without_overlay:QgsPoint,
                       pnt_intersect:QgsPoint,
                       pnt_overlay:QgsPoint):
    """
    Allows you to select the point with the minimum distance to the hedge from a given list.
    
    Parameters
    ---
    pnt_without_overlay (QgsPoint): Result uniques points.
    pnt_intersect (QgsPoint): Points of the grid layer that intersect the shelter areas.
    pnt_overlay (QgsPoint): Result overlay points.
    Return
    ---
    pnt_without_overlay (QgsPoint): Update pnt_without_overlay.
    """
    alg_name = "native:joinattributesbylocation"
    params = {'INPUT': pnt_overlay,
              'PREDICATE': [0],
              'JOIN': pnt_intersect,
              'JOIN_FIELDS': [],
              'METHOD': 0,
              'DISCARD_NONMATCHING': False,
              'PREFIX': '',
              'OUTPUT': 'TEMPORARY_OUTPUT'}
    join_spatial = processing.run(alg_name, params)["OUTPUT"]
    dico = {}
    for feat in join_spatial.getFeatures():
        if feat.attribute("id") in dico.keys():
            dico[feat.attribute("id")].append(feat.attribute("id_2"))
        else:
            dico[feat.attribute("id")] = [feat.attribute("id_2")]
    list_overlay = sorted(dico.values())
    set_list_overlay = set(tuple(row) for row in list_overlay)
    list_overlay_clean = list(set_list_overlay)
    list_real_pnt = []
    for pair in list_overlay_clean:
        min_dist = 99999
        expression = "id in {}".format(pair)
        request = QgsFeatureRequest().setFilterExpression(expression)
        real_pnt = None
        for pnt in pnt_overlay.getFeatures(request):
            dist = pnt['dist']
            if dist < min_dist:
                min_dist = dist
                real_pnt = pnt
        if real_pnt != None: 
            list_real_pnt.append(real_pnt)
    pnt_without_overlay.dataProvider().addFeatures(list_real_pnt)
    return pnt_without_overlay

# Function to generates a final single point layer.
def compute_pnt_in_shelter_hedge(shelters_area:QgsPolygon,
                                 data_hedge:QgsLineString,
                                 pnt_mesh_bash:QgsPoint,
                                 wind_orientation:float,
                                 output_folder:str,
                                 feedback:QgsFeedback,
                                 height_field_name:str="height",
                                 porosity_field_name:str="op",
                                 width_field_name:str="width",
                                 hedge_id_field_name:str="id"):
    """
    Allows to select point of `pnt_intersect_duplicates` closest to the hedge between overlapping ones.
    Import selected points into layer `pnt_intersect_no_duplicates`. 
    
    Parameters
    ---
    shelters_area (QgsPolygon): Layer delimitation of shelter areas. 
    data_hedge (QgsLineString): Layer to copy the attributes, geometry type and crs. 
    pnt_mesh_bash (QgsPoint): Mesh point layer.
    wind_orientation (float): Angle in degrees indicating the origin of the wind.
    output_folder (str): Path of file in which the output layers will be deposited.
    feedback (QgsFeedback) : Allow communication with the users (message, progress bar, ...).
    height_field_name (str): Name of the height field containing in the `data_hedge`. Defaults to "height".
    width_field_name (str): Name of the width field containing in the `data_hedge`. Defaults to "width".
    porosity_field_name (str): Name of the optical porosity field containing the `data_hedge`. Defaults to "op".
    hedge_id_field_name (str): Name of the id of the hedge field containing in the `data_hedge`. Defaults to "id".
    Return
    ---
    pnt_in_shelter_hedge (QgsPoint): Update pnt_in_shelter_hedge.
    """
    # Init progress bar steps
    alg_number = 8
    step_per_alg = int(100/alg_number)

    pnt_intersect = compute_pnt_intersect(shelters_area, pnt_mesh_bash, data_hedge, wind_orientation, feedback,
                                          height_field_name, porosity_field_name, width_field_name, hedge_id_field_name)
    # Set progress
    feedback.setProgress(step_per_alg*5)
    feedback.pushInfo("5/8 - Extract overlay pnt")
    pnt_overlay = extract_pnt_overlay(pnt_intersect)
    # Check for cancellation
    if feedback.isCanceled():
        return {}
    # Set progress
    feedback.setProgress(step_per_alg*6)
    feedback.pushInfo("6/8 - Extract without overlay pnt")
    pnt_without_overlay = extract_pnt_without_overlay(pnt_intersect, output_folder)
    # Set progress
    feedback.setProgress(step_per_alg*7)
    feedback.pushInfo("7/8 - Treatments on overlay pnt")
    pnt_in_shelter_hedge = clean_pnt_overlay (pnt_without_overlay, pnt_intersect, pnt_overlay)
    return pnt_in_shelter_hedge



# Function to rasterize.
def get_rasterize(pnt_in_shelter_hedge:QgsPoint,
                  field_name:str,
                  resolution:int,
                  output_path:str):
    """
    Allows to rasterize a vector layer according to a field. 
    
    Parameters
    ---
    pnt_in_shelter_hedge (QgsPoint): `compute_pnt_in_shelter_hedge` function result. 
    field_name (str): Name of the field in the input layer by which the rasterization will be assigned. 
    resolution (float): Dimension (width, height) of the resulting raster image.
    output_path (str): Path of file in which the output layers will be deposited. 
    Return
    ---
    raster : Raster image.
    """
    alg_name = "gdal:rasterize"
    params = {'BURN': 0,
              'DATA_TYPE': 5,
              'EXTENT': None,
              'HEIGHT': resolution,
              'FIELD': field_name,
              'INIT': None,
              'INPUT': pnt_in_shelter_hedge,
              'INVERT': False,
              'NODATA': 2,
              'OUTPUT': output_path,
              'UNITS': 1,
              'USE_Z': False,
              'WIDTH': resolution}
    raster = processing.run(alg_name, params)['OUTPUT']
    return raster




# Calculator .
def get_calculator_raster(INPUT_A:QgsRaster,
                          INPUT_B:QgsRaster,
                          INPUT_C:QgsRaster,
                          expression:str,
                          output_path:str):
    """
    Allows to calculate  a raster with an expression.
    
    Parameters
    ---
    INPUT_A (QgsRaster): Raster A. 
    INPUT_B (QgsRaster): Raster B. 
    INPUT_C (QgsRaster): Raster C.
    expression (str): Expression in raster calculator.
    output_path (str): Path of file in which the output layers will be deposited. 
    Return
    ---
    raster (QgsRaster): Raster result.
    """
    alg_name = "gdal:rastercalculator"
    params = {'BAND_A': 1, 
              'BAND_B': 1,
              'BAND_C': 1,
              'FORMULA': expression,
              'INPUT_A': INPUT_A,
              'INPUT_B': INPUT_B,
              'INPUT_C': INPUT_C,
              'OUTPUT': output_path,
              'NO DATA' : 1,
              'RTYPE': 5}
    raster = processing.run(alg_name, params)['OUTPUT']
    return raster



# Function to calculate friction velocity reduction factor with intermediates parameters.
def compute_fxh_and_param(pnt_in_shelter_hedge:QgsPoint,
                          resolution:float,
                          output_folder:str,
                          height_field_name:str="height",
                          porosity_field_name:str="op",
                          width_field_name:str="width",
                          sink:dict="TEMPORARY_OUTPUT"):
    """
    Allows to calculate friction velocity reduction factor at any distance in the surrounding area and the intermediates
     parameters.
    
    Parameters
    ---
    pnt_in_shelter_hedge (QgsPoint): `compute_pnt_in_shelter_hedge` function result. 
    resolution (float): Dimension (width, height) of the resulting raster image.
    output_folder (str): Path of file in which the output layers will be deposited.
    height_field_name (str): Name of the height field containing in the `pnt_in_shelter_hedge` layer. Defaults to
    "height".
    porosity_field_name (str): Name of the optical porosity field containing in the `pnt_in_shelter_hedge` layer.
    Defaults to "op".
    width_field_name (str): Name of the width field containing in the `pnt_in_shelter_hedge` layer. Defaults to
    "width".
    sink (dict) : Dictionnary including raster path, projection, raster. 
    Return
    ---
    raster_fxh (Qgsraster): Friction velocity reduction factor raster.
    """
    raster_dist_path = os.path.join(output_folder + '/raster_dist.tif')
    raster_a_path = os.path.join(output_folder + '/raster_a.tif')
    raster_b_path = os.path.join(output_folder + '/raster_b.tif')
    raster_c_path = os.path.join(output_folder + '/raster_c.tif')
    raster_d_path = os.path.join(output_folder + '/raster_d.tif')
    raster_xh_path = os.path.join(output_folder , 'raster_xh.tif')
    
    raster_op = get_rasterize(pnt_in_shelter_hedge, porosity_field_name, resolution, 'TEMPORARY_OUTPUT')
    raster_height = get_rasterize(pnt_in_shelter_hedge, height_field_name, resolution, 'TEMPORARY_OUTPUT')
    raster_width = get_rasterize(pnt_in_shelter_hedge, width_field_name, resolution, 'TEMPORARY_OUTPUT')
    raster_dist = get_rasterize(pnt_in_shelter_hedge,'dist',resolution,raster_dist_path)
    
    a = 'where(A!=1, 0.008 - 0.17*(A+0.02*(C/B)) + 0.17*(A+0.02*(C/B))**1.05, 1)'
    b = "where(A!=1, 1.35*numpy.exp(-0.5*(A+0.02*(C/B))**0.2), 1)"
    c = 'where(A!=1, 10*(1-0.5*(A+0.02*(C/B))), 1)'
    d = 'where(A!=1, 3 - (A+0.02*(C/B)), 1)'
    xh = 'A/B'
    
    # To calculate theta (optical porosity)
    # raster_theta_path = os.path.join(output_folder , 'raster_theta.tif')
    # theta = 'A+0.02*(C/B)'
    # raster_theta = get_calculator_raster(raster_op, raster_height, raster_width, theta, raster_theta_path)
    
    raster_a = get_calculator_raster(raster_op, raster_height, raster_width, a, raster_a_path)
    raster_b = get_calculator_raster(raster_op, raster_height, raster_width, b, raster_b_path)
    raster_c = get_calculator_raster(raster_op, raster_height, raster_width, c, raster_c_path)
    raster_d = get_calculator_raster(raster_op, raster_height, raster_width, d, raster_d_path)
    raster_xh = get_calculator_raster(raster_dist, raster_height, None, xh, raster_xh_path)
    alg_name = "gdal:rastercalculator"
    params = {'BAND_A': 1,
              'BAND_B': 1, 'BAND_C': 1,
              'BAND_D': 1, 'BAND_E': 1,
              'FORMULA': 'where(A!=1, 1 - numpy.exp(-A*E**2)+B*numpy.exp(-0.003*(E+C)**D), 1)',
              'INPUT_A': raster_a,
              'INPUT_B': raster_b,
              'INPUT_C': raster_c,
              'INPUT_D': raster_d,
              'INPUT_E': raster_xh,
              'OUTPUT': sink,
              'NO_DATA': 1,
              'RTYPE': 5}
    raster_fxh_process = processing.run(alg_name, params)['OUTPUT']
    raster_fxh = QgsRasterLayer(raster_fxh_process, "raster_fxh", "gdal")
    raster_fxh.dataProvider().setNoDataValue(1, -255)
    return raster_fxh



def compute_fxh(pnt_in_shelter_hedge:QgsPoint,
                resolution:float,
                height_field_name:str="height",
                porosity_field_name:str="op",
                width_field_name:str="width",
                sink:dict="TEMPORARY_OUTPUT"):
    """
    Allows to calculate friction velocity reduction factor at any distance in the surrounding area. 
    
    Parameters
    ---
    pnt_in_shelter_hedge (QgsPoint): `compute_pnt_in_shelter_hedge` function result. 
    resolution (float): Dimension (width, height) of the resulting raster image.
    height_field_name (str): Name of the height field containing in the `data_hedge` layer. Defaults to "height".
    porosity_field_name (str): Name of the optical porosity field containing in the `data_hedge` layer. Defaults to
    "op".
    width_field_name (str): Name of the width field containing in the `data_hedge` layer. Defaults to "width".
    sink (dict) : Dictionnary including raster path, projection, raster. 
    Return
    ---
    raster_fxh (Qgsraster): Friction velocity reduction factor raster.
    """
    raster_op = get_rasterize(pnt_in_shelter_hedge, porosity_field_name, resolution, 'TEMPORARY_OUTPUT')
    raster_height = get_rasterize(pnt_in_shelter_hedge, height_field_name, resolution, 'TEMPORARY_OUTPUT')
    raster_width = get_rasterize(pnt_in_shelter_hedge, width_field_name, resolution, 'TEMPORARY_OUTPUT')
    raster_dist = get_rasterize(pnt_in_shelter_hedge, 'dist', resolution, 'TEMPORARY_OUTPUT')
    xh = 'A/B'
    raster_xh = get_calculator_raster(raster_dist, raster_height, None, xh, 'TEMPORARY_OUTPUT')
    alg_name = "gdal:rastercalculator"
    params = {'BAND_A': 1,
              'BAND_B': 1, 'BAND_C': 1,
              'BAND_D': 1, 'BAND_E': 1,
              'FORMULA': 'where(A!=1, 1 - numpy.exp( -(0.008 - 0.17*(A+0.02*(C/B)) + \
                    + 0.17*(A+0.02*(C/B))**1.05)*D**2) + \
                    (1.35*numpy.exp(-0.5*(A+0.02*(C/B))**0.2))* \
                    + numpy.exp(-0.003*((D+(10*(1-0.5*(A+0.02*(C/B)))))**(3 - (A+0.02*(C/B))))), 1)',
              'INPUT_A': raster_op,
              'INPUT_B': raster_height,
              'INPUT_C': raster_width,
              'INPUT_D': raster_xh,
              'OUTPUT': sink,
              'NO_DATA': 1,
              'RTYPE': 5}
    raster_fxh = processing.run(alg_name, params)['OUTPUT']
    raster_fxh = QgsRasterLayer(raster_fxh, "raster_fxh", "gdal")
    raster_fxh.dataProvider().setNoDataValue(1, -255)
    return raster_fxh



def compute_windbreak_effect(data_hedge:QgsLineString, 
                             wind_orientation:float,
                             max_extent:float,
                             resolution:float,
                             output_folder:str,
                             feedback:QgsFeedback,
                             height_field_name:str="height",
                             porosity_field_name:str="op",
                             width_field_name:str="width",
                             hedge_id_field_name:str="id",
                             intermediate_raster:bool=False,
                             sink:dict="TEMPORARY_OUTPUT"):
    """
    Allows to calculate friction velocity reduction factor at any distance in the surrounding area. 
    
    Parameters
    ---
    data_hedge (QgsLineString): Layer to copy the attributes, geometry type and crs. 
    wind_orientation (float): Angle in degrees indicating the origin of the wind.
    max_extent (float): Factor of the height of the hedge characterizing the maximum distance to the hedge.
    resolution (float): Dimension (width, height) of the resulting raster image.
    output_folder (str): Path of file in which the output layers will be deposited.
    feedback (QgsFeedback) : Allow communication with the users (message, progress bar, ...).
    height_field_name (str): Name of the height field containing in the `data_hedge` layer. Defaults to "height".
    porosity_field_name (str): Name of the optical porosity field containing in the `data_hedge` layer. Defaults to
    "op".
    width_field_name (str): Name of the width field containing in the `data_hedge` layer. Defaults to "width".
    hedge_id_field_name (str): Name of the id of the hedge field containing in the `data_hedge` layer. Defaults to
    "id".
    intermediate_raster (bool): Option to store the intermediate raster calculated by the function. If None, only
    the final raster 'raster_fxh' will be stored.
    sink (dict) : Dictionnary including raster path, projection, raster. 
    Return
    ---
    fxh (Qgsraster): Friction velocity reduction factor raster.
    """

    # Init progress bar steps
    alg_number = 8
    step_per_alg = int(100/alg_number)
    # Set progress
    feedback.setProgress(step_per_alg)
    feedback.pushInfo("1/8 - Compute shelters area")
    shelters_area = compute_hedges_shelter(data_hedge, wind_orientation, max_extent, output_folder,
                                           height_field_name, porosity_field_name, width_field_name, hedge_id_field_name)
    # Check for cancellation
    if feedback.isCanceled():
        return {}
    # Set progress
    feedback.setProgress(step_per_alg*2)
    feedback.pushInfo("2/8 - Get grid")
    pnt_mesh_bash = get_grid(resolution,
                             shelters_area)
    pnt_in_shelter_hedge = compute_pnt_in_shelter_hedge(shelters_area, data_hedge, pnt_mesh_bash, wind_orientation, output_folder, feedback,
                                                        height_field_name, porosity_field_name, width_field_name, hedge_id_field_name)
    # Set progress
    feedback.setProgress(step_per_alg*8)
    feedback.pushInfo("8/8 - Compute raster fxh")
    if intermediate_raster is True:
        fxh = compute_fxh_and_param(pnt_in_shelter_hedge, resolution, output_folder, height_field_name,
                                    porosity_field_name, width_field_name, sink)
    else:
        fxh = compute_fxh(pnt_in_shelter_hedge, resolution, height_field_name,
                          porosity_field_name, width_field_name, sink)        
    return fxh, shelters_area, pnt_in_shelter_hedge
