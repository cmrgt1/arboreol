"""
Functions used to preprocess lidar point clouds with PDAL library.
Following choices are made:
    — Point cloud format used is LAS/LAZ.
    — Geospatial data are in Lambert 93, EPSG = 2154.
"""

# import json
# import os
# import warnings
# from pathlib import Path
# from subprocess import check_output

# import geopandas as gpd
# import numpy as np
# import pandas as pd
# import pdal
# import pyproj


from hedge_tools.utils.mathematical import (skewness, kurtosis,
                                            kurtosis_pearson,
                                            skewness_fusion,
                                            kurtosis_fusion,
                                            compute_percentile)

# os.environ['PROJ_LIB'] = '/home/dynafor/miniconda3/envs/pdal/share/proj'


# TODO Check for column name and type (does not work everytime)
#  as there is no error message
#  Check CRS of las and shp
def clip(point_cloud_path: str, polygons_path: str,
         clipped_point_cloud_path: str, column: str = "id",
         polygon_id: int = None) -> None:

    """
    Clip a las file on a shapefile extent.

    Parameters
    ----------
    point_cloud_path :
        Path of the point cloud file.
    polygons_path :
        Path of the polygons file.
    clipped_point_cloud_path :
        Path of the output clipped data file.
    column :
        The OGR datasource column from which to read the attribute
        (Default value = "id")
    polygon_id :
        ID of a unique polygon inside the shapefile.
        If specified, the clip will be done only for this one.
        (Default value = None)

    Returns
    -------
    None.

    """
    pipe = pdal.Reader.las(filename=point_cloud_path).pipeline()
    pipe |= pdal.Filter.ferry(dimensions="=> Zone")
    pipe |= pdal.Filter.overlay(datasource=polygons_path, dimension="Zone",
                                column=column)
    if not polygon_id:
        pipe |= pdal.Filter.range(limits=f"Zone[1:]")
        print("No polygon id declared")
    else:
        pipe |= pdal.Filter.range(limits=f"Zone[{polygon_id}:{polygon_id}]")
        print("Polygon id declared")
    pipe |= pdal.Writer.las(filename=clipped_point_cloud_path)
    pipe.execute()


def merge_pcl(pcl_paths: list[str], merged_path: str) -> None:
    """
    Merge multiple point clouds into one.

    Parameters
    ----------
    pcl_paths :
        Paths of point clouds files to merge.
    merged_path :
        Path of the merged point cloud file.

    Returns
    -------
    None.

    """
    nb_files = len(pcl_paths)
    if nb_files == 0:
        raise ValueError("List of las files [pcl_paths] should not be empty.")
    elif nb_files == 1:
        pipe_merge = pdal.Reader.las(filename=pcl_paths[0]).pipeline()
        pipe_merge |= pdal.Writer.las(filename=merged_path)
        pipe_merge.execute()
        warnings.warn(f"List of las files [pcl_paths] contains only one file. "
                      f"Original file is saved at {merged_path} ")
    else:
        pipe_merge = pdal.Reader.las(filename=pcl_paths[0]).pipeline()
        for path_pcl in pcl_paths[1:]:
            pipe_merge |= pdal.Reader.las(filename=path_pcl)
        pipe_merge |= pdal.Filter.merge()
        pipe_merge |= pdal.Writer.las(filename=merged_path)
        pipe_merge.execute()


def compute_ground_classification(point_cloud_path: str,
                                  classified_path: str = None) -> None:
    """
    Compute a ground classification of a point cloud.
    
    This ground classification uses the SMRF algorithm.

    Parameters
    ----------
    point_cloud_path :
        Path of the input point cloud.
    classified_path :
        Path of the classified point cloud. (Default value = None)

    Returns
    -------
    None.

    """
    if not classified_path:
        filename, file_extension = os.path.splitext(point_cloud_path)
        classified_path = filename + "_classified.las"

    pipe_ground = pdal.Reader.las(filename=point_cloud_path).pipeline()
    pipe_ground |= pdal.Filter.smrf(scalar=1.2, slope=0.2, threshold=0.45,
                                    window=16.0)
    pipe_ground |= pdal.Writer.las(filename=classified_path)
    pipe_ground.execute()


def compute_dem(las_path: str, dem_path: str = None,
                resolution: float = 0.5) -> None:
    """
    Compute DEM (Digital Elevation Model) from a classified point cloud.

    Parameters
    ----------
    las_path :
        Path of the input LAS file.
    dem_path :
        Path of the DEM raster file. (Default value = None)
    resolution :
        Spatial resolution of the DEM raster file.(Default value = 0.5)

    Returns
    -------
     None.

    """
    if not dem_path:
        filename, file_extension = os.path.splitext(las_path)
        dem_path = filename + "_DEM.tiff"

    pipe_dem = pdal.Reader.las(filename=las_path).pipeline()
    pipe_dem |= pdal.Filter.hag_dem(raster=dem_path)
    pipe_dem |= pdal.Writer.gdal(filename=dem_path, resolution=resolution,
                                 gdaldriver="GTiff", output_type="idw",
                                 window_size=24)
    pipe_dem.execute()


def compute_dtm(classified_las_path: str, dtm_path: str = None,
                resolution: float = 0.5) -> None:
    """
    Compute DTM (Digital Terrain Model) raster from a classified point cloud.

    Parameters
    ----------
    classified_las_path :
        Path of the input LAS file.
    dtm_path :
        Path of the DTM raster file. (Default value = None)
    resolution :
        Spatial resolution of the DTM raster file.(Default value = 0.5)

    Returns
    -------
    None.

    """
    if not dtm_path:
        filename, file_extension = os.path.splitext(classified_las_path)
        dtm_path = filename + "_DTM.tiff"

    pipe_ground = pdal.Reader.las(filename=classified_las_path).pipeline()
    pipe_ground |= pdal.Filter.range(limits="Classification[2:2]")
    pipe_ground |= pdal.Writer.gdal(filename=dtm_path, resolution=resolution,
                                    gdaldriver="GTiff", output_type="idw",
                                    window_size=24)
    pipe_ground.execute()


def compute_hag(classified_las: str, las_hag_path: str = None,
                method: str = "delaunay") -> None:
    """
    Compute Height Above Ground (HAG) value for a point cloud.
    
    Two methods are available : one using a delaunay triangulation
    and the other one, a nearest neighbor approach.
    In both methods, the input point cloud must be classified.

    Parameters
    ----------
    classified_las :
        Path the input point cloud file.
    las_hag_path :
        Path of the output point cloud file. (Default value = None)
    method :
        Method's name. Possible values are 'delaunay' and 'nn'
        (Default value = "delaunay")

    Returns
    -------
    None.

    """
    if not las_hag_path:
        filename, file_extension = os.path.splitext(classified_las)
        las_hag_path = filename + f"_HAG_{method}.las"

    pipe_hag_nn = pdal.Reader.las(filename=classified_las).pipeline()
    if method == "delaunay":
        pipe_hag_nn |= pdal.Filter.hag_delaunay()
    elif method == "nn":
        pipe_hag_nn |= pdal.Filter.hag_nn()
    else:
        raise ValueError(f"This method ({method}) is not recognized")
    pipe_hag_nn |= pdal.Filter.ferry(dimensions="HeightAboveGround=>Z")
    pipe_hag_nn |= pdal.Writer.las(filename=las_hag_path)
    pipe_hag_nn.execute()


def compute_pointwise_metrics(input_las: str, stats_las_path: str,
                              csv_path: str) -> None:
    """
    Compute all PDAL pointwise metrics for a LAS file.
    
    For the following list of attributes, average, kurtosis, maximum, minimum,
    skewness, stddev, variance are computed.
    
    Check PDAL documentation to know more about each pointwise feature.
    https://pdal.io/en/latest/stages/filters.html#pointwise-features
    
    Blue, Classification, Coplanar, Curvature, EdgeOfFlightLine, Eigenvalue0,
    Eigenvalue1, Eigenvalue2, GpsTime, Green, Intensity, Linearity,
    LocalOutlierFactor, LocalReachabilityDistance, Miniball, NNDistance,
    NormalX, NormalY, NormalZ, NumberOfReturns, OptimalKNN, OptimalRadius,
    Planarity, PlaneFit, PointSourceId, RadialDensity, Rank, Reciprocity, Red,
    ReturnNumber, ScanAngleRank, ScanDirectionFlag, Scattering, UserData,
    Verticality, X, Y, Z

    Parameters
    ----------
    input_las :
        Path of the input LAS file.
    stats_las_path :
        Path of the output LAS file with computed metrics.
    csv_path :
        Path of the csv file storing metrics.

    Returns
    -------
    None.

    """
    # Compute metrics.
    pipe = pdal.Reader.las(filename=input_las).pipeline()
    pipe |= pdal.Filter.approximatecoplanar()
    pipe |= pdal.Filter.covariancefeatures()
    pipe |= pdal.Filter.eigenvalues()
    pipe |= pdal.Filter.estimaterank()
    pipe |= pdal.Filter.lof()
    pipe |= pdal.Filter.miniball()
    pipe |= pdal.Filter.nndistance()
    pipe |= pdal.Filter.normal()
    pipe |= pdal.Filter.optimalneighborhood()
    pipe |= pdal.Filter.planefit()
    pipe |= pdal.Filter.radialdensity()
    pipe |= pdal.Filter.reciprocity()
    # Export in a LAS file.
    pipe |= pdal.Writer.las(filename=stats_las_path, extra_dims="all",
                            minor_version="4")
    pipe.execute()
    # Export metrics statistics in a csv
    pipe |= pdal.Filter.stats(advanced="true")
    pipe.execute()
    stats_values = pipe.metadata['metadata']["filters.stats"]["statistic"]
    df = pd.DataFrame(stats_values)
    column_names = df["name"]
    df_transpose = df.T
    df_transpose.columns = column_names
    df_transpose.to_csv(csv_path, encoding='utf-8', index=True)


def compute_tile_products(input_las_path: str, output_folder: str,
                          ground_classification_path: str = None,
                          dem_path: str = None, dtm_path: str = None,
                          dhm_path: str = None, hag_path: str = None,
                          set_epsg: int = None) -> None:
    """
    Compute different products that can be computed for a point cloud.
    
    The computed products are:
        — a ground classification;
        — a DEM raster;
        — a DTM raster;
        — a CHM raster;
        — a HAG las.

    Parameters
    ----------
    input_las_path :
        Path of the point cloud LAS file.
    ground_classification_path :
        Path of the classified point cloud LAS file.
    dem_path :
        Path of the DEM raster file.
        (Default value = None)
    dtm_path :
        Path of the DTM raster file.
        (Default value = None)
    dhm_path :
        Path of the DHM raster file.
        (Default value = None)
    hag_path :
        Path of the HAG LAS file.
        (Default value = None)
    output_folder :
        Path of the folder where output products are written.
    set_epsg :
        CRS EPSG code of the point cloud if info is missing in metadata.
        (Default value = None)

    Returns
    -------
    None.

    """
    # Set CRS
    filename = Path(input_las_path).stem
    file_epsg = os.path.join(output_folder, filename + ".las")
    if set_epsg:
        set_las_crs_from_epsg(input_las_path, file_epsg, 2154)

    # Compute ground classification
    if not ground_classification_path:
        filename, file_extension = os.path.splitext(file_epsg)
        ground_classification_path = filename + "_classified.las"
    compute_ground_classification(file_epsg, ground_classification_path)
    # Compute DEM
    if not dem_path:
        dem_path = filename + "_DEM.tiff"
    compute_dem(ground_classification_path, dem_path)
    # Compute DTM
    if not dtm_path:
        dtm_path = filename + "_DEM.tiff"
    compute_dtm(ground_classification_path, dtm_path)
    # Compute HAG
    if not hag_path:
        hag_path = filename + "_HAG.las"
    compute_hag(ground_classification_path, hag_path)
    # Compute CHM/DHM
    if not dhm_path:
        dhm_path = filename + "_DHM.tiff"
    compute_dem(hag_path, dhm_path)


def classification_count(las_path: str) -> np.ndarray:
    """
    Counts the number of times each unique item appears in the classification's
    attribute of a LAS file.

    Parameters
    ----------
    las_path :
        Path of the input LAS file.

    Returns
    -------
    Counts of every classification's values.

    """
    pipe = pdal.Reader.las(filename=las_path).pipeline()
    pipe.execute()
    counts = np.unique(pipe.arrays[0]["Classification"], return_counts=True)
    return np.array(counts)


# Point cloud formats.
def laz_to_las(laz_path: str, las_path: str = None) -> None:
    """
    Converts a point cloud file in LAZ format to a LAS format file.

    Parameters
    ----------
    laz_path :
        Input point cloud file in LAZ format.
    las_path :
        Output point cloud file in LAS format.
        If not specified, output filename is same as original one.
        (Default value = None)

    Returns
    -------
    None.

    """
    if not las_path:
        las_path = laz_path.replace(".laz", ".las")
    pipe_clip_pygar = pdal.Reader.las(filename=laz_path).pipeline()
    pipe_clip_pygar |= pdal.Writer.las(filename=las_path)
    pipe_clip_pygar.execute()


def las_to_laz(las_path: str, laz_path: str = None) -> None:
    """
    Converts a point cloud file in LAS format to a LAZ format file.

    Parameters
    ----------
    las_path :
        Input point cloud file in LAS format.
    laz_path :
        Output point cloud file in LAZ format.
        If not specified, output filename is same as original one.
        (Default value = None)

    Returns
    -------
    None.

    """
    if not laz_path:
        laz_path = las_path.replace(".las", ".laz")
    pipe_clip_pygar = pdal.Reader.las(filename=las_path).pipeline()
    pipe_clip_pygar |= pdal.Writer.las(filename=laz_path)
    pipe_clip_pygar.execute()


# Coordinates Reference System.
def get_las_epsg_subprocess(las_path: str) -> int:
    """
    Get EPSG code of a LAS file using a subprocess call.

    Parameters
    ----------
    las_path :
        Path of the input LAS file.

    Returns
    -------
    CRS EPSG code of the point cloud data.

    """
    txt = check_output(f"pdal info {las_path} --metadata",
                       shell=True).replace(b'\\\\"', b'\\"')
    spatial_reference = json.loads(txt)["metadata"]["comp_spatialreference"]
    if spatial_reference == "":
        raise ValueError("There is no CRS attached to this point cloud")
    las_crs = pyproj.CRS.from_wkt(spatial_reference)
    return las_crs.to_epsg()


def get_las_epsg(las_path: str) -> int:
    """
    Get EPSG code of a LAS file by reading the file and its metadata.

    Parameters
    ----------
    las_path :
        Path of the input LAS file.

    Returns
    -------
     CRS EPSG code of the point cloud data.

    """
    if not os.path.exists(las_path):
        raise ValueError("Input file does not exist")
    pipe = pdal.Reader.las(filename=las_path, count=1).pipeline()
    pipe.execute()
    crs_wkt = pipe.metadata["metadata"]["readers.las"]["comp_spatialreference"]
    if crs_wkt == "":
        raise ValueError("There is no CRS attached to this point cloud")
    las_crs = pyproj.CRS.from_wkt(crs_wkt)
    return las_crs.to_epsg()


def set_las_crs_from_epsg(las_path: str, out_path: str, epsg: int) -> None:
    """
    Set a CRS to a LAS file without a defined one.

    Parameters
    ----------
    las_path :
        Path of the input LAS file.
    out_path :
        Path of the output LAS file with a CRS.
    epsg :
        EPSG code of the CRS added.

    Returns
    -------
    None.

    """
    if not os.path.exists(las_path):
        raise ValueError("Input file does not exist")
    pipe_c = pdal.Reader.las(filename=las_path).pipeline()
    pipe_c |= pdal.Writer.las(filename=out_path, a_srs=f"EPSG:{str(epsg)}")
    pipe_c.execute()


def reproject_las(las_path: str, out_path: str, epsg: int) -> None:
    """
    Reproject a LAS file in a specific CRS.

    Parameters
    ----------
    las_path :
        Path of the input LAS file.
    out_path :
        Path of the output LAS file with a CRS.
    epsg :
        EPSG code of the CRS added.

    Returns
    -------
    None.

    """
    pipe_c = pdal.Reader.las(filename=las_path).pipeline()
    pipe_c |= pdal.Filter.reprojection(out_srs=f"EPSG:{str(epsg)}")
    pipe_c |= pdal.Writer.las(filename=out_path)
    pipe_c.execute()


# Raster functions

# Point cloud metrics
def format_pdal_metrics_csv(pdal_metrics_csv: str,
                            clean_metrics_csv: str) -> None:
    """
    Remove useless metrics and format data in a one object CSV file.

    Parameters
    ----------
    pdal_metrics_csv :
        Path of the original PDAL metrics CSV file.
    clean_metrics_csv :
        Path of the formatted PDAL metrics CSV file.

    Returns
    -------
    None.

    """
    hedge_metrics = pd.read_csv(pdal_metrics_csv, index_col=0)
    pdal_df = hedge_metrics.drop(["name", "count", "position"])
    pdal_df = pdal_df.drop(["Blue", "Classification", "GpsTime", "Green",
                            "Red", "X", "Y"], axis=1)
    pdal_dict = pdal_df.to_dict()
    new_dict = {}
    pdal_metrics_names = []
    for keys, values in pdal_dict.items():
        for stats, metrique in values.items():
            metric_name = keys + "_" + stats
            pdal_metrics_names.append(metric_name)
            new_dict[metric_name] = float(metrique)

    df = pd.DataFrame.from_dict(new_dict, orient="index")
    df.to_csv(clean_metrics_csv)


def combine_metrics_to_shape(shapefile_path: str,
                             pdal_formatted_metrics_csv: str,
                             shapefile_metrics_path: str) -> None:
    """
    Add point cloud metrics to a shapefile.

    Parameters
    ----------
    shapefile_path :
        Path of the input shapefile.
    pdal_formatted_metrics_csv :
        Path of the CSV file with PDAL metrics formatted.
    shapefile_metrics_path :
        Path of the output shapefile with metrics.

    Returns
    -------
    None.

    """
    hedges = gpd.read_file(shapefile_path)
    metrics = pd.read_csv(pdal_formatted_metrics_csv, index_col=0)
    metrics_h = metrics.T.to_dict(orient="records")[0]
    df_metrics = gpd.GeoDataFrame(metrics_h, index=[0])
    hedge_metrics_df = pd.concat([hedges, df_metrics], axis=1)
    hedge_metrics_df.to_file(shapefile_metrics_path, driver='GPKG')


# TODO replace hand made function for percentile with numpy one


def compute_heights_percentiles(input_las: str, csv_path: str,
                                percentiles: list[float],
                                min_height: float = 0.) -> None:
    """
    Compute height percentiles for a LAS file.

    Parameters
    ----------
    input_las :
        Path of the input LAS file.
    csv_path :
        Path of the csv file storing metrics.
    percentiles :
        List of percentile values that will be computed.
    min_height :
        Threshold height under which points will be ignored for the computation.
        (Default value = 0.)

    Returns
    -------
    None.

    """
    # Compute metrics.
    pipe_heights_percentiles = pdal.Reader.las(filename=input_las).pipeline()
    pipe_heights_percentiles |= pdal.Filter.sort(dimension="Z", order="ASC")
    pipe_heights_percentiles.execute()
    z_sorted = pipe_heights_percentiles.arrays[0]["Z"]
    values = compute_percentile(z_sorted, percentiles, min_height)
    dict_percentiles = {}
    for i, per in enumerate(percentiles):
        dict_percentiles[f"P{per}"] = values[i]
    # Export metrics statistics in a csv
    df = pd.DataFrame(dict_percentiles.items())
    df.columns = [["Metrics", "Value"]]
    df.to_csv(csv_path, index=False)


def compute_mathematical_metrics(input_las: str, csv_path: str,
                                 attributes: str = "zi") -> None:
    """
    Computes mathematical metrics for heights and/or intensity of point cloud.

    Set attributes to
        'z' for heights,
        'i' for intensity
        'zi' for both heights and intensity.

    Parameters
    ----------
    input_las: str :
        Path of the input LAS file.
    csv_path: str :
        Path of the output CSV file.
    attributes: str :
         (Default value = "zi")

    Returns
    -------
    None.
    """
    pipe_math_metrics = pdal.Reader.las(filename=input_las).pipeline()
    pipe_math_metrics.execute()

    metrics = {}
    if "z" in attributes:
        z_sorted = pipe_math_metrics.arrays[0]["Z"]
        metrics["Z_kurtosis"] = kurtosis(z_sorted)
        metrics["Z_skewness"] = skewness(z_sorted)
        metrics["Z_kurtosis_pearson"] = kurtosis_pearson(z_sorted)
        metrics["Z_kurtosis_fusion"] = kurtosis_fusion(z_sorted)
        metrics["Z_skewness"] = skewness_fusion(z_sorted)
        metrics["Z_std"] = np.std(z_sorted)
        metrics["Z_variance"] = np.var(z_sorted)

    if "i" in attributes:
        i_sorted = pipe_math_metrics.arrays[0]["Intensity"]
        metrics["Intensity_kurtosis"] = kurtosis(i_sorted)
        metrics["Intensity_skewness"] = skewness(i_sorted)
        metrics["Intensity_kurtosis_pearson"] = kurtosis_pearson(i_sorted)
        metrics["Intensity_kurtosis_fusion"] = kurtosis_fusion(i_sorted)
        metrics["Intensity_skewness"] = skewness_fusion(i_sorted)
        metrics["Intensity_std"] = np.std(i_sorted)
        metrics["Intensity_variance"] = np.var(i_sorted)

    df = pd.DataFrame(metrics.items())
    df.columns = [["Metrics", "Value"]]
    df.to_csv(csv_path, index=False)
