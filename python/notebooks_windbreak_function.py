from qgis.core import *
from qgis.core import NULL
from qgis.utils import iface
import processing
from processing.core.Processing import Processing
from qgis.PyQt.QtCore import QVariant
import math
import numpy
import os
import hedge_tools.utils.vector.utils as vutils
import hedge_tools.utils.lidar.windbreak_function as wf
import pandas
import geopandas as gpd

def gdf_from_qgis_layer(layer):
    """
    From a QgsVectorLayer extract fields value from given fields name and construct a df

    Parameters
    ----------
    layer : QgsVectorLayer
    fields_name : ite[str]

    Return
    ------
    df : pd.DataFrame
    """
    attr_map = {}
    attr_map["id"] = [f.id() for f in layer.getFeatures()]
    for name in layer.fields().names():
        attr_map[name] = [f[name] if f[name] != NULL else None for f in layer.getFeatures()]

    attr_map["geometry"] = [f.geometry() for f in layer.getFeatures()]

    gdf = gpd.GeoDataFrame.from_dict(attr_map)
    gdf = gdf.set_crs(layer.crs().authid())

    return gdf



def compute_hedges_shelter_test (data_hedge: QgsVectorLayer, 
                                wind_orientation: int,
                                max_extent: int,
                                output_folder: str,
                                height_field_name: str="height",
                                porosity_field_name: str="op",
                                width_field_name: str="width",
                                hedge_id_field_name: str="id") :
    """
    Allows to create polygons representing the areas protected by hedges and construct a df.
    
    Parameters
    ---
    data_hedge (QgsVectorLayer): Layer to copy the attributes, geometry type and crs. 
    wind_orientation (int): Angle in degrees indicating the origin of the wind.
    max_extent (int): factor of the height of the hedge characterizing the maximum distance to the hedge.
    output_folder (str): path of file in which the output layers will be deposited.
    height_field_name (str): name of the height field containing in the `data_hedge`. Defaults to "height".
    porosity_field_name (str): name of the optical porosity field containing the `data_hedge`. Defaults to "op".
    width_field_name (str): name of the width field containing in the `data_hedge`. Defaults to "width".
    hedge_id_field_name (str): name of the id of the hedge field containing in the `data_hedge`. Defaults to "id".
    Return
    ---
    working_hedge (polyligne) : Duplicated layer on which processing was performed.
    shelters_area (polygon) : Layer delimitation of shelter areas.
    """   
    shelters_area = wf.compute_hedges_shelter(data_hedge,
                                                wind_orientation,
                                                max_extent,
                                                output_folder,
                                                height_field_name,
                                                porosity_field_name,
                                                width_field_name,
                                                hedge_id_field_name)
    alg_name = "native:removenullgeometries"
    params = {'INPUT' : shelters_area, 
                 'OUTPUT' : 'TEMPORARY_OUTPUT', 
                 'REMOVE_EMPTY' : True }
    shelters_area_removnullgeometries = processing.run(alg_name, params)['OUTPUT']
    shelters_area_removnullgeometries.setName("shelters_area")
    QgsProject.instance().addMapLayer(shelters_area_removnullgeometries) 
    gdf = gdf_from_qgis_layer(shelters_area_removnullgeometries)
    output_dict = {"shelters area": gdf}
    return shelters_area, output_dict


def compute_pnt_in_shelter_hedge_test(max_extent: int,
                          resolution: int,
                          data_hedge: QgsLineString,
                          wind_orientation: int,
                          output_folder: str,
                          height_field_name: str = "height",
                          porosity_field_name: str = "op",
                          width_field_name: str = "width",
                          hedge_id_field_name: str = "id"):
    
    shelters_area = wf.compute_hedges_shelter(data_hedge,
                                                wind_orientation,
                                                max_extent,
                                                output_folder,
                                                height_field_name,
                                                porosity_field_name,
                                                width_field_name,
                                                hedge_id_field_name)
    alg_name = "native:removenullgeometries"
    params = {'INPUT' : shelters_area, 
                'OUTPUT' : 'TEMPORARY_OUTPUT', 
                'REMOVE_EMPTY' : True }
    shelters_area_removnullgeometries = processing.run(alg_name, params)['OUTPUT']
    
    pnt_mesh_bash = wf.get_grid(resolution, shelters_area_removnullgeometries)

    pnt_intersect = wf.compute_pnt_intersect(shelters_area_removnullgeometries,pnt_mesh_bash, data_hedge, wind_orientation,
                                              height_field_name, porosity_field_name, width_field_name, hedge_id_field_name)
    pnt_overlay = wf.extract_pnt_overlay(pnt_intersect)
    pnt_without_overlay = wf.extract_pnt_without_overlay(pnt_intersect, output_folder)
    pnt_in_shelter_hedge  = wf.clean_pnt_overlay(pnt_overlay, pnt_intersect, pnt_without_overlay)
    pnt_intersect.setName("pnt intersect")
    pnt_overlay.setName("pnt overlay")
    pnt_without_overlay.setName("pnt no overlay")
    pnt_in_shelter_hedge.setName("pnt in shelter hedge")

    QgsProject.instance().addMapLayer(pnt_intersect)
    QgsProject.instance().addMapLayer(pnt_overlay) 
    QgsProject.instance().addMapLayer(pnt_without_overlay) 
    QgsProject.instance().addMapLayer(pnt_in_shelter_hedge) 
    
    gdf_pnt_intersect = gdf_from_qgis_layer(pnt_intersect)
    gdf_pnt_overlay = gdf_from_qgis_layer(pnt_overlay)
    gdf_pnt_no_overlay = gdf_from_qgis_layer(pnt_without_overlay)
    gdf_pnt_in_shelter_hedge = gdf_from_qgis_layer(pnt_in_shelter_hedge)

    output_dict = {"pnt_intersect": gdf_pnt_intersect, "pnt_overlay": gdf_pnt_overlay,
                   "pnt_without_overlay": gdf_pnt_no_overlay, "gdf_pnt_in_shelter_hedge" : gdf_pnt_in_shelter_hedge}
    
    return  pnt_intersect, pnt_overlay, pnt_without_overlay, output_dict



def compute_windbreak_effect_test(data_hedge: QgsPolygon, 
                             wind_orientation: int,
                             max_extent: int,
                             resolution:float,
                             output_folder: str,
                             height_field_name: str="height",
                             porosity_field_name: str="op",
                             width_field_name: str="width",
                             hedge_id_field_name: str="id",
                             intermediate_raster: bool=False,
                             sink:dict="TEMPORARY_OUTPUT"):
    
    shelters_area = wf.compute_hedges_shelter(data_hedge, wind_orientation,  max_extent, output_folder, height_field_name, 
                                              porosity_field_name, width_field_name, hedge_id_field_name)
    alg_name = "native:removenullgeometries"
    params = {'INPUT' : shelters_area, 
                'OUTPUT' : 'TEMPORARY_OUTPUT', 
                'REMOVE_EMPTY' : True }
    shelters_area_removnullgeometries = processing.run(alg_name, params)['OUTPUT']

    pnt_mesh_bash = wf.get_grid(resolution, shelters_area)

    pnt_intersect = wf.compute_pnt_intersect(shelters_area_removnullgeometries,pnt_mesh_bash, data_hedge, wind_orientation,
                                              height_field_name, porosity_field_name, width_field_name, hedge_id_field_name)
    pnt_overlay = wf.extract_pnt_overlay(pnt_intersect)
    pnt_without_overlay = wf.extract_pnt_without_overlay(pnt_intersect, output_folder)
    pnt_in_shelter_hedge  = wf.clean_pnt_overlay(pnt_overlay, pnt_intersect, pnt_without_overlay)
    
    shelters_area_removnullgeometries.setName("shelters area")
    pnt_in_shelter_hedge.setName("pnt in shelter hedge")

    raster_fxh = wf.compute_fxh

    QgsProject.instance().addMapLayer(shelters_area_removnullgeometries) 
    QgsProject.instance().addMapLayer(pnt_in_shelter_hedge)

    gdf_shelters = gdf_from_qgis_layer(shelters_area_removnullgeometries)
    gdf_pnt_in_shelter_hedge = gdf_from_qgis_layer(pnt_in_shelter_hedge)
    
    output_dict = {"shelters area": gdf_shelters, "pnt_in_shelter_hedge": gdf_pnt_in_shelter_hedge}
    
    return shelters_area, pnt_in_shelter_hedge, output_dict, raster_fxh