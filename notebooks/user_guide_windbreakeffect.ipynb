{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Guide d'utilisation de l'outil **windbreak effect**"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ce Notebook est destiné à guider l'utilisateur dans l'application de l'outil `windbreak_effect`. Cet outil, contenu dans le plug-in **HedgeTools**, permet de cartographier l'effet brise-vent des haies."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Modèle spatial d'effet brise-vent "
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Contextualisation"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Modèle WEPS "
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "La spatialisation de l'effet brise-vent des haies s'appuie sur le modèle d'érosion élienne **WEPS** (Wind Erosion Prediction System), (Vigiak et al. 2003).\n",
    "Ce modèle, créé en 2003, simule les conditions météorologiques, les conditions sur le terrain et l'érosion éolienne selon un pas de temps quotidien. Il se base sur une équation empirique :\n",
    "\n",
    "  $$ f_{xh} = 1 - \\exp[-axh^2] + b\\exp[-0.003(xh+c)^d]$$\n",
    "\n",
    "Où $ f_{xh}$  est le facteur de réduction de la vitesse de frottement. $xh$, la distance à la barrière.\n",
    "Et $a$, $b$, $c$, $d$, les coefficients :\n",
    "\n",
    "  $$\\begin{split} a =  & 0.008 - 0.17 \\theta + 0.017\\theta^{1.05}\\\\\n",
    "  b = & 1.35\\exp(-0.5\\theta^2) \\\\\n",
    "  c = & 10(1-0.5\\theta) \\\\\n",
    "  d = & 3-\\theta \\\\\n",
    "  \\theta = & op + 0.2\\frac{w}{h}\\\\\n",
    "\\end{split}$$\n",
    "\n",
    "La porosité du brise-vent est calculée à partir de la porosité optique $op$, de la largeur ($w$) mesurée perpendiculairement à l'axe principal du brise-vent et la hauteur ($h$). "
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Le phénomène de réduction de la vitesse se produits **SOUS** le vent ainsi que **FACE** au vent. Le diagramme ci-dessous, (Viagiak, 2003), illustre ce phénomène. \n",
    "<figure>\n",
    "    <center><img src=\"image/schema/windward_leeward.png\" title=\"Phénomène de réduction de la vitesse du vent\" width=\"55%\"></center>\n",
    "    <figcaption> Phénomène de réduction de la vitesse du vent (Viagiak, 2003). </figcaption>\n",
    "</figure>"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Objectifs de `windbreak_effect`"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Le modèle `windbreak_effect` permet de calculer le facteur de réduction de la vitesse de frottement du vent à partir de l'équation empirique (modèle WEPS) et de spatialiser ces zones protégées du vent, c'est-à-dire de déterminer en tout point de l'espace géographique, s'il est sous l'influence d'une haie, laquelle et dans quelles proportions.\n",
    "\n",
    "<figure>\n",
    "    <center><img src=\"image/schema/enjeu_windbreak_effect.png\" title=\"Les enjeux du modèle windbreak effect\" width=\"75%\"></center>\n",
    "    <figcaption>Les enjeux du modèle <code>windbreak_effect</code>. </figcaption>\n",
    "</figure>"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Les grandes étapes du modèle `windbreak_effect`"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "L'outil `windbreak_effect` permet de générer un raster de facteur de réduction de la vitesse frottement à partir d'un réseau de haies, d'une orientation du vent, et d'une résolution fournie en entrée du modèle par l'utilisateur.\n",
    "Le modèle se décompose en quatre grandes étapes, détaillées ci-dessous :"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<figure>\n",
    "    <center><img src=\"image/schema/etape_windbreak_effect_redac.png\" title=\"Les quatres grandes étapes du modèle windbreak effect\" width=\"65%\"></center>\n",
    "    <figcaption>Les quatres grandes étapes du modèle <code>windbreak_effect</code>. </figcaption>\n",
    "</figure>"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**1. Réseau de haies en entrée**\n",
    " - Description :couche de haies à founir en entrée du modèle. Cette couche doit respecter certaines caractéristiques et champs; les géométries doivent être corrigées au préalable.\n",
    " - Caractéristique : polyligne, vecteur.\n",
    " - Champs obligatoires : hauteur moyenne, largeur moyenne, porosité optique, identifiant de la haie.\n",
    "\n",
    "**2. Création des zones d'abris**\n",
    " - Description : couche des zones d'abris générée en fonction de la hauteur de la haie, de l'étendue maximale de la zone face et sous le vent et de l'orientation du vent.\n",
    " - Caractéristique : polygone, vecteur.\n",
    " - Champs : hauteur moyenne, largeur moyenne, porosité optique, identifiant de la haie, identifiant de la zone d'abri.\n",
    "\n",
    "**3. Sélection des points à l'intérieur des zones d'abris et traitement des superposition de points**\n",
    " - Description : intersection entre un maillage de point et les zones d'abris. Seulement les points contenus dans ces zones sont extraits. La distance minimale à la haie associée à chaque point est calculée en fonction de l'orientation du vent (donnée en entrée). Les superpositions de points  sont traités en fonction de la distance à la haie associée. La grille de point générée est proportionnelle à la résolution spatiale du raster en sortie.\n",
    " - Caractéristique : point, vecteur.\n",
    " - Champs : hauteur moyenne, largeur moyenne, porosité optique, distance à la haie, identifiant de la haie, identifiant de la zone d'abri, identidiant du point, position (leeward/windward).\n",
    "\n",
    " <figure>\n",
    "    <center><img src=\"image/schema/schema_traitement_pnt.png\" title=\"Traitements des points à l'intérieur des zones d'abris et calcul de distance\" width=\"85%\"></center>\n",
    "    <figcaption>Traitements des points à l'intérieur des zones d'abris et calcul de distance. </figcaption>\n",
    "</figure>\n",
    "\n",
    "**4. Calcul du raster du facteur de réduction de la vitesse de frottement**\n",
    " - Description : une fois traités, les points à l'intérieur des zones d'abris sont rasterisés en fonction de divers champs (hauteur, largeur, distance, porosité) afin de calculer le facteur de réduction de la vitesse de frottement $f_{xh}$.\n",
    " - Caractéristique : raster. \n",
    "\n",
    " <figure>\n",
    "    <center><img src=\"image/schema/schema_rasterisation.png\" title=\"Rasterisation et calcul du facteur de réduction de la vitesse du vent\" width=\"85%\"></center>\n",
    "    <figcaption>Rasterisation et calcul du facteur de réduction de la vitesse du vent. </figcaption>\n",
    "</figure>"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Diagramme de flux du modèle"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ce diagramme de flux présente de manière graphique et simplifiée l'enchaînement des traitements, les grandes fonctions et les données en entrées et sorties du modèle spatial d'effet brise-vent. Le jeu de données de haies, l'orientation du vent, l'étendue maximale de la zone d'abri et la résolution sont obligatoirement à remplir par l'utilisateur.\n",
    "Le modèle fait appel à quatre fonctions :\n",
    "- `compute_hedges_shelter` : calcul des zones d'abris ;\n",
    "- `get_grid` : création du quadrillage de points ;\n",
    "- `compute_pnt_in_shelter_hedge` : calcul et traitements des points à l'intérieur des zones d'abris ;\n",
    "- `compute_fxh` : calcul du raster $f_{xh}$ ."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<figure>\n",
    "    <center><img src=\"image/diagramme_flux/windbreak_effect_user.png\" title=\"diagramme de flux\" width=\"70%\"></center>\n",
    "    <figcaption>Diagramme de flux du modèle <code>windbreak_effect</code>. </figcaption>\n",
    "</figure>"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Utilisation de l'outil **windbreak effect**"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Interface de l'outil"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<figure>\n",
    "    <center><img src=\"image/schema/interface_utilisateur.png\" title=\"interface utilisateur\" width=\"70%\"></center>\n",
    "    <figcaption> Interface utilisateur du modèle <code>windbreak_effect</code>. </figcaption>\n",
    "</figure>"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Données"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<figure>\n",
    "    <center><img src=\"image/schema/donnee_entree_sortie.png\" title=\"Données entrantes et sortantes du modèle windbreak_effect\" width=\"80%\"></center>\n",
    "    <figcaption> Données entrantes et sortantes du modèle <code>windbreak_effect</code>. </figcaption>\n",
    "</figure>"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Données entrantes"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### *Obligatoires*"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " **Réseau de haie** : couche vecteur, polyligne.\n",
    " \n",
    " **Orientation du vent** : Flottant.\n",
    "\n",
    " **Étendue maximale de la zone d'abri face au vent** : multipliée par la hauteur de la haie, représente la distance maximale de la barrière le long de la direction du vent (${xh}$) - Flottant. La valeur maximale est 5 ${h}$.\n",
    " \n",
    " **Étendue maximale de la zone d'abri sous le vent** : multipliée par la hauteur de la haie, représente la distance maximale de la barrière opposée à la direction du vent (${xh}$) - Flottant. La valeur maximale est 35 ${h}$.\n",
    "\n",
    " **Résolution spatiale** : résolution spatiale (en pixels) du/des raster(s) en sorti(s) - Flottant."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### *Optionnelles*"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Champ hauteur** : nom du champ hauteur contenu dans la couche de haie - Chaîne de caractère. La valeur par défaut est *'height'*.\n",
    "\n",
    "**Champ largeur** : nom du champ largeur contenu dans la couche de haie - Chaîne de caractère. La valeur par défaut est *'width'*.\n",
    "\n",
    "**Champ porosité optique** : nom du champ de la porosité contenu dans la couche de haie - Chaîne de caractère. La valeur par défaut est *'op'*.\n",
    "\n",
    "**Champ identifiant de la haie** :  nom du champ id de la haie contenu dans la couche de haie - Chaîne de caractère. La valeur par défaut est *'id'*.\n",
    "\n",
    "**Enregistrement des rasters intermédiaires** :  option True/False enregistrement des couches intermédiaires - Booléen. La valeur par défaut est *'False'*.\n",
    "\n",
    "**Dossier de dépôt** : dossier où seront stockés les résultats (chemin) - Chaîne de caractère."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Données sortantes"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### *Obligatoires*"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " **Zones d'abris** : couche vecteur de polygones représentant les zones protégées par le vent.\n",
    "\n",
    " **Points à l'intérieur des zones d'abris** : couche vecteur de points à l'intérieur des zones d'abris. Ces points récupèrent les attributs de la zone protégée dans laquelle ils sont contenus; le nombre de points est proportionnel à la résolution spatiale des rasters souhaitée en sortie du modèle. \n",
    "\n",
    " **Raster $f_{xh}$** : raster du facteur de réduction de la vitesse du vent."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### *Optionnelles*"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " **Raster de la distance à la haie** : raster des distances minimales le long de la direction à la haie associé.\n",
    "\n",
    " **Raster ${xh}$** : raster de la distance de la barrière le long de la direction du vent en **hauteur de barrière**.\n",
    " \n",
    " **Raster des coefficient** : rasters des coefficient **a**, **b**, **c**, **d** implémentés dans l'équation empirique du modèle WEPS."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Cas particuliers"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "Les schémas qui suivent ont pour but d'expliquer le traitement des superpositions de points selon différents cas de figure.\n",
    "\n",
    "Dans chaque zone de superposition présentées dans les cas particuliers ci-dessous, les points sont dupliqués : un point est associé à la haie 1, un autre à la haie 2. Pour chaque point, la distance minimale à la haie associée, et selon une orientation du vent donnée, est calculée. Pour chaque cas de superposition, le point ayant la distance la plus petite est extrait."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Superposition de deux zones d'abris **sous** le vent "
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Le cas de figure ci-dessous présente une zone de superposition de **deux zones d'abris SOUS le vent**, indiquée par une flèche grise. La haie 1 est la haie la **plus proche de cette zone de superposition**. Par conséquent, seuls les points associés à la **haie 1** sont extraits et les points associés à la **haie 2** superposant les points associés à la **haie 1** sont supprimés.\n",
    "<figure>\n",
    "    <center><img src=\"image/schema/superposition_cas1.png\" title=\"Cas particuliers - superposition de deux zones d'abris sous le vent\" width=\"45%\"></center>\n",
    "    <figcaption>Cas particuliers - superposition de deux zones d'abris sous le vent. </figcaption>\n",
    "</figure>"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Superposition de deux zones d'abris **face** au vent"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Le cas de figure ci-dessous présente une zone de superposition de **deux zones d'abris FACE au vent**, indiquée par la flèche.  Ici tout les points associés à la **haie 1**, la haie la plus proche de ces derniers, sont extraits. Les points associés à la **haie 2** superposant les points associés à la **haie 1** sont alors supprimés.\n",
    "<figure>\n",
    "    <center><img src=\"image/schema/superposition_cas3.png\" title=\"Cas particuliers - superposition de deux zones d'abris face au vent\" width=\"45%\"></center>\n",
    "    <figcaption>Cas particuliers - superposition de deux zones d'abris face au vent. </figcaption>\n",
    "</figure>"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Superposition d'une zone d'abri **face** au vent avec une zone **sous** le vent"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Le cas de figure ci-dessous présente une zone de superposition de **d'une zone d'abri FACE au vent avec une zone SOUS le vent**, indiquée par la flèche. Ici une majeur partie des points associés à la **haie 1**, représenté en jaune, sont extraits car la haie 1 est la plus proche de ces points. Toutefois, certains points associés à la haie 2, représentés en rouge, sont extraits. À ces endroits, c'est la haie 2 qui se trouve être la plus proche.\n",
    "<figure>\n",
    "    <center><img src=\"image/schema/superposition_cas2.png\" title=\"Cas particuliers - superposition d'une zone d'abri face au vent avec une zone sous le vent\" width=\"45%\"></center>\n",
    "    <figcaption>Cas particuliers - superposition d'une zone d'abri face au vent avec une zone sous le vent. </figcaption>\n",
    "</figure>"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Analyse du temps de calcul"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Malgré la création d’index spatiaux sur certaines couches générées par le modèle, la durée globale du traitement reste assez importante. Le tableau, présenté ci-dessous, recense la durée de calcul du modèle et de ses fonctions testées sur différentes résolutions spatiales et selon les trois découpages de tronçons.\n",
    "Au regard du tableau, le temps des traitements semble proportionnel à la résolution spatiale souhaitée par l’utilisateur : plus la résolution spatiale est élevée (i.e. taille du pixel réduite), plus les points du quadrillage sont nombreux et plus la modélisation est longue.\n",
    "\n",
    "Pour les différents découpages, la modélisation est 10 fois plus longue lorsque l’on passe d’une résolution spatiale de 10 mètres (durée < 1 min) à 5 mètres (durée environ égale à 10 min).\n",
    "Une fonction influence grandement la durée de la modélisation : le calcul et traitements des points à l'intérieur des zones d'abris; celle- ci occupe 95% - 98% du temps du traitement. Comme évoquée plus haut, cette fonction permet d’extraire les points situés à l’intérieur des zones d’abris et mesurer la distance à la haie la plus proche selon une orientation donnée. Par conséquent plus la résolution est élevée, plus les points à traiter seront nombreux et plus la boucle de traitement sera longue (calculs répétés pour chaque point).\n",
    "<figure>\n",
    "    <center><img src=\"image/schema/temps_traitement.png\" title=\"Tableau - temps de calcul du modèle en fonction de la résolution spatiale et du découpage des tronçons de haie\" width=\"50%\"></center>\n",
    "    <figcaption>Tableau - temps de calcul du modèle en fonction de la résolution spatiale et du découpage des tronçons de haie. </figcaption>\n",
    "</figure>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<style>\n",
       "body{\n",
       "counter-reset: figure-counter;\n",
       "}\n",
       "\n",
       "figure{\n",
       "  text-align: center;\n",
       "}\n",
       "figcaption::before {\n",
       "  counter-increment:  figure-counter;\n",
       "  content: \"Figure \" counter(figure-counter) \" : \" ;\n",
       "  font-weight: bold;\n",
       "  color: #69271e;\n",
       "}\n",
       "</style>\n"
      ],
      "text/plain": [
       "<IPython.core.display.HTML object>"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Styling notebook\n",
    "from IPython.core.display import HTML\n",
    "def css_styling():\n",
    "    styles = open(\"./styles/fig_caption.css\", \"r\").read()\n",
    "    return HTML(styles)\n",
    "css_styling()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.9"
  },
  "toc-autonumbering": true
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
